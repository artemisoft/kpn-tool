package org.ntic;

import ctlparser.CTLKLexer;
import ctlparser.CTLKParser;
import org.antlr.v4.gui.TreeViewer;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;

import javax.swing.*;
import java.util.Arrays;

public class CTLKFormulaTreeViewer {

  public static void main(String[] args) {
    CTLKLexer ctlkLexer = new CTLKLexer(CharStreams.fromString("AG(p3,3->K(p1,3|p2,3))"));

    CommonTokenStream tokenStream = new CommonTokenStream(ctlkLexer);
    CTLKParser parser = new CTLKParser(tokenStream);
    parser.addErrorListener(new BaseErrorListener() {
      @Override
      public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
        super.syntaxError(recognizer, offendingSymbol, line, charPositionInLine, msg, e);
        throw new IllegalArgumentException("Syntax Error at line " + line + ":" + charPositionInLine);
      }
    });
    ParseTree tree = parser.formula();

    //show AST in console
    System.out.println(tree.toStringTree(parser));

    //show AST in GUI
    JFrame frame = new JFrame("Antlr AST");
    JPanel panel = new JPanel();
    TreeViewer viewer = new TreeViewer(Arrays.asList(parser.getRuleNames()), tree);
    viewer.setScale(1.5); // Scale a little
    panel.add(viewer);
    frame.add(panel);
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.pack();
    frame.setVisible(true);
  }

}