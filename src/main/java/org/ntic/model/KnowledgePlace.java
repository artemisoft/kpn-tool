package org.ntic.model;

public class KnowledgePlace extends Place {

  public KnowledgePlace(String name, boolean marking) {
    super(name, marking);
  }

  public KnowledgePlace() {
    super();
  }

  @Override
  public VertexType getType() {
    return VertexType.KNOWLEDGE_PLACE;
  }

  @Override
  public String toString() {
    return name + " - " + isMarked();
  }

}
