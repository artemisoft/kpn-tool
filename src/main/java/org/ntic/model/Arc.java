package org.ntic.model;

import org.jgrapht.graph.DefaultEdge;

public class Arc extends DefaultEdge {

  public Arc() {
  }

  @Override
  public Vertex getSource() {
    return (Vertex) super.getSource();
  }

  @Override
  public Vertex getTarget() {
    return (Vertex) super.getTarget();
  }

  @Override
  public String toString() {
    return getSource().getName() + " -> " + getTarget().getName();
  }

}
