package org.ntic.model;

import java.io.Serializable;

public class Agent implements Serializable {

  private String name;

  public Agent(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  @Override
  public String toString() {
    return name;
  }

  @Override
  public boolean equals(Object o) {
    if (this == o) return true;
    if (!(o instanceof Agent)) return false;

    Agent agent = (Agent) o;

    return getName().equals(agent.getName());
  }

  @Override
  public int hashCode() {
    return getName().hashCode();
  }

}
