package org.ntic.model.modelchecking.ctlk.expressions;

import org.ntic.model.modelchecking.ctlk.CTLKFormula;
import org.ntic.model.modelchecking.ctlk.UnaryOperator;

public class EX extends UnaryOperator {

  public EX(CTLKFormula operand) {
    super(operand);
  }

  @Override
  public CTLKFormula toENFExpression() {
    return new EX(getOperand().toENFExpression());
  }

}
