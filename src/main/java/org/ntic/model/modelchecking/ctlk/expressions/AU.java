package org.ntic.model.modelchecking.ctlk.expressions;

import org.ntic.model.modelchecking.ctlk.BinaryOperator;
import org.ntic.model.modelchecking.ctlk.CTLKFormula;
import org.ntic.model.modelchecking.ctlk.ENFExpression;

public class AU extends BinaryOperator implements ENFExpression {

  public AU(CTLKFormula firstOperand, CTLKFormula secondOperand) {
    super(firstOperand, secondOperand);
  }

  @Override
  public CTLKFormula toENFExpression() {
    return new NOT(new OR(new EU(new NOT(getSecondOperand().toENFExpression()), new AND(new NOT(getFirstOperand().toENFExpression()), new NOT(getSecondOperand().toENFExpression()))), new EG(new NOT(getSecondOperand().toENFExpression()))).toENFExpression());
  }

}
