package org.ntic.model.modelchecking.ctlk.expressions;

import org.ntic.model.modelchecking.ctlk.BinaryOperator;
import org.ntic.model.modelchecking.ctlk.CTLKFormula;

public class AND extends BinaryOperator {

  public AND(CTLKFormula firstOperand, CTLKFormula secondOperand) {
    super(firstOperand, secondOperand);
  }

  @Override
  public CTLKFormula toENFExpression() {
    return new AND(getFirstOperand().toENFExpression(), getSecondOperand().toENFExpression());
  }

}
