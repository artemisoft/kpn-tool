package org.ntic.model.modelchecking.ctlk.expressions;

import org.ntic.model.modelchecking.ctlk.BinaryOperator;
import org.ntic.model.modelchecking.ctlk.CTLKFormula;
import org.ntic.model.modelchecking.ctlk.ENFExpression;

public class EQUIVALENT extends BinaryOperator implements ENFExpression {

  public EQUIVALENT(CTLKFormula firstOperand, CTLKFormula secondOperand) {
    super(firstOperand, secondOperand);
  }

  @Override
  public CTLKFormula toENFExpression() {
    return new AND(new IMPLIES(getFirstOperand(), getSecondOperand()).toENFExpression(), new IMPLIES(getSecondOperand(), getFirstOperand()).toENFExpression());
  }

}
