package org.ntic.model.modelchecking.ctlk.expressions;

import org.ntic.model.modelchecking.ctlk.CTLKFormula;
import org.ntic.model.modelchecking.ctlk.ENFExpression;
import org.ntic.model.modelchecking.ctlk.UnaryOperator;

public class EF extends UnaryOperator implements ENFExpression {

  public EF(CTLKFormula operand) {
    super(operand);
  }

  @Override
  public CTLKFormula toENFExpression() {
    return new EU(new TRUE(), getOperand()).toENFExpression();
  }

}
