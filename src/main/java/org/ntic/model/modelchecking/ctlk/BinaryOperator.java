package org.ntic.model.modelchecking.ctlk;

public abstract class BinaryOperator extends Operator {

  private final CTLKFormula firstOperand;
  private final CTLKFormula secondOperand;

  public BinaryOperator(CTLKFormula firstOperand, CTLKFormula secondOperand) {
    this.firstOperand = firstOperand;
    this.secondOperand = secondOperand;
  }

  public CTLKFormula getFirstOperand() {
    return firstOperand;
  }

  public CTLKFormula getSecondOperand() {
    return secondOperand;
  }

}
