package org.ntic.model.modelchecking.ctlk.expressions;

import org.ntic.model.modelchecking.ctlk.CTLKFormula;
import org.ntic.model.modelchecking.ctlk.UnaryOperator;

public class NOT extends UnaryOperator {

  public NOT(CTLKFormula operand) {
    super(operand);
  }

  @Override
  public CTLKFormula toENFExpression() {
    return new NOT(getOperand().toENFExpression());
  }

}
