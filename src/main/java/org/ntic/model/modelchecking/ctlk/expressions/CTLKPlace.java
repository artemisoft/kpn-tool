package org.ntic.model.modelchecking.ctlk.expressions;

import org.ntic.model.modelchecking.ctlk.CTLKFormula;

public class CTLKPlace extends CTLKFormula {

  private final String name;

  public CTLKPlace(String name) {
    this.name = name;
  }

  public String getName() {
    return name;
  }

  @Override
  public CTLKFormula toENFExpression() {
    return this;
  }

}
