package org.ntic.model.modelchecking.ctlk.expressions;

import org.ntic.model.modelchecking.ctlk.BinaryOperator;
import org.ntic.model.modelchecking.ctlk.CTLKFormula;
import org.ntic.model.modelchecking.ctlk.ENFExpression;

public class IMPLIES extends BinaryOperator implements ENFExpression {

  public IMPLIES(CTLKFormula firstOperand, CTLKFormula secondOperand) {
    super(firstOperand, secondOperand);
  }

  @Override
  public CTLKFormula toENFExpression() {
    return new OR(new NOT(getFirstOperand()), getSecondOperand()).toENFExpression();
  }

}
