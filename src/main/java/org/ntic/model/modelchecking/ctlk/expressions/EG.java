package org.ntic.model.modelchecking.ctlk.expressions;

import org.ntic.model.modelchecking.ctlk.CTLKFormula;
import org.ntic.model.modelchecking.ctlk.UnaryOperator;

public class EG extends UnaryOperator {

  public EG(CTLKFormula operand) {
    super(operand);
  }

  @Override
  public CTLKFormula toENFExpression() {
    return new EG(getOperand().toENFExpression());
  }

}
