package org.ntic.model.modelchecking.ctlk;

public abstract class UnaryOperator extends Operator {

  private final CTLKFormula operand;

  public UnaryOperator(CTLKFormula operand) {
    this.operand = operand;
  }

  public CTLKFormula getOperand() {
    return operand;
  }

}
