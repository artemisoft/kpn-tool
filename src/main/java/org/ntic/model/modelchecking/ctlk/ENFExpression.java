package org.ntic.model.modelchecking.ctlk;

public interface ENFExpression {

  CTLKFormula toENFExpression();

}
