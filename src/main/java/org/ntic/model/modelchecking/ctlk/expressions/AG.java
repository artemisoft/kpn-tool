package org.ntic.model.modelchecking.ctlk.expressions;

import org.ntic.model.modelchecking.ctlk.CTLKFormula;
import org.ntic.model.modelchecking.ctlk.ENFExpression;
import org.ntic.model.modelchecking.ctlk.UnaryOperator;

public class AG extends UnaryOperator implements ENFExpression {

  public AG(CTLKFormula operand) {
    super(operand);
  }

  @Override
  public CTLKFormula toENFExpression() {
    return new NOT(new EF(new NOT(getOperand().toENFExpression())).toENFExpression());
  }

}
