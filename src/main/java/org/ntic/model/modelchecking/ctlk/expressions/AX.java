package org.ntic.model.modelchecking.ctlk.expressions;

import org.ntic.model.modelchecking.ctlk.CTLKFormula;
import org.ntic.model.modelchecking.ctlk.ENFExpression;
import org.ntic.model.modelchecking.ctlk.UnaryOperator;

public class AX extends UnaryOperator implements ENFExpression {

  public AX(CTLKFormula operand) {
    super(operand);
  }

  @Override
  public CTLKFormula toENFExpression() {
    return new NOT(new EX(new NOT(getOperand().toENFExpression())));
  }

}
