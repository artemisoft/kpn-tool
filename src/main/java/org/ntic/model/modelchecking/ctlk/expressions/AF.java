package org.ntic.model.modelchecking.ctlk.expressions;

import org.ntic.model.modelchecking.ctlk.CTLKFormula;
import org.ntic.model.modelchecking.ctlk.ENFExpression;
import org.ntic.model.modelchecking.ctlk.UnaryOperator;

public class AF extends UnaryOperator implements ENFExpression {

  public AF(CTLKFormula operand) {
    super(operand);
  }

  @Override
  public CTLKFormula toENFExpression() {
    return new NOT(new EU(new TRUE(), new NOT(getOperand().toENFExpression())));
  }

}
