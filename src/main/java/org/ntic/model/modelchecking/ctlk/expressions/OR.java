package org.ntic.model.modelchecking.ctlk.expressions;

import org.ntic.model.modelchecking.ctlk.BinaryOperator;
import org.ntic.model.modelchecking.ctlk.CTLKFormula;
import org.ntic.model.modelchecking.ctlk.ENFExpression;

public class OR extends BinaryOperator implements ENFExpression {

  public OR(CTLKFormula firstOperand, CTLKFormula secondOperand) {
    super(firstOperand, secondOperand);
  }

  @Override
  public CTLKFormula toENFExpression() {
    return new NOT(new AND(new NOT(getFirstOperand().toENFExpression()), new NOT(getSecondOperand().toENFExpression())));
  }

}
