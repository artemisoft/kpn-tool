package org.ntic.model.modelchecking.ctlk.expressions;

import org.ntic.model.modelchecking.ctlk.CTLKFormula;

public class TRUE extends CTLKFormula {

  @Override
  public CTLKFormula toENFExpression() {
    return this;
  }

}
