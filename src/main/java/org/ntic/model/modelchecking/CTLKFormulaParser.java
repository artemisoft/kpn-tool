package org.ntic.model.modelchecking;

import ctlparser.CTLKLexer;
import ctlparser.CTLKParser;
import org.antlr.v4.runtime.*;
import org.antlr.v4.runtime.tree.ParseTree;
import org.ntic.model.modelchecking.ctlk.CTLKFormula;
import org.ntic.model.modelchecking.ctlk.expressions.*;

public class CTLKFormulaParser {

  public CTLKFormula parse(String expression) {
    CTLKLexer ctlkLexer = new CTLKLexer(CharStreams.fromString(expression));
    CommonTokenStream tokenStream = new CommonTokenStream(ctlkLexer);
    CTLKParser parser = new CTLKParser(tokenStream);
    parser.addErrorListener(new BaseErrorListener() {
      @Override
      public void syntaxError(Recognizer<?, ?> recognizer, Object offendingSymbol, int line, int charPositionInLine, String msg, RecognitionException e) {
        super.syntaxError(recognizer, offendingSymbol, line, charPositionInLine, msg, e);
        throw new IllegalArgumentException("Syntax Error at line " + line + ":" + charPositionInLine);
      }
    });
    ParseTree tree = parser.formula();
    if (parser.getNumberOfSyntaxErrors() == 0)
      return toCTLKObject(tree);
    return null;
  }

  private CTLKFormula toCTLKObject(ParseTree tree) {
    switch (tree.getChild(0).getText()) {
      case "(":
        if (tree.getChildCount() == 3)
          return toCTLKObject(tree.getChild(1));
      case "!":
        return new NOT(toCTLKObject(tree.getChild(1)));
      case "A":
        return new AU(toCTLKObject(tree.getChild(2)), toCTLKObject(tree.getChild(4)));
      case "E":
        return new EU(toCTLKObject(tree.getChild(2)), toCTLKObject(tree.getChild(4)));
      case "AX":
        return new AX(toCTLKObject(tree.getChild(2)));
      case "AF":
        return new AF(toCTLKObject(tree.getChild(2)));
      case "AG":
        return new AG(toCTLKObject(tree.getChild(2)));
      case "EX":
        return new EX(toCTLKObject(tree.getChild(2)));
      case "EF":
        return new EF(toCTLKObject(tree.getChild(2)));
      case "EG":
        return new EG(toCTLKObject(tree.getChild(2)));
      case "K":
        return new K(toCTLKObject(tree.getChild(2)));
      case "true":
        if (tree.getChildCount() == 1)
          return new TRUE();
      default: {
        if (tree.getChildCount() == 1) {
          return new CTLKPlace(tree.getChild(0).getText());
        } else {
          return switch (tree.getChild(1).getText()) {
            case "|" -> new OR(toCTLKObject(tree.getChild(0)), toCTLKObject(tree.getChild(2)));
            case "&" -> new AND(toCTLKObject(tree.getChild(0)), toCTLKObject(tree.getChild(2)));
            case "->" -> new IMPLIES(toCTLKObject(tree.getChild(0)), toCTLKObject(tree.getChild(2)));
            case "<->" -> new EQUIVALENT(toCTLKObject(tree.getChild(0)), toCTLKObject(tree.getChild(2)));
            default -> null;
          };
        }
      }
    }
  }

}
