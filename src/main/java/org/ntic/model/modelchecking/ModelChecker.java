package org.ntic.model.modelchecking;

import org.ntic.model.Agent;
import org.ntic.model.AgentManager;
import org.ntic.model.KnowledgePlace;
import org.ntic.model.modelchecking.ctlk.CTLKFormula;
import org.ntic.model.modelchecking.ctlk.expressions.*;
import org.ntic.model.reachability.Marking;
import org.ntic.model.reachability.ReachabilityGraph;
import org.ntic.model.reachability.SimilarReachabilityGraph;

import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

public class ModelChecker {

  private final AgentManager agentManager;
  private Agent agent;

  public ModelChecker(AgentManager agentManager, Agent agent) {
    this.agentManager = agentManager;
    this.agent = agent;
  }

  public boolean modelChecking(CTLKFormula ex, SimilarReachabilityGraph sRGraph) {
    return sat(sRGraph, sRGraph.getM(), ex).contains(sRGraph.getKPetriNet().getInitialMarking(agent));
  }

  public Set<Marking> sat(SimilarReachabilityGraph sRGraph, Set<Marking> markings, CTLKFormula ex) {
    if (ex instanceof TRUE) return markings;
    if (ex instanceof CTLKPlace && sRGraph.getKPetriNet().getVertexByName(((CTLKPlace) ex).getName()) instanceof KnowledgePlace)
      return markings.stream().filter(marking -> marking.get(((CTLKPlace) ex).getName())).collect(Collectors.toSet());
    if (ex instanceof NOT) {
      Set<Marking> sat = sat(sRGraph, markings, ((NOT) ex).getOperand());
      return markings.stream().filter(marking -> !sat.contains(marking)).collect(Collectors.toSet());
    }
    if (ex instanceof AND) {
      markings = new HashSet<>(sat(sRGraph, markings, ((AND) ex).getFirstOperand()));
      return sat(sRGraph, markings, ((AND) ex).getSecondOperand());
    }
    if (ex instanceof EX)
      return satEX(sRGraph, markings, ((EX) ex).getOperand());
    if (ex instanceof EG)
      return satEG(sRGraph, markings, ((EG) ex).getOperand());
    if (ex instanceof EU)
      return satEU(sRGraph, markings, ((EU) ex).getFirstOperand(), ((EU) ex).getSecondOperand());
    if (ex instanceof K)
      return satK(sRGraph, markings, ((K) ex).getOperand());
    return new HashSet<>();
  }

  public Set<Marking> satEX(SimilarReachabilityGraph sRGraph, Set<Marking> markings, CTLKFormula ex) {
    return getNextOf(markings, sat(sRGraph, sRGraph.getM(), ex), sRGraph);
  }

  public Set<Marking> satEG(SimilarReachabilityGraph sRGraph, Set<Marking> markings, CTLKFormula ex) {
    Set<Marking> X = sat(sRGraph, sRGraph.getM(), ex);
    Set<Marking> Z = new HashSet<>(X);
    Set<Marking> Y = sRGraph.getM();

    while (!X.equals(Y)) {
      Y = new HashSet<>(X);
      Set<Marking> finalX = X;
      X = getNextOf(Z, finalX, sRGraph);
    }
    Set<Marking> Z1 = Z.stream().filter(sRGraph::isDeadLock).collect(Collectors.toSet());
    Set<Marking> Y1 = new HashSet<>();
    while (!Y1.equals(Z1)) {
      Y1 = new HashSet<>(Z1);
      Z1.addAll(Y1);
      Set<Marking> finalY1 = Y1;
      Z1.addAll(getNextOf(Z, finalY1, sRGraph));
    }
    Y.retainAll(Y1);
    Y.retainAll(markings);
    return Y;
  }

  public Set<Marking> satEU(SimilarReachabilityGraph sRGraph, Set<Marking> markings, CTLKFormula ex1, CTLKFormula ex2) {
    Set<Marking> X = sat(sRGraph, sRGraph.getM(), ex2);
    Set<Marking> Z = sat(sRGraph, sRGraph.getM(), ex1);
    Set<Marking> Y = new HashSet<>();

    while (!X.equals(Y)) {
      Y = new HashSet<>(X);
      X.addAll(Y);
      Set<Marking> finalY = Y;
      X.addAll(getNextOf(Z, finalY, sRGraph));
    }
    Y.retainAll(markings);
    return Y;
  }

  public Set<Marking> satK(SimilarReachabilityGraph sRGraph, Set<Marking> markings, CTLKFormula ex) {
    Set<Marking> X = sat(sRGraph, sRGraph.getM(), ex);
    Set<Marking> Y = new HashSet<>();
    for (Set<Marking> Q : sRGraph.getQ()) {
      if (!Q.retainAll(X))
        Y.addAll(Q);
    }
    Y.retainAll(markings);
    return Y;
  }

  // Gets every m ∈ M such as ∃t ∈ T, ∃m' ∈ X : (m', t, M) ∈ F
  private Set<Marking> getNextOf(Set<Marking> M, Set<Marking> M1, ReachabilityGraph graph) {
    return M.stream().filter(marking -> !graph.getF().stream().filter(triple -> triple.getLeft().equals(marking) && M1.contains(triple.getRight())).collect(Collectors.toSet()).isEmpty()).collect(Collectors.toSet());
  }

  public Agent getAgent() {
    return agent;
  }

  public void setAgent(Agent agent) {
    this.agent = agent;
  }

  public AgentManager getAgentManager() {
    return agentManager;
  }

}
