package org.ntic.model;

import java.util.ArrayList;

public class Transition extends Vertex implements Comparable<Transition> {

  private static final String FIRING = "firing";

  private boolean fireable;
  private long nextFiringTime;

  public Transition(String name) {
    super(name);
  }

  public Transition() {
    super();
  }

  @Override
  public VertexType getType() {
    return VertexType.TRANSITION;
  }

  @Override
  public String getName() {
    return this.name;
  }

  @Override
  protected void setName(String name) {
    this.name = name;
  }

  public void fire(KPetriNet g) {
    ArrayList<Vertex> predecessors = g.getPredecessors(this);
    ArrayList<Vertex> successors = g.getSuccessors(this);
    for (Vertex p : predecessors) {
      Place place = (Place) p;
      place.setMarked(false);
    }
    for (Vertex s : successors) {
      Place place = (Place) s;
      if (!place.isMarked()) {
        place.setMarked(true);
      } else {
        System.out.println("This is not a KPN, its not safe.");
      }
    }
  }

  public boolean isFireable() {
    return fireable;
  }

  public void setFireable(boolean fireable) {
    notifyListeners(FIRING, this.fireable, this.fireable = fireable);
  }

  public long getNextFiringTime() {
    return nextFiringTime;
  }

  public void setNextFiringTime(long nextFiringTime) {
    this.nextFiringTime = nextFiringTime;
  }

  @Override
  public int compareTo(Transition transition) {
    if (transition.getNextFiringTime() < this.getNextFiringTime()) {
      return 1;
    } else if (this.getNextFiringTime() < transition.getNextFiringTime()) {
      return -1;
    } else {
      return Math.random() < 0.5 ? -1 : 1; // equals, then generate priority randomly. (Specific to the simulator)
    }
  }

}
