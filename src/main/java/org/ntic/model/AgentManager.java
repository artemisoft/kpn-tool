package org.ntic.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.*;

public class AgentManager {

  private final Set<Agent> agents = new HashSet<>();
  private final HashMap<Agent, ArrayList<KnowledgePlace>> agentsMap = new HashMap<>();
  private final HashMap<KnowledgePlace, ArrayList<Agent>> placesMap = new HashMap<>();
  private final HashMap<KnowledgePlace, ArrayList<PropertyChangeListener>> agentListeners = new HashMap<>();
  private final ArrayList<AgentsEventListener> listeners = new ArrayList<>();

  public Set<Agent> getAgents() {
    return agents;
  }

  public Agent addAgent(String name, boolean silent) {
    Agent agent = new Agent(name);
    agents.add(agent);
    if (!silent) {
      for (AgentsEventListener listener : listeners) {
        listener.agentAdded(agent);
      }
    }
    return agent;
  }

  public Collection<KnowledgePlace> getAssociatedPlaces(Agent agent) {
    ArrayList<KnowledgePlace> knowledgePlaces = agentsMap.get(agent);
    return knowledgePlaces == null ? new ArrayList<>() : knowledgePlaces;
  }

  public List<Agent> getAssociatedAgents(KnowledgePlace place) {
    ArrayList<Agent> agents = placesMap.get(place);
    return agents == null ? new ArrayList<>() : agents;
  }

  public void removeAllAssociations(KnowledgePlace place) {
    if (placesMap.get(place) != null) {
      for (Agent agent : placesMap.get(place)) {
        agentsMap.get(agent).remove(place);
      }
    }
    placesMap.remove(place);
  }

  public void associateAgentKnowledgePlace(Agent agent, KnowledgePlace knowledgePlace) {
    if (agents.contains(agent)) {
      agentsMap.computeIfAbsent(agent, k -> new ArrayList<>()).add(knowledgePlace);
      ArrayList<Agent> agents = placesMap.computeIfAbsent(knowledgePlace, k -> new ArrayList<>());
      agents.add(agent);
      notifyListeners(knowledgePlace, null, agents);

    }
  }

  public void addAgentListener(KnowledgePlace place, PropertyChangeListener listener) {
    agentListeners.computeIfAbsent(place, k -> new ArrayList<>()).add(listener);
  }

  public void addAgentListener(AgentsEventListener listener) {
    listeners.add(listener);
  }

  public void notifyListeners(KnowledgePlace place, Object oldValue, Object newValue) {
    if (agentListeners.get(place) != null) {
      for (PropertyChangeListener name : agentListeners.get(place)) {
        name.propertyChange(new PropertyChangeEvent(place, "agents", oldValue, newValue));
      }
    }
  }

  public String getAgentLabelText(KnowledgePlace place) {
    return "{" + String.join(",", getAssociatedAgents(place).stream().map(Agent::getName).toArray(String[]::new)) + "}";
  }

  public void removeAllAssociations() {
    agents.forEach((a) -> listeners.forEach(l -> l.agentRemoved(a)));
    placesMap.clear();
    agentsMap.clear();
    agents.clear();
  }

  public interface AgentsEventListener {

    void agentAdded(Agent agent);

    void agentRemoved(Agent agent);

  }

}
