package org.ntic.model;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

public abstract class Vertex {

  private final List<PropertyChangeListener> listener = new ArrayList<>();
  protected String name;
  protected String id;

  public Vertex(String name) {
    this.name = name;
  }

  public Vertex() {

  }

  public abstract VertexType getType();

  public String getName() {
    return name;
  }

  protected void setName(String name) {
    this.name = name;
  }

  public void notifyListeners(String property, Object oldValue, Object newValue) {
    for (PropertyChangeListener name : listener) {
      name.propertyChange(new PropertyChangeEvent(this, property, oldValue, newValue));
    }
  }

  public void addChangeListener(PropertyChangeListener newListener) {
    listener.add(newListener);
  }

  public void removeChangeListener(PropertyChangeListener markingListener) {
    listener.remove(markingListener);
  }

  public String getId() {
    return id;
  }

  public void setId(String id) {
    this.id = id;
  }

  public List<PropertyChangeListener> getListener() {
    return listener;
  }

  @Override
  public String toString() {
    return name;
  }

  public enum VertexType {
    KNOWLEDGE_PLACE,
    TRANSITION,
    STATE_PLACE
  }

}
