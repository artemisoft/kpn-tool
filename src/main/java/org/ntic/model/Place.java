package org.ntic.model;

public abstract class Place extends Vertex {

  private static final String MARKING = "marking";

  private boolean marked;
  private boolean initiallyMarked;

  Place(String name, boolean initiallyMarked) {
    super(name);
    setInitiallyMarked(initiallyMarked);
    setMarked(initiallyMarked);
  }

  public Place() {
    super();
  }

  public boolean isMarked() {
    return marked;
  }

  public void setMarked(boolean marked) {
    notifyListeners(MARKING, this.marked, this.marked = marked);
  }

  @Override
  public String getName() {
    return name;
  }

  @Override
  protected void setName(String name) {
    this.name = name;
  }

  public void toggleInitiallyMarked() {
    this.setInitiallyMarked(!initiallyMarked);
  }

  public boolean isInitiallyMarked() {
    return initiallyMarked;
  }

  public void setInitiallyMarked(boolean initiallyMarked) {
    this.initiallyMarked = initiallyMarked;
    setMarked(initiallyMarked);
  }

  @Override
  public String toString() {
    return super.toString() + " - " + marked;
  }

}
