package org.ntic.model;

import org.javatuples.Pair;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultListenableGraph;
import org.ntic.model.reachability.Marking;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

public class KPetriNet extends DefaultListenableGraph<Vertex, Arc> {

  private final Set<String> uniqueNames = new HashSet<>();
  private int statePlaceCounter = 1;
  private int knowledgePlaceCounter = 1;
  private int transitionCounter = 1;
  private String name;

  public KPetriNet() {
    super(new DefaultDirectedGraph<>(Arc.class));
  }

  public KPetriNet(String name) {
    this();
    this.name = name;
  }

  public KPetriNet(String netName, ArrayList<Vertex> vertices, List<Pair<Vertex, Vertex>> arcTerminals) {
    this(netName);

    for (Vertex vertex : vertices) {
      super.addVertex(vertex);
      uniqueNames.add(vertex.getName());
      String vertexName = vertex.getName();

      if (vertexName.matches("p[sk]\\d+}"))
        vertexName = vertexName.replaceAll("\\D+", "");
      else
        vertexName = "0";

      if (vertex instanceof StatePlace) {
        statePlaceCounter = Math.max(statePlaceCounter, Integer.parseInt(vertexName) + 1);
      } else if (vertex instanceof KnowledgePlace) {
        knowledgePlaceCounter = Math.max(knowledgePlaceCounter, Integer.parseInt(vertexName) + 1);
      } else if (vertex instanceof Transition) {
        transitionCounter = Math.max(transitionCounter, Integer.parseInt(vertexName) + 1);
      }
    }
    for (Pair<Vertex, Vertex> arc : arcTerminals) {
      this.addEdge(arc.getValue0(), arc.getValue1());
    }
  }

  @Override
  public Arc addEdge(Vertex sourceVertex, Vertex targetVertex) {
    //Check if from Place to transition or the opposite.
    if (sourceVertex instanceof Place && targetVertex instanceof Place || sourceVertex instanceof Transition && targetVertex instanceof Transition)
      throw new RuntimeException("Source and target vertex must not be the same type.");
      //Check if edge in this direction already exists.
    else if (this.containsEdge(sourceVertex, targetVertex))
      throw new RuntimeException("Edge already exists (must be at most one edge in each direction between each 2 vertices).");
    else {
      return super.addEdge(sourceVertex, targetVertex);
    }
  }

  public boolean validateConnection(Vertex sourceVertex, Vertex targetVertex) {
    //Check if from Place to transition or the opposite.
    if (sourceVertex instanceof Place && targetVertex instanceof Place || sourceVertex instanceof Transition && targetVertex instanceof Transition)
      return false;
      //Check if edge in this direction already exists.
    else
      return !this.containsEdge(sourceVertex, targetVertex);
  }

  public void addVertexWithCallable(Vertex vertex, KPNCallable callable) {
    addVertex(vertex, callable);
  }

  public void addTransitionWithCallable(KPNCallable callable) {
    Transition transition = new Transition("");
    addVertex(transition, callable);
  }

  public void addStatePlaceWithCallable(KPNCallable callable) {
    StatePlace place = new StatePlace();
    addVertex(place, callable);
  }

  public void addKnowledgePlaceWithCallable(KPNCallable callable) {
    KnowledgePlace place = new KnowledgePlace();
    addVertex(place, callable);
  }

  @Override
  public boolean addVertex(Vertex vertex) {
    return addVertex(vertex, null);
  }

  private boolean addVertex(Vertex vertex, KPNCallable callable) {
    vertex.setName(getUniqueName(vertex));

    boolean modified = getDelegate().addVertex(vertex);
    if (modified) {
      if (callable != null) {
        callable.call(vertex);
      }
      fireVertexAdded(vertex);
    }
    return modified;
  }

  @Override
  public String toString() {
    StringBuilder string = new StringBuilder();
    string.append("KPetriNet{   ");
    string.append("vertexes   ");
    for (Vertex vertex : vertexSet()) {
      string.append("(").append(vertex.getName()).append(" ").append(vertex instanceof Place ? ((Place) vertex).isMarked() : "").append(")   ");
    }
    string.append("Arcs   ");
    for (Arc arc : edgeSet()) {
      string.append("(").append(arc.getSource().getName()).append(" -> ").append(arc.getTarget().getName()).append(")   ");
    }
    string.append("   }");
    return string.toString();
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public boolean updateName(Vertex vertex, String name) {
    if (!uniqueNames.contains(name)) {
      uniqueNames.remove(vertex.getName());
      vertex.setName(name);
      uniqueNames.add(name);
      return true;
    }
    return false;
  }

  private String getUniqueName(Vertex vertex) {
    if (vertex.getName() == null || vertex.getName().isBlank() || uniqueNames.contains(vertex.getName())) {
      String name = "";
      if (vertex instanceof StatePlace) {
        name = "ps" + statePlaceCounter++;
      } else if (vertex instanceof KnowledgePlace) {
        name = "pk" + knowledgePlaceCounter++;
      } else if (vertex instanceof Transition) {
        name = "t" + transitionCounter++;
      }
      uniqueNames.add(name);
      return name;
    } else {
      uniqueNames.add(vertex.getName());
      return vertex.getName();
    }
  }

  public boolean isSafe() {

    //if it has a source transition it can't be safe
    //if every transition has one input and one output it is safe
    // a place with 2 inputs is not afe unless it both of it's inputs come from a path coming from the same place
    //if a transition generates a new token (new path) both of it's paths must come together in another transition
    //if it generates more than 2 paths, all of its paths must eventually come together in one transition
    return true;
  }

  public ArrayList<Vertex> getSuccessors(Vertex vertex) {
    Set<Arc> arcs = outgoingEdgesOf(vertex);
    ArrayList<Vertex> successors = new ArrayList<>();
    for (Arc arc : arcs) {
      successors.add(arc.getTarget());
    }
    return successors;
  }

  public ArrayList<Vertex> getPredecessors(Vertex vertex) {
    Set<Arc> arcs = incomingEdgesOf(vertex);
    ArrayList<Vertex> predecessors = new ArrayList<>();
    for (Arc arc : arcs) {
      predecessors.add(arc.getSource());
    }
    return predecessors;
  }

  public ArrayList<Place> getPlaces() {
    ArrayList<Place> places = new ArrayList<>();
    for (Vertex vertex : vertexSet()) {
      if (vertex instanceof Place) {
        places.add((Place) vertex);
      }
    }
    return places;
  }

  public ArrayList<Transition> getTransitions() {
    ArrayList<Transition> transitions = new ArrayList<>();
    for (Vertex vertex : vertexSet()) {
      if (vertex instanceof Transition) {
        transitions.add((Transition) vertex);
      }
    }
    return transitions;
  }

  public Vertex getVertexByName(String name) {
    for (Vertex vertex : vertexSet()) {
      if (vertex.getName().equals(name)) {
        return vertex;
      }
    }
    System.out.println("Not found");
    return null;
  }

  // Checks the firing of a transition
  public void checkFiring(Transition transition) {
    ArrayList<Vertex> tPredecessors = getPredecessors(transition);
    boolean fireable = true;
    for (Vertex p : tPredecessors) {
      if (!((Place) p).isMarked()) {
        fireable = false;
        break;
      }
    }
    transition.setFireable(fireable);
  }

  public Marking getInitialMarking() {
    Marking marking = new Marking(getPlaces().size());
    for (Place place : getPlaces()) {
      marking.put(place.getName(), place.isInitiallyMarked());
    }
    return marking;
  }

  public Marking getInitialMarking(Agent agent) {
    Marking marking = new Marking(getPlaces().size());
    for (Place place : getPlaces()) {
      marking.setAgent(agent);
      marking.put(place.getName(), place.isInitiallyMarked());
    }
    return marking;
  }

  public Marking getMarking() {
    Marking marking = new Marking(getPlaces().size());
    for (Place place : getPlaces()) {
      marking.put(place.getName(), place.isMarked());
    }
    return marking;
  }

  public void setMarking(Marking marking) {
    for (Place place : getPlaces()) {
      place.setMarked(marking.get(place.getName()));
    }
  }

  @Override
  public boolean removeVertex(Vertex vertex) {
    uniqueNames.remove(vertex.getName());
    return super.removeVertex(vertex);
  }

  public interface KPNCallable {

    void call(Vertex transition);

  }

}
