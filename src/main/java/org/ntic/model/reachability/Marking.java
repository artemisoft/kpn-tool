package org.ntic.model.reachability;

import org.ntic.model.Agent;

import java.util.HashMap;
import java.util.LinkedHashMap;
import java.util.Map;

import static java.util.Map.Entry.comparingByKey;
import static java.util.stream.Collectors.toMap;

public class Marking extends HashMap<String, Boolean> {

  private int equivalenceClass = -1;
  private Agent agent;
  private String name;

  public Marking() {
  }

  public Marking(int initialCapacity) {
    super(initialCapacity);
  }

  public int getEquivalenceClass() {
    return equivalenceClass;
  }

  public void setEquivalenceClass(int equivalenceClass) {
    this.equivalenceClass = equivalenceClass;
  }

  @Override
  public String toString() {
    Map<String, Boolean> result = entrySet()
        .stream()
        .filter(Entry::getValue)
        .collect(toMap(Entry::getKey, stringBooleanEntry -> true));
    result = result.entrySet()
        .stream()
        .sorted(comparingByKey())
        .collect(
            toMap(Entry::getKey, Entry::getValue,
                (e1, e2) -> e2, LinkedHashMap::new));
    String prefix = "";
    StringBuilder s = new StringBuilder();
    for (String p : result.keySet()) {
      s.append(prefix);
      prefix = "+";
      s.append(p);
    }
    return name + "=" + s.toString();
  }

  @Override
  public boolean equals(Object o) {
    return super.equals(o) && (agent == null || agent.equals(((Marking) o).getAgent()));
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public Agent getAgent() {
    return agent;
  }

  public void setAgent(Agent agent) {
    this.agent = agent;
  }

}
