package org.ntic.model.reachability;

import org.jgrapht.graph.DefaultEdge;
import org.ntic.model.Transition;

public class TransitionArc extends DefaultEdge {

  private final Transition transition;

  public TransitionArc(Transition transition) {
    this.transition = transition;
  }

  @Override
  public Marking getSource() {
    return (Marking) super.getSource();
  }

  @Override
  public Marking getTarget() {
    return (Marking) super.getTarget();
  }

  public Transition getTransition() {
    return transition;
  }

  @Override
  public String toString() {
    return transition.getName();
  }

}
