package org.ntic.model.reachability;

import org.ntic.model.Agent;
import org.ntic.model.AgentManager;
import org.ntic.model.KPetriNet;
import org.ntic.model.KnowledgePlace;
import org.ntic.util.UniqueQueue;

import java.util.HashSet;
import java.util.Set;

public class SimilarReachabilityGraph extends ReachabilityGraph {

  private final Set<UniqueQueue<Marking>> Q = new HashSet<>();

  private final AgentManager agentManager;
  private final Agent agent;

  public SimilarReachabilityGraph(KPetriNet kPetriNet, AgentManager agentManager, Agent agent) {
    super(kPetriNet);
    this.agentManager = agentManager;
    this.agent = agent;
  }

  @Override
  public void build() {
    super.build();
    int eqClass = 0;
    boolean found = false;
    for (Marking m : getM()) {
      m.setAgent(agent);
      for (UniqueQueue<Marking> q : Q) {
        found = true;
        for (KnowledgePlace place : agentManager.getAssociatedPlaces(agent))
          if (q.peek() != null) {
            found &= q.peek().get(place.getName()) == m.get(place.getName());
          }
        if (found && q.peek() != null) {
          m.setEquivalenceClass(q.peek().getEquivalenceClass());
          q.add(m);
          break;
        }
      }
      if (!found) {
        Q.add(new UniqueQueue<>(m));
        m.setEquivalenceClass(eqClass);
        eqClass++;
      }
    }
  }

  public Set<Set<Marking>> getQ() {
    Set<Set<Marking>> Q = new HashSet<>();
    for (UniqueQueue<Marking> q : this.Q) {
      Q.add(new HashSet<>(q));
    }
    return Q;
  }

  public Agent getAgent() {
    return this.agent;
  }

}
