package org.ntic.model.reachability;

import org.apache.commons.lang3.tuple.MutableTriple;
import org.apache.commons.lang3.tuple.Triple;
import org.jgrapht.graph.DefaultDirectedGraph;
import org.jgrapht.graph.DefaultListenableGraph;
import org.ntic.model.KPetriNet;
import org.ntic.model.Transition;
import org.ntic.util.UniqueQueue;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class ReachabilityGraph extends DefaultListenableGraph<Marking, TransitionArc> {

  private final UniqueQueue<Marking> M = new UniqueQueue<>();
  private final UniqueQueue<Transition> T = new UniqueQueue<>();
  private final UniqueQueue<Triple<Marking, Transition, Marking>> F = new UniqueQueue<>();
  private final KPetriNet kPetriNet;

  public ReachabilityGraph(KPetriNet kPetriNet) {
    super(new DefaultDirectedGraph<>(TransitionArc.class));
    this.kPetriNet = kPetriNet;
  }

  public void build() {
    int namingIndex = 0;
    Marking m0 = this.kPetriNet.getInitialMarking();
    m0.setName("M" + namingIndex);
    UniqueQueue<Marking> wait = new UniqueQueue<>();
    M.add(m0);
    wait.add(m0);
    addVertex(m0);
    while (!wait.isEmpty()) {
      Marking selectedState = wait.remove();
      for (Transition transition : getEnabledAtState(kPetriNet, selectedState)) {
        transition.fire(kPetriNet);
        Marking newState = kPetriNet.getMarking();
        kPetriNet.setMarking(selectedState);
        T.add(transition);
        F.add(new MutableTriple<>(selectedState, transition, newState));
        addVertex(newState);
        addEdge(selectedState, newState, new TransitionArc(transition));
        if (M.add(newState)) {
          newState.setName("M" + ++namingIndex);
          wait.add(newState);
          addVertex(newState);
        }
      }
    }
    kPetriNet.setMarking(kPetriNet.getInitialMarking());
  }

  protected ArrayList<Transition> getEnabledAtState(KPetriNet kPetriNet, Marking selectedState) {
    ArrayList<Transition> enabledAtSelected = new ArrayList<>();
    kPetriNet.setMarking(selectedState);
    for (Transition transition : kPetriNet.getTransitions()) {
      kPetriNet.checkFiring(transition);
      if (transition.isFireable()) {
        enabledAtSelected.add(transition);
      }
    }
    return enabledAtSelected;
  }

  public Set<Marking> getM() {
    return new HashSet<>(M);
  }

  public Set<Transition> getT() {
    return new HashSet<>(T);
  }

  public Set<Triple<Marking, Transition, Marking>> getF() {
    return new HashSet<>(F);
  }

  @Override
  public String toString() {
    StringBuilder s = new StringBuilder();
    for (Marking m : vertexSet()) {
      s.append(m.toString()).append("\n");
    }
    return s.toString();
  }

  public boolean isDeadLock(Marking marking) {
    return getEnabledAtState(kPetriNet, marking).isEmpty();
  }

  public KPetriNet getKPetriNet() {
    return kPetriNet;
  }

}
