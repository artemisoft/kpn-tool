package org.ntic.model;

public class StatePlace extends Place {

  public StatePlace(String name, boolean marking) {
    super(name, marking);
  }

  public StatePlace() {
    super();
  }

  @Override
  public VertexType getType() {
    return VertexType.STATE_PLACE;
  }

}
