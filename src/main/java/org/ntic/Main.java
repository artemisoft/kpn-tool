package org.ntic;

import com.formdev.flatlaf.FlatLightLaf;
import com.mxgraph.swing.util.mxSwingConstants;
import com.mxgraph.util.mxConstants;
import org.ntic.view.SplashScreen;
import org.ntic.view.editor.GraphEditor;

import javax.swing.*;
import java.awt.*;
import java.util.Properties;

public class Main {

  public static final String USER_SETTINGS_PATH = System.getenv("LOCALAPPDATA") + "\\KPN Tool\\settings.properties";
  public static GraphEditor editor;
  public static Properties settings = new Properties();
  public static JFrame frame;

  public static void main(String[] args) {
    mxConstants.DEFAULT_FONTSIZE = 14;
    mxConstants.DEFAULT_FONTFAMILY = "JetBrains Mono Medium";
    mxSwingConstants.VERTEX_SELECTION_COLOR = new Color(253, 141, 52);
    mxSwingConstants.EDGE_SELECTION_COLOR = new Color(253, 141, 52);
    mxSwingConstants.VERTEX_SELECTION_STROKE = new BasicStroke(3, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND, 1.0f, new float[]{1, 0}, 0.0f);
    mxSwingConstants.EDGE_SELECTION_STROKE = new BasicStroke(3, BasicStroke.CAP_SQUARE, BasicStroke.JOIN_ROUND, 1.0f, new float[]{1, 0}, 0.0f);
    UIManager.put("ScrollBar.showButtons", true);
    UIManager.put("ScrollBar.width", 16);
    try {
      UIManager.setLookAndFeel(new FlatLightLaf());
    } catch (Exception ex) {
      System.err.println("Failed to initialize LaF");
    }
    editor = new GraphEditor();
    frame = editor.createFrame();
    SplashScreen splashScreen = new SplashScreen();
    splashScreen.splash();
  }

}
