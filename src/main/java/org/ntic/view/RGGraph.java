package org.ntic.view;

import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.layout.mxGraphLayout;
import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.util.mxConstants;
import com.mxgraph.view.mxGraph;
import org.ntic.model.reachability.Marking;
import org.ntic.model.reachability.ReachabilityGraph;
import org.ntic.model.reachability.TransitionArc;

import java.util.HashMap;
import java.util.Iterator;

public class RGGraph extends mxGraph {

  protected static final int MARKING_HEIGHT = 40;
  protected static final int MARKING_WIDTH = 40;
  private final RGGraphComponent graphComponent;
  private final mxGraphLayout graphLayout;
  private final HashMap<Marking, mxCell> markings = new HashMap<>();
  private final HashMap<TransitionArc, mxCell> arcsToCells = new HashMap<>();
  private final HashMap<mxCell, TransitionArc> cellsToArcs = new HashMap<>();

  protected RGGraph() {
    this.graphComponent = new RGGraphComponent(this);
    this.graphLayout = new mxHierarchicalLayout(this);
    this.setAllowDanglingEdges(false);
    this.setCellsResizable(false);
    this.setEdgeLabelsMovable(false);
    this.setCollapseToPreferredSize(true);
    this.setAutoSizeCells(true);
    this.getStylesheet().getDefaultEdgeStyle().put(mxConstants.STYLE_EDGE, mxConstants.EDGESTYLE_ENTITY_RELATION);
  }

  public RGGraph(ReachabilityGraph rGraph) {
    this();

    if (rGraph == null)
      throw new IllegalArgumentException();

    this.drawRGraph(rGraph);
    graphLayout.execute(getDefaultParent());
  }

  public void drawRGraph(ReachabilityGraph rGraph) {

    Iterator<Marking> vertexIterator = rGraph.vertexSet().iterator();

    Marking marking;
    while (vertexIterator.hasNext()) {
      marking = vertexIterator.next();
      this.drawMarking(marking, "white");
    }

    Iterator<TransitionArc> edgeIterator = rGraph.edgeSet().iterator();

    TransitionArc transitionArc;
    while (edgeIterator.hasNext()) {
      transitionArc = edgeIterator.next();
      this.drawArc(transitionArc);
    }
  }

  public void drawMarking(Marking marking, String fillColor) {
    int x = (int) (Math.random() * 0.8 * View.WIDTH + MARKING_WIDTH);
    int y = (int) (Math.random() * 0.8 * View.HEIGHT + MARKING_HEIGHT);
    this.getModel().beginUpdate();
    try {
      mxCell g = (mxCell) this.insertVertex(this.getDefaultParent(), null, marking, x, y, MARKING_WIDTH, MARKING_HEIGHT, "ROUNDED;strokeColor=#646464;strokeWidth=3;fillColor=" + fillColor + ";fontColor=#434343");
      this.markings.put(marking, g);
      updateCellSize(g);
      mxGeometry geometry = g.getGeometry();
      g.setGeometry(new mxGeometry(geometry.getX(), geometry.getY(), geometry.getWidth() + 16, MARKING_HEIGHT));
    } finally {
      this.getModel().endUpdate();
    }
  }

  public void drawArc(TransitionArc transitionArc) {
    this.getModel().beginUpdate();
    try {
      mxCell g = (mxCell) this.insertEdge(this.getDefaultParent(), null, transitionArc.toString(), markings.get(transitionArc.getSource()), markings.get(transitionArc.getTarget()), "strokeColor=#434343;strokeWidth=3");
      arcsToCells.put(transitionArc, g);
      cellsToArcs.put(g, transitionArc);
    } finally {
      this.getModel().endUpdate();
    }
  }

  public RGGraphComponent getGraphComponent() {
    return graphComponent;
  }

  public mxGraphLayout getGraphLayout() {
    return graphLayout;
  }

  @Override
  public boolean isCellsEditable() {
    return false;
  }

  @Override
  public boolean isCellsDeletable() {
    return false;
  }

  @Override
  public boolean isCellConnectable(Object cell) {
    return false;
  }

  @Override
  public boolean isValidDropTarget(Object cell, Object[] cells) {
    return false;
  }

  public HashMap<Marking, mxCell> getMarkings() {
    return markings;
  }

}
