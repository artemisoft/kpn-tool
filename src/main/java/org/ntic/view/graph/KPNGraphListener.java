package org.ntic.view.graph;

import org.jgrapht.event.GraphEdgeChangeEvent;
import org.jgrapht.event.GraphVertexChangeEvent;
import org.ntic.model.Arc;
import org.ntic.model.Vertex;

public class KPNGraphListener implements org.jgrapht.event.GraphListener<Vertex, Arc> {

  private final KPetriNetGraph graph;

  public KPNGraphListener(KPetriNetGraph graph) {
    this.graph = graph;
  }

  @Override
  public void vertexAdded(GraphVertexChangeEvent<Vertex> e) {
    Vertex vertex = e.getVertex();
    graph.getGraphPainter().drawVertex(vertex);
  }

  @Override
  public void vertexRemoved(GraphVertexChangeEvent<Vertex> e) {
    graph.getGraphPainter().eraseVertex(e.getVertex());
  }

  @Override
  public void edgeAdded(GraphEdgeChangeEvent<Vertex, Arc> e) {
    this.graph.getGraphPainter().drawArc(e.getEdge());
  }

  @Override
  public void edgeRemoved(GraphEdgeChangeEvent<Vertex, Arc> e) {
    this.graph.getGraphPainter().eraseEdge(e.getEdge());
  }

}
