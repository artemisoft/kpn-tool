package org.ntic.view.graph;

import com.mxgraph.model.mxCell;
import com.mxgraph.swing.handler.*;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.view.mxICellEditor;
import com.mxgraph.swing.view.mxInteractiveCanvas;
import com.mxgraph.view.mxCellState;
import com.mxgraph.view.mxEdgeStyle;
import com.mxgraph.view.mxGraph;
import org.ntic.view.KPNCellEditor;
import org.ntic.view.editor.EditorKeyboardHandler;

import java.awt.*;
import java.awt.event.MouseEvent;

public class KPNGraphComponent extends mxGraphComponent {

  private static final long serialVersionUID = 4683716829748931448L;
  private Cursor customCursor;
  private EditorKeyboardHandler keyboardHandler;

  public KPNGraphComponent(mxGraph graph) {
    super(graph);
    getViewport().setPreferredSize(new Dimension(512, 512));
    getViewport().setOpaque(true);
    getViewport().setBackground(Color.WHITE);
    setBorder(null);

    getVerticalScrollBar().setUnitIncrement(16);
  }

  @Override
  public mxInteractiveCanvas createCanvas() {
    return new KPNCanvas(this);
  }

  @Override
  public KPetriNetGraph getGraph() {
    return (KPetriNetGraph) super.getGraph();
  }

  void setCustomCursor(Cursor customCursor) {
    this.customCursor = customCursor;
    getGraphControl().setCursor(customCursor);
  }

  @Override
  protected mxGraphControl createGraphControl() {
    return new mxGraphControl() {
      @Override
      public void setCursor(Cursor cursor) {
        if (customCursor == null || customCursor == cursor)
          super.setCursor(cursor);
      }
    };
  }

  @Override
  public mxCellHandler createHandler(mxCellState state) {
    if (graph.getModel().isVertex(state.getCell())) {
      return new mxVertexHandler(this, state);
    } else if (graph.getModel().isEdge(state.getCell())) {
      mxEdgeStyle.mxEdgeStyleFunction style = graph.getView().getEdgeStyle(state,
          null, null, null);

      if (graph.isLoop(state) || style == mxEdgeStyle.ElbowConnector
          || style == mxEdgeStyle.SideToSide
          || style == mxEdgeStyle.TopToBottom) {
        return new mxElbowEdgeHandler(this, state);
      }

      return new mxEdgeHandler(this, state){
        @Override
        public void mouseReleased(MouseEvent e) {
          graph.repaint();
          super.mouseReleased(e);
        }
        @Override
        public void mouseDragged(MouseEvent e) {
          ((KPNCanvas) getGraphComponent().getCanvas()).updateAbsolutePoints((mxCell) state.getCell());
          super.mouseDragged(e);
        }
      };
    }

    return new mxCellHandler(this, state);
  }

  @Override
  protected mxConnectionHandler createConnectionHandler() {
    return new myMxConnectionHandler(this);
  }

  public EditorKeyboardHandler getKeyboardHandler() {
    return keyboardHandler;
  }

  public void setKeyboardHandler(EditorKeyboardHandler keyboardHandler) {
    this.keyboardHandler = keyboardHandler;
  }

  @Override
  protected mxICellEditor createCellEditor() {
    return new KPNCellEditor(this);
  }

  private static class myMxConnectionHandler extends mxConnectionHandler {

    public myMxConnectionHandler(KPNGraphComponent kpnGraphComponent) {
      super(kpnGraphComponent);

      marker = new mxCellMarker(graphComponent) {
        /**
         *
         */
        private static final long serialVersionUID = 103433247310526381L;

        // Overrides to return cell at location only if valid (so that
        // there is no highlight for invalid cells that have no error
        // message when the mouse is released)
        @Override
        protected Object getCell(MouseEvent e) {
          Object cell = super.getCell(e);

          if (isConnecting()) {
            if (source != null) {
              error = validateConnection(source.getCell(), cell);

              if (error != null && error.length() == 0) {
                cell = null;

                // Enables create target inside groups
                if (createTarget) {
                  error = null;
                }
              }
            }
          } else if (!isValidSource(cell)) {
            cell = null;
          }

          return cell;
        }

        // Sets the highlight color according to isValidConnection
        @Override
        protected boolean isValidState(mxCellState state) {
          if (isConnecting()) {
            return error == null;
          } else {
            return super.isValidState(state);
          }
        }

        // Overrides to use marker color only in highlight mode or for
        // target selection
        @Override
        protected Color getMarkerColor(MouseEvent e, mxCellState state, boolean isValid) {
          return (isHighlighting() || isConnecting()) ? super.getMarkerColor(e, state, isValid) : null;
        }

        // Overrides to use hotspot only for source selection otherwise
        // intersects always returns true when over a cell
        @Override
        protected boolean intersects(mxCellState state, MouseEvent e) {
          if (!isHighlighting() || isConnecting()) {
            return true;
          }

          return super.intersects(state, e);
        }
      };

    }

  }

}
