package org.ntic.view.graph;

import com.mxgraph.model.mxCell;
import com.mxgraph.shape.mxITextShape;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.view.mxInteractiveCanvas;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxPoint;
import com.mxgraph.util.mxUtils;
import com.mxgraph.view.mxCellState;
import org.javatuples.Pair;
import org.ntic.model.Vertex;

import java.awt.*;
import java.awt.geom.Ellipse2D;
import java.util.*;
import java.util.stream.Collectors;

public class KPNCanvas extends mxInteractiveCanvas {

  protected mxGraphComponent graphComponent;
  private HashMap<Pair<Vertex, Vertex>, Point[]> absolutePoints = new HashMap<>();

  public KPNCanvas(mxGraphComponent graphComponent) {
    this.graphComponent = graphComponent;
  }

  @Override
  public Object drawLabel(String text, mxCellState state, boolean html) {

    Map<String, Object> style = state.getStyle();
    mxITextShape shape = this.getTextShape(style, html);
    if (this.g != null && shape != null) {

      if (((mxCell) state.getCell()).isVertex()) {
        text = text.trim();
        if (!text.equals(Boolean.TRUE.toString()) && !text.equals(Boolean.FALSE.toString())) {
          return super.drawLabel(text, state, html);
        }
        boolean marking = Boolean.parseBoolean(text);

        float opacity = mxUtils.getFloat(style, mxConstants.STYLE_TEXT_OPACITY, 100.0F);
        Graphics2D previousGraphics = this.g;
        this.g = this.createTemporaryGraphics(style, opacity, null);
        Color bg = mxUtils.getColor(style, mxConstants.STYLE_LABEL_BACKGROUNDCOLOR);
        Color border = mxUtils.getColor(style, mxConstants.STYLE_LABEL_BORDERCOLOR);
        this.paintRectangle(state.getLabelBounds().getRectangle(), bg, border);
        shape.paintShape(this, "", state, style);

        if (marking) {
          int width = (int) (state.getWidth()) / 8 * 3;
          int x = (int) (state.getWidth() / 2) - width / 2, y = (int) (state.getWidth() / 2) - width / 2;

          //g.setColor(Color.getHSBColor((float) Math.random() * 1000 - 800, (float) Math.random() * 1000 - 800, (float) Math.random() * 1000 - 800));
          //g.fillRect((int) ((state.getX() + x)), (int) ((state.getY() + y)), width, width);
          g.fill(new Ellipse2D.Double((int) ((state.getX() + x)), (int) ((state.getY() + y)), width, width));
        }

        this.g.dispose();
        this.g = previousGraphics;

      } else {
        return super.drawLabel(text, state, html);
      }

    }
    return shape;
  }

  @Override
  public Object drawCell(mxCellState state) {

    final mxCell cell = (mxCell) state.getCell();

    if (cell.isEdge()) {

      KPetriNetGraph graph = (KPetriNetGraph) graphComponent.getGraph();
      Vertex source = graph.getGraphPainter().getVertex((mxCell) cell.getSource());
      Vertex target = graph.getGraphPainter().getVertex((mxCell) cell.getTarget());

      if (graph.getArcsPositions()!=null){
        HashMap<Pair<Vertex, Vertex>, Point[]> arcsPositions = new HashMap<>(graph.getArcsPositions());
        if (arcsPositions.size() != 0) {
          absolutePoints = new HashMap<>();
          arcsPositions.forEach((key, points) -> {

            if (points != null) {
              //getting arc positions when it is loaded from the file. This code only runs once.
              absolutePoints.put(key, points);
              graph.getArcsPositions().remove(key);

            }
          });

          Point[] points = absolutePoints.get(new Pair<>(source, target));
          if (points != null) {
            state.setAbsolutePoints(Arrays.stream(points).map(p -> new mxPoint(p.x, p.y)).collect(Collectors.toList()));
          }

        }

      }

      if (graph.isCellLocked(cell)) {
        if (graph.getPetriNet().getEdge(target, source) != null) {
          Point[] points = absolutePoints.get(new Pair<>(target, source));
          if (points != null) {
            state.setAbsolutePoints(Arrays.stream(points).map(p -> new mxPoint(p.x, p.y)).collect(Collectors.toList()));
          }else{
            mxCell edge2 = graph.getGraphPainter().getCellFromArc(graph.getPetriNet().getEdge(target, source));
            //state.setAbsolutePoints(graph.getView().getState(edge2).getAbsolutePoints());

            ArrayList<mxPoint> newAbsolutePoints = new ArrayList<>(graph.getView().getState(edge2).getAbsolutePoints());
            Collections.reverse(newAbsolutePoints);
            state.setAbsolutePoints(newAbsolutePoints);
          }

        }

      } else {
        Point[] points = absolutePoints.get(new Pair<>(source, target));
        if (points != null) {
          state.setAbsolutePoints(Arrays.stream(points).map(p -> new mxPoint(p.x, p.y)).collect(Collectors.toList()));
        }
      }

    }
    return super.drawCell(state);
  }

  @Override
  public boolean contains(mxGraphComponent graphComponent, Rectangle rect, mxCellState state) {
    return state != null && state.getX() - state.getWidth() / 4 >= rect.x && state.getY() - state.getHeight() / 4 >= rect.y && state.getX() + state.getWidth() / 4 <= rect.x + rect.width && state.getY() + state.getHeight() / 4 <= rect.y + rect.height;
  }

  public void updateAbsolutePoints(mxCell cell) {

    KPetriNetGraph graph = (KPetriNetGraph) graphComponent.getGraph();
    Vertex source = graph.getGraphPainter().getVertex((mxCell) cell.getSource());
    Vertex target = graph.getGraphPainter().getVertex((mxCell) cell.getTarget());

    absolutePoints.remove(new Pair<>(source, target));

  }

}
