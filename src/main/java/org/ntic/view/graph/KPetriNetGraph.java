package org.ntic.view.graph;

import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxEvent;
import com.mxgraph.view.mxGraph;
import org.javatuples.Pair;
import org.jgrapht.event.GraphListener;
import org.ntic.model.*;
import org.ntic.view.editor.simulation.AutomaticSimulator;
import org.ntic.view.editor.simulation.ManualSimulator;
import org.ntic.view.editor.simulation.Simulator;
import org.ntic.view.graph.listeners.AddModeListener;
import org.ntic.view.graph.listeners.SimulationListener;
import org.ntic.view.graph.listeners.UndoChangeListener;

import java.awt.*;
import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;
import java.util.*;

public class KPetriNetGraph extends mxGraph {

  public static final String MODE = "Mode";
  public static final String SIMULATION = "Simulation";
  private final ArrayList<PropertyChangeListener> listeners = new ArrayList<>();
  private final PropertyChangeListener simulationStateListener = new SimulationListener(this);
  private final ArrayList<GraphListener<Vertex, Arc>> graphListeners = new ArrayList<>();
  private final KPNGraphComponent graphComponent;
  private final AgentManager agentManager;
  private Mode mode;
  private KPetriNet petriNet;
  private Simulator simulator;
  private GraphPainter graphPainter = new GraphPainter(this);
  private HashMap<Pair<Vertex, Vertex>, Point[]> arcsPositions = new HashMap<>();

  public KPetriNetGraph(KPetriNet petriNet, HashMap<Vertex, Point> positions) {
    if (petriNet != null) {

      this.allowNegativeCoordinates = false;

      this.petriNet = petriNet;
      this.agentManager = new AgentManager();
      this.graphComponent = new KPNGraphComponent(this);

      this.setAllowDanglingEdges(false);
      this.setCellsResizable(false);
      this.setEdgeLabelsMovable(false);
      this.getStylesheet().getDefaultEdgeStyle().put(mxConstants.STYLE_EDGE, mxConstants.EDGESTYLE_ORTHOGONAL);

      addGraphListener(new KPNGraphListener(this));
      if (positions != null) {
        graphPainter.drawPetriNet(petriNet, positions::get);
      } else {
        graphPainter.drawPetriNet(petriNet, v -> graphPainter.getVertices().get(v).getGeometry().getPoint());
      }

      graphComponent.getGraphControl().addMouseListener(new AddModeListener(this));
      getModel().addListener(mxEvent.CHANGE, new UndoChangeListener(this));

    } else {
      throw new IllegalArgumentException();
    }
  }

  public KPetriNetGraph(KPetriNet petriNet) {
    this(petriNet, null);
  }

  public KPNGraphComponent getGraphComponent() {
    return this.graphComponent;
  }

  public KPetriNet getPetriNet() {
    return petriNet;
  }

  public AgentManager getAgentManager() {
    return agentManager;
  }

  public GraphPainter getGraphPainter() {
    return graphPainter;
  }

  public Mode getMode() {
    return mode;
  }

  public void setMode(Mode mode) {
    if (this.getMode() != Mode.SIMULATION || mode == Mode.SIMULATION || mode == null) {
      notifyListeners(MODE, this.mode, this.mode = mode);

      if (mode == null || mode == Mode.SIMULATION) {
        graphComponent.setCustomCursor(null);
      } else if (mode == Mode.ARC) {
        graphComponent.setCustomCursor(new Cursor(Cursor.HAND_CURSOR));
      } else {
        graphComponent.setCustomCursor(new Cursor(Cursor.CROSSHAIR_CURSOR));
      }
    }
  }

  public HashMap<Pair<Vertex, Vertex>, Point[]> getArcsPositions() {
    return arcsPositions;
  }

  public void notifyListeners(String property, Object oldValue, Object newValue) {
    for (PropertyChangeListener name : listeners) {
      name.propertyChange(new PropertyChangeEvent(this, property, oldValue, newValue));
    }
  }

  public void addChangeListener(PropertyChangeListener newListener) {
    listeners.add(newListener);
  }

  public void updateSimulator(Simulator simulator) {
    this.simulator = simulator;
    this.simulator.setG(petriNet);
    this.simulator.addChangeListener(simulationStateListener);
  }

  public void updateSimulator() {
    this.simulator = this.simulator instanceof AutomaticSimulator ? new AutomaticSimulator(petriNet) : new ManualSimulator(petriNet);
    this.simulator.setG(petriNet);
    this.simulator.addChangeListener(simulationStateListener);
  }

  public Simulator getSimulator() {
    return simulator;
  }

  public boolean updateParentName(mxCell cell, Object value) {
    value = value.toString().trim().toLowerCase();
    Vertex parent = graphPainter.getVertex((mxCell) cell.getParent());
    if (parent != null && !parent.getName().equals(value.toString())) {
      return value.toString().matches("[a-z][a-z0-9_,]+") && petriNet.updateName(parent, value.toString().toLowerCase());
    }
    return true;
  }

  public void updateState(KPetriNet petriNet, HashMap<Vertex, Point> positions, HashMap<Pair<Vertex, Vertex>, Point[]> arcPositions) {
    this.removeCells(graphPainter.getVertices().values().toArray());

    agentManager.removeAllAssociations();
    this.petriNet = petriNet;
    for (GraphListener<Vertex, Arc> listener : graphListeners) {
      petriNet.removeGraphListener(listener);
      petriNet.addGraphListener(listener);
    }
    updateSimulator();

    graphPainter = new GraphPainter(this);

    this.arcsPositions = arcPositions;
    setMode(null);
    if (positions != null) {
      graphPainter.drawPetriNet(petriNet, positions::get);
    } else {
      graphPainter.drawPetriNet(petriNet, v -> graphPainter.getVertices().get(v).getGeometry().getPoint());
    }
  }

  public void addGraphListener(GraphListener<Vertex, Arc> listener) {
    petriNet.addGraphListener(listener);
    graphListeners.add(listener);
  }

  @Override
  public Object[] moveCells(Object[] cells, double dx, double dy, boolean clone, Object target, Point location) {

    if (clone) {

      //*************

      //In case of copy/pasting cells
      //we copy first all vertices

      HashMap<mxCell, Vertex> copiedVertices = new HashMap<>();

      for (Object cell : cells) {
        if (((mxCell) cell).isVertex()) {
          Vertex vertex = petriNet.getVertexByName(((mxCell) cell).getId());
          if (vertex != null) {
            Point position;
            position = Objects.requireNonNullElseGet(location, () -> new Point(graphPainter.getVertices().get(vertex).getGeometry().getPoint()));

            if (vertex instanceof Transition) {
              petriNet.addTransitionWithCallable(t -> {
                copiedVertices.put((mxCell) cell, t);
                graphPainter.drawVertexAdjustedPosition(t, (position.x + 50), (position.y + 50));
              });
            } else if (vertex instanceof StatePlace) {
              petriNet.addStatePlaceWithCallable(p -> {
                copiedVertices.put((mxCell) cell, p);
                ((Place) p).setInitiallyMarked(((Place) vertex).isInitiallyMarked());
                graphPainter.drawVertexAdjustedPosition(p, (position.x + 50), (position.y + 50));
              });
            } else if (vertex instanceof KnowledgePlace) {
              petriNet.addKnowledgePlaceWithCallable(p -> {
                copiedVertices.put((mxCell) cell, p);
                ((Place) p).setInitiallyMarked(((Place) vertex).isInitiallyMarked());
                graphPainter.drawVertexAdjustedPosition(p, (position.x + 50), position.y + 50);
              });
            }
          }
        }
      }

      //then we copy edges when both ends are also copied
      for (Object cell : cells) {
        if (((mxCell) cell).isEdge() && ((mxCell) cell).getTarget() != null && ((mxCell) cell).getSource() != null) {
          Vertex edgeTarget = copiedVertices.get(((mxCell) cell).getTarget());
          Vertex edgeSource = copiedVertices.get(((mxCell) cell).getSource());
          if (!petriNet.containsEdge(edgeSource, edgeTarget)) {
            petriNet.addEdge(edgeSource, edgeTarget);
          }
        }
      }

      List<mxCell> cellList = new ArrayList<>();
      for (mxCell key : copiedVertices.keySet()) {
        cellList.add(graphPainter.getVertices().get(copiedVertices.get(key)));
      }
      //***************

      cells = cellList.toArray();

    }
    return super.moveCells(cells, dx, dy, false, target, location);

  }

  //Labels are not used as terminals for edges, they are only used to compute the graphical connection point
  @Override
  public boolean isPort(Object cell) {
    mxGeometry geo = getCellGeometry(cell);
    return (geo != null) && geo.isRelative();
  }

  @Override
  public boolean isCellLocked(Object cell) {
    //When we have 2 arcs with the same terminals, we lock one of them to prevent moving it away from the other one
    // User can only move one arc and the other one will follow, so he will get the feeling that it's one double-sided arc
    Object[] edges = getEdgesBetween(((mxCell) cell).getSource(), ((mxCell) cell).getTarget());
    if (edges.length > 1) {
      if (cell == edges[0]) {
        return true;
      }
    }
    return super.isCellLocked(cell);
  }

  // Removes the folding icon and disables any folding
  @Override
  public boolean isCellFoldable(Object cell, boolean collapse) {
    return false;
  }

  @Override
  public void cellLabelChanged(Object cell, Object value, boolean autoSize) {
    if (updateParentName((mxCell) cell, value))
      super.cellLabelChanged(cell, value.toString().toLowerCase(), autoSize);
  }

  @Override
  public void cellsRemoved(Object[] cells) {
    for (Object o : cells) {
      if (o != null) {
        mxCell cell = (mxCell) o;
        if (cell.isVertex() && graphPainter.getVertices().containsValue(cell)) {
          petriNet.removeVertex(graphPainter.getVertex((mxCell) o));
        } else if (graphPainter.getArc((mxCell) o) != null) {
          petriNet.removeEdge(graphPainter.getArc((mxCell) o));
        }
      }
    }
    super.cellsRemoved(cells);
  }

  @Override
  public void cellsMoved(Object[] cells, double dx, double dy, boolean disconnect, boolean constrain) {
    int maxX = 0, maxY = 0;
    for (Object cell : cells) {
      final mxCell cell1 = (mxCell) cell;
      if (cell1.isEdge()) {
        ((KPNCanvas) graphComponent.getCanvas()).updateAbsolutePoints(cell1);
      } else {
        Arrays.stream(getEdges(cell)).forEach(e -> ((KPNCanvas) graphComponent.getCanvas()).updateAbsolutePoints((mxCell) e));
      }
      maxX = (int) Math.max(maxX, cell1.getGeometry().getX() + dx);
      maxY = (int) Math.max(maxY, cell1.getGeometry().getY() + dy);
    }
    graphPainter.resizeGraphComponentView(maxX, maxY);
    super.cellsMoved(cells, dx, dy, disconnect, constrain);
  }

  @Override
  public void cellsAdded(Object[] cells, Object parent, Integer index, Object source, Object target, boolean absolute, boolean constrain) {
    if (source != null && target != null) {
      if (!petriNet.containsEdge(graphPainter.getVertex((mxCell) source), graphPainter.getVertex((mxCell) target))) {
        petriNet.addEdge(graphPainter.getVertex((mxCell) source), graphPainter.getVertex((mxCell) target));
        repaint();
        return;
      }
    }
    super.cellsAdded(cells, parent, index, source, target, absolute, constrain);
  }

  @Override
  public boolean isValidConnection(Object source, Object target) {
    Vertex v1 = graphPainter.getVertex((mxCell) source);
    Optional<Map.Entry<Vertex, mxCell>> o = graphPainter.getVertices().entrySet().stream().filter(a -> a.getValue() == target).findFirst();
    return o.map(entry -> petriNet.validateConnection(v1, entry.getKey())).orElseGet(() -> super.isValidConnection(source, target));
  }

  @Override
  public boolean isValidSource(Object cell) {
    if (getMode() != Mode.ARC)
      return false;
    if (!graphPainter.getVertices().containsValue(cell))
      return false;
    return super.isValidSource(cell);
  }

  @Override
  public boolean isValidDropTarget(Object cell, Object[] cells) {
    mxCell c = (mxCell) cell;
    if (graphPainter.getVertices().containsValue(c)) {
      return false;
    }
    return super.isValidDropTarget(cell, cells);
  }

  @Override
  public boolean isCellEditable(Object cell) {
    mxCell c = (mxCell) cell;
    return getMode() != Mode.SIMULATION && (!graphPainter.getVertices().containsValue(c) && graphPainter.getArc(c) == null && !this.graphPainter.getAgentsLabels().containsValue(cell));
  }

  @Override
  public boolean isCellSelectable(Object cell) {
    return getMode() != Mode.ARC && getMode() != Mode.SIMULATION && (!this.graphPainter.getAgentsLabels().containsValue(cell) || super.isCellSelectable(cell));
  }

  @Override
  public boolean isCellMovable(Object cell) {
    return getMode() == null && super.isCellMovable(cell);
  }

  @Override
  public boolean isCellDeletable(Object cell) {
    mxCell c = (mxCell) cell;
    return super.isCellDeletable(cell) && (graphPainter.getVertices().containsValue(c) || graphPainter.getArc(c) != null);
  }

  @Override
  public boolean isCellsSelectable() {
    return !(getMode() == Mode.SIMULATION && super.isCellsSelectable());
  }

  @Override
  public String toString() {
    return getPetriNet().getName();
  }

  public enum Mode {
    TRANSITION,
    STATE_PLACE,
    KNOWLEDGE_PLACE,
    ARC,
    DELETE,
    SIMULATION
  }

  public interface PositionGetter {

    Point call(Vertex vertex);

  }

}
