package org.ntic.view.graph;

import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import com.mxgraph.util.mxPoint;
import com.mxgraph.util.mxRectangle;
import org.ntic.model.*;
import org.ntic.view.View;
import org.ntic.view.graph.listeners.AgentListener;
import org.ntic.view.graph.listeners.FiringListener;
import org.ntic.view.graph.listeners.MarkingListener;

import java.awt.*;
import java.beans.PropertyChangeListener;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;
import java.util.Optional;

public class GraphPainter {

  public static final int PLACE_DIAMETER = 40;
  public static final int TRANSITION_WIDTH = 40;
  public static final int TRANSITION_HEIGHT = 16;
  public static final int ADDITIONAL_DRAWING_SPACE = 500;
  private final HashMap<Vertex, mxCell> vertices = new HashMap<>();
  private final HashMap<mxCell, Arc> cellsToArcs = new HashMap<>();
  private final HashMap<Arc, mxCell> arcsToCells = new HashMap<>();
  private final HashMap<KnowledgePlace, mxCell> agentsLabels = new HashMap<>();
  private final PropertyChangeListener markingListener = new MarkingListener(this);
  private final PropertyChangeListener firingListener = new FiringListener(this);
  private final PropertyChangeListener agentListener = new AgentListener(this);
  private final KPetriNetGraph graph;
  private mxRectangle graphSize;

  public GraphPainter(KPetriNetGraph graph) {
    this.graph = graph;
  }

  public void drawArc(Arc arc) {
    if (arcsToCells.get(arc) != null)
      return;
    graph.getModel().beginUpdate();
    try {
      mxCell g = (mxCell) graph.insertEdge(graph.getDefaultParent(), null, "", vertices.get(arc.getSource()), vertices.get(arc.getTarget()), "strokeColor=#279CEB;strokeWidth=3");
      arcsToCells.put(arc, g);
      cellsToArcs.put(g, arc);
      resizeGraphComponentView((int) g.getGeometry().getX(), (int) g.getGeometry().getY());
    } finally {
      graph.getModel().endUpdate();
    }

  }

  private void drawLabel(mxCell vertex, String text) {

    mxGeometry geo2 = new mxGeometry(0, 0, PLACE_DIAMETER, 20);
    geo2.setOffset(new mxPoint(-PLACE_DIAMETER + 4, vertex.getValue() instanceof Boolean ? -8 : -16));
    geo2.setRelative(true);

    mxCell label = new mxCell(text, geo2, "fillColor=none;strokeColor=none;fontColor=#434343;align=right");
    label.setVertex(true);
    label.setConnectable(false);
    graph.addCell(label, vertex);
  }

  private void drawAgentsLabel(mxCell vertex, String text) {

    mxGeometry geo2 = new mxGeometry(0, 0, PLACE_DIAMETER, 20);
    geo2.setOffset(new mxPoint(PLACE_DIAMETER - 4, vertex.getValue() instanceof Boolean ? -8 : -16));
    geo2.setRelative(true);

    mxCell label = new mxCell(text, geo2, "fillColor=none;strokeColor=none;fontColor=#279CEB;align=left");
    label.setVertex(true);
    label.setConnectable(false);
    graph.addCell(label, vertex);
    agentsLabels.put((KnowledgePlace) getVertex(vertex), label);
  }

  public void drawVertexAdjustedPosition(Vertex vertex, int x, int y) {
    drawVertex(vertex, x, y, true);
  }

  public void drawVertex(Vertex vertex) {
    int x = (int) (Math.random() * 0.8 * View.WIDTH + PLACE_DIAMETER), y = (int) (Math.random() * 0.8 * View.HEIGHT + PLACE_DIAMETER);
    drawVertex(vertex, x, y, false);
  }

  private void installListeners(Vertex vertex) {
    if (vertex instanceof StatePlace) {
      vertex.addChangeListener(markingListener);
    } else if (vertex instanceof Transition) {
      vertex.addChangeListener(firingListener);
    } else if (vertex instanceof KnowledgePlace) {
      vertex.addChangeListener(markingListener);
      graph.getAgentManager().addAgentListener((KnowledgePlace) vertex, agentListener);
    }
  }

  private void drawVertex(Vertex vertex, int x, int y, boolean adjustPosition) {
    int width = 0, height = 0;
    Object value = null;
    String style = "";
    switch (vertex.getType()) {
      case STATE_PLACE -> {
        width = PLACE_DIAMETER;
        height = PLACE_DIAMETER;
        value = ((Place) vertex).isMarked();
        style = "shape=ellipse;perimeter=ellipsePerimeter;strokeColor=#279CEB;strokeWidth=3;fillColor=white;fontColor=#434343";
      }
      case KNOWLEDGE_PLACE -> {
        width = PLACE_DIAMETER;
        height = PLACE_DIAMETER;
        value = ((Place) vertex).isMarked();
        style = "shape=ellipse;perimeter=ellipsePerimeter;strokeColor=#279CEB;strokeWidth=3;fillColor=#7DD5F5;fontColor=#434343";
      }
      case TRANSITION -> {
        width = TRANSITION_WIDTH;
        height = TRANSITION_HEIGHT;
        style = TransitionStyle.NOT_FIRABLE.getStyle();
      }
    }

    if (vertices.get(vertex) != null)
      return;
    graph.getModel().beginUpdate();

    try {
      if (adjustPosition) {
        int offsetX = (width / 2), offsetY = (height / 2);
        x = (int) (x / graph.getView().getScale()) - offsetX;
        y = (int) (y / graph.getView().getScale()) - offsetY;
      }

      mxCell g = (mxCell) graph.insertVertex(graph.getDefaultParent(), vertex.getName(), value, x, y, width, height, style);
      vertices.put(vertex, g);
      drawLabel(g, vertex.getName());

      if (vertex instanceof KnowledgePlace) {
        drawAgentsLabel(g, graph.getAgentManager().getAgentLabelText((KnowledgePlace) vertex));
      }

      installListeners(vertex);
      resizeGraphComponentView(x, y);
    } finally {
      graph.getModel().endUpdate();
    }

  }

  public void updatePlace(Place place) {
    if (place.isMarked() != (Boolean) graph.getModel().getValue(vertices.get(place))) {
      graph.getModel().beginUpdate();
      try {
        graph.getModel().setValue(vertices.get(place), place.isMarked());
      } finally {
        graph.getModel().endUpdate();
      }
    }
  }

  public void eraseVertex(Vertex vertex) {
    graph.removeCells(new Object[]{vertices.get(vertex)});
    vertices.remove(vertex);
    Object[] edges = graph.getEdges(vertex);

    for (Object o : edges) {
      mxCell removedEdge = (mxCell) o;
      eraseEdge(cellsToArcs.get(removedEdge));
    }

    if (vertex instanceof Place) {
      vertex.removeChangeListener(markingListener);
    } else if (vertex instanceof Transition) {
      vertex.removeChangeListener(firingListener);
    }
  }

  public void eraseEdge(Arc edge) {
    mxCell cell = arcsToCells.get(edge);
    graph.removeCells(new Object[]{cell});
    cellsToArcs.remove(arcsToCells.remove(edge));
  }

  public void updateAgentLabel(KnowledgePlace place, String text) {
    graph.getModel().beginUpdate();
    try {
      graph.getModel().setValue(agentsLabels.get(place), text);
    } finally {
      graph.getModel().endUpdate();
    }
  }

  public void drawPetriNet(KPetriNet petriNet, KPetriNetGraph.PositionGetter positionGetter) {
    Iterator<Vertex> vertexIterator = petriNet.vertexSet().iterator();

    Vertex vertex;
    while (vertexIterator.hasNext()) {
      vertex = vertexIterator.next();
      Point point = positionGetter.call(vertex);
      if (point == null) {
        drawVertex(vertex);
      } else {
        drawVertex(vertex, point.x, point.y, false);
      }
      if (vertex instanceof Place) {
        vertex.addChangeListener(markingListener);
      } else if (vertex instanceof Transition) {
        if (vertex.getListener().isEmpty())
          vertex.addChangeListener(firingListener);
      }
    }

    Iterator<Arc> edgeIterator = petriNet.edgeSet().iterator();

    Arc arc;
    while (edgeIterator.hasNext()) {
      arc = edgeIterator.next();
      drawArc(arc);
    }
  }

  public void setTransitionStyle(TransitionStyle style, Transition transition) {
    graph.setCellStyle(style.getStyle(), new Object[]{vertices.get(transition)});
  }

  public void resizeGraphComponentView(int x, int y) {
    if (graphSize == null) {
      graphSize = new mxRectangle(0, 0, graph.getGraphComponent().getViewport().getView().getSize().width, graph.getGraphComponent().getViewport().getView().getSize().height);
    }

    if ((x + ADDITIONAL_DRAWING_SPACE) > graphSize.getWidth()) {
      graphSize.setWidth(x + ADDITIONAL_DRAWING_SPACE);
    }

    if ((y + ADDITIONAL_DRAWING_SPACE) > graphSize.getHeight()) {
      graphSize.setHeight(y + ADDITIONAL_DRAWING_SPACE);
    }

    graph.setMinimumGraphSize(graphSize);
  }

  public Vertex getVertex(mxCell cell) {
    Optional<Map.Entry<Vertex, mxCell>> optional = vertices.entrySet().stream().filter(a -> a.getValue() == cell).findFirst();
    return optional.map(Map.Entry::getKey).orElse(null);
  }

  public Point getVertexPosition(Vertex vertex) {
    return vertices.get(vertex).getGeometry().getPoint();
  }

  public HashMap<Vertex, mxCell> getVertices() {
    return vertices;
  }

  public Arc getArc(mxCell cell) {
    return cellsToArcs.get(cell);
  }

  public mxCell getCellFromArc(Arc arc) {
    return arcsToCells.get(arc);
  }

  public KPetriNetGraph getGraph() {
    return graph;
  }

  public HashMap<KnowledgePlace, mxCell> getAgentsLabels() {
    return agentsLabels;
  }

  public HashMap<Arc, mxCell> getArcsToCells() {
    return arcsToCells;
  }

  public enum TransitionStyle {
    FIRABLE("strokeColor=#3AC375;strokeWidth=3;fillColor=#B0FFEB"),
    NOT_FIRABLE("strokeColor=#3AC375;strokeWidth=3;fillColor=white");

    private final String style;

    TransitionStyle(String s) {
      this.style = s;
    }

    public String getStyle() {
      return style;
    }
  }

}
