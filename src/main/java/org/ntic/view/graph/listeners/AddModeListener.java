package org.ntic.view.graph.listeners;

import com.mxgraph.model.mxCell;
import org.ntic.model.Place;
import org.ntic.model.Transition;
import org.ntic.model.Vertex;
import org.ntic.view.editor.EditorVertexPopupMenu;
import org.ntic.view.editor.simulation.ManualSimulator;
import org.ntic.view.editor.simulation.Simulator;
import org.ntic.view.graph.KPetriNetGraph;

import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;

import static org.ntic.Main.editor;

public class AddModeListener extends MouseAdapter {

  private final KPetriNetGraph graph;

  public AddModeListener(KPetriNetGraph graph) {
    this.graph = graph;
  }

  @Override
  public void mouseReleased(MouseEvent e) {
    if (e.getClickCount() >= 2) {
      mxCell cell = (mxCell) graph.getGraphComponent().getCellAt(e.getX(), e.getY());
      Vertex vertex = graph.getGraphPainter().getVertex(cell);
      if (graph.getMode() == null) {
        if (vertex instanceof Place) {
          ((Place) vertex).toggleInitiallyMarked();
          return;
        }
      }
      if (vertex instanceof Transition && graph.getMode() == KPetriNetGraph.Mode.SIMULATION && graph.getSimulator().getSimulationMode() == Simulator.SimulationMode.MANUAL) {
        if (((Transition) vertex).isFireable())
          ((ManualSimulator) graph.getSimulator()).scheduleForFiring((Transition) vertex);
      }

    } else {
      if (e.isPopupTrigger()) {
        if (graph.getMode() != KPetriNetGraph.Mode.SIMULATION) {
          EditorVertexPopupMenu menu = new EditorVertexPopupMenu(editor);
          menu.show(e.getComponent(), e.getX(), e.getY());
        }
      } else if (graph.getMode() != null) {
        switch (graph.getMode()) {
          case TRANSITION -> graph.getPetriNet().addTransitionWithCallable(t -> graph.getGraphPainter().drawVertexAdjustedPosition(t, e.getX(), e.getY()));
          case STATE_PLACE -> graph.getPetriNet().addStatePlaceWithCallable(p -> graph.getGraphPainter().drawVertexAdjustedPosition(p, e.getX(), e.getY()));
          case KNOWLEDGE_PLACE -> graph.getPetriNet().addKnowledgePlaceWithCallable(p -> graph.getGraphPainter().drawVertexAdjustedPosition(p, e.getX(), e.getY()));
          case DELETE -> {
            mxCell cell = (mxCell) graph.getGraphComponent().getCellAt(e.getX(), e.getY());
            if (cell != null) {
              if (cell.isVertex() && graph.getGraphPainter().getVertices().containsValue(cell)) {
                graph.getPetriNet().removeVertex(graph.getGraphPainter().getVertices().entrySet().stream().filter(a -> a.getValue() == cell).findFirst().get().getKey());
              } else if (cell.isEdge()) {
                graph.getPetriNet().removeEdge(graph.getGraphPainter().getArc(cell));
              }
            }
          }
        }
      }
    }

  }

}
