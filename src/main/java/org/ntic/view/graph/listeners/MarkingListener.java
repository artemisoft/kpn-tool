package org.ntic.view.graph.listeners;

import org.ntic.model.Place;
import org.ntic.view.graph.GraphPainter;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

public class MarkingListener implements PropertyChangeListener {

  private final GraphPainter graphPainter;

  public MarkingListener(GraphPainter graphPainter) {
    this.graphPainter = graphPainter;
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    graphPainter.updatePlace((Place) evt.getSource());
  }

}
