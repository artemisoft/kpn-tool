package org.ntic.view.graph.listeners;

import org.ntic.model.Transition;
import org.ntic.view.graph.KPetriNetGraph;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.Map;

import static org.ntic.view.graph.KPetriNetGraph.SIMULATION;

public class SimulationListener implements PropertyChangeListener {

  private final KPetriNetGraph graph;

  public SimulationListener(KPetriNetGraph graph) {
    this.graph = graph;
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {

    if (graph.getSimulator() != null) {
      graph.notifyListeners(SIMULATION, evt.getOldValue(), evt.getNewValue());
      switch (graph.getSimulator().getSimulationState()) {
        case NEW -> {

          graph.setMode(null);
          graph.setCellStyle("strokeColor=#3AC375;strokeWidth=3;fillColor=white", graph.getGraphPainter().getVertices().entrySet().stream().filter(e -> e.getKey() instanceof Transition).map(Map.Entry::getValue).toArray());
        }
        case RUNNING, PAUSED, STOPPED -> {
          if (graph.getMode() != KPetriNetGraph.Mode.SIMULATION) {
            graph.setMode(KPetriNetGraph.Mode.SIMULATION);
          }
        }
      }
    }
  }

}
