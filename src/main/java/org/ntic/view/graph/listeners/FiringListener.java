package org.ntic.view.graph.listeners;

import org.ntic.model.Transition;
import org.ntic.view.graph.GraphPainter;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;

import static org.ntic.view.graph.GraphPainter.TransitionStyle.FIRABLE;
import static org.ntic.view.graph.GraphPainter.TransitionStyle.NOT_FIRABLE;

public class FiringListener implements PropertyChangeListener {

  private final GraphPainter graphPainter;

  public FiringListener(GraphPainter graphPainter) {
    this.graphPainter = graphPainter;
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (evt.getNewValue() != evt.getOldValue()) {
      Transition transition = (Transition) evt.getSource();
      if (transition.isFireable()) {
        graphPainter.setTransitionStyle(FIRABLE, transition);
      } else {
        graphPainter.setTransitionStyle(NOT_FIRABLE, transition);
      }
    }
  }

}
