package org.ntic.view.graph.listeners;

import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGraphModel;
import com.mxgraph.util.mxEventObject;
import com.mxgraph.util.mxEventSource;
import org.ntic.model.Arc;
import org.ntic.model.Place;
import org.ntic.model.Vertex;
import org.ntic.view.graph.KPetriNetGraph;

import java.util.ArrayList;
import java.util.HashMap;

public class UndoChangeListener implements mxEventSource.mxIEventListener {

  private final HashMap<mxCell, Vertex> vertices = new HashMap<>();
  private final HashMap<mxCell, Arc> arcs = new HashMap<>();
  KPetriNetGraph graph;

  public UndoChangeListener(KPetriNetGraph graph) {
    this.graph = graph;
  }

  @Override
  public void invoke(Object sender, mxEventObject evt) {

    for (Object o : (ArrayList) evt.getProperties().get("changes")) {
      if (o instanceof mxGraphModel.mxValueChange) {
        mxCell cell = (mxCell) ((mxGraphModel.mxValueChange) o).getCell();
        if (graph.getGraphPainter().getVertices().containsValue(cell)) {
          Vertex vertex = graph.getGraphPainter().getVertex(cell);
          if (vertex instanceof Place && ((Place) vertex).isMarked() != (Boolean) cell.getValue()) {
            ((Place) vertex).setInitiallyMarked((Boolean) cell.getValue());
          }
        } else if (cell.isVertex() && !graph.getGraphPainter().getAgentsLabels().containsValue(cell) && (graph.getGraphPainter().getVertex((mxCell) cell.getParent())).getName() != cell.getValue()) {
          graph.updateParentName(cell, cell.getValue());
        }

      } else if (o instanceof mxGraphModel.mxChildChange) {
        mxCell child = (mxCell) ((mxGraphModel.mxChildChange) o).getChild();
        if (((mxGraphModel.mxChildChange) o).getPrevious() != null) {

          if (child.isVertex() && !child.getGeometry().isRelative()) {
            vertices.put(child, graph.getGraphPainter().getVertex(child));
          } else if (child.isEdge()) {
            arcs.put(child, graph.getGraphPainter().getArc(child));
          }

          //deleted
          if (child.isEdge() && graph.getPetriNet().containsEdge(graph.getGraphPainter().getArc(child))) {
            graph.getPetriNet().removeEdge(graph.getGraphPainter().getArc(child));
          } else if (child.isVertex() && graph.getPetriNet().containsVertex(graph.getGraphPainter().getVertex(child))) {
            graph.getPetriNet().removeVertex(graph.getGraphPainter().getVertex(child));
          }

        } else {
          //added
          if (child.isEdge() && !graph.getPetriNet().containsEdge(graph.getGraphPainter().getArc(child))) {
            graph.getPetriNet().addEdge(graph.getGraphPainter().getVertex((mxCell) child.getSource()), graph.getGraphPainter().getVertex((mxCell) child.getTarget()));
          } else {
            if (child.isVertex() && vertices.get(child) != null) {
              Vertex vertex = vertices.get(child);
              graph.getPetriNet().addVertexWithCallable(vertex, v -> graph.getGraphPainter().getVertices().put(v, child));
            } else if (child.isEdge() && arcs.get(child) != null) {
              Arc arc = arcs.get(child);
              graph.getGraphPainter().getArcsToCells().put(arc, child);
              graph.getPetriNet().addEdge(arc.getSource(), arc.getTarget(), arc);
            }
          }
        }
      }
    }
  }

}
