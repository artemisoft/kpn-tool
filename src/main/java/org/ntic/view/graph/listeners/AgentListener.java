package org.ntic.view.graph.listeners;

import org.ntic.model.Agent;
import org.ntic.model.KnowledgePlace;
import org.ntic.view.graph.GraphPainter;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.List;

public class AgentListener implements PropertyChangeListener {

  private final GraphPainter graphPainter;

  public AgentListener(GraphPainter graphPainter) {
    this.graphPainter = graphPainter;
  }

  @Override
  public void propertyChange(PropertyChangeEvent evt) {
    if (evt.getNewValue() != evt.getOldValue()) {
      KnowledgePlace place = (KnowledgePlace) evt.getSource();

      List<Agent> associatedAgents = graphPainter.getGraph().getAgentManager().getAssociatedAgents(place);
      if (associatedAgents.size() == 0) {
        graphPainter.updateAgentLabel(place, "");
        return;
      }
      graphPainter.updateAgentLabel(place, graphPainter.getGraph().getAgentManager().getAgentLabelText(place));
    }
  }

}
