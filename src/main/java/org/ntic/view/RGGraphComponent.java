package org.ntic.view;

import com.mxgraph.model.mxCell;
import com.mxgraph.shape.mxITextShape;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.view.mxInteractiveCanvas;
import com.mxgraph.util.mxConstants;
import com.mxgraph.util.mxUtils;
import com.mxgraph.view.mxCellState;
import com.mxgraph.view.mxGraph;

import java.awt.*;
import java.awt.geom.RoundRectangle2D;
import java.util.Map;

public class RGGraphComponent extends mxGraphComponent {

  private static final long serialVersionUID = 4683716829748931448L;

  public RGGraphComponent(mxGraph graph) {
    super(graph);
    getViewport().setPreferredSize(new Dimension(512, 512));
    getViewport().setOpaque(true);
    getViewport().setBackground(Color.WHITE);
    setBorder(null);
  }

  @Override
  public mxInteractiveCanvas createCanvas() {
    return new mxInteractiveCanvas() {
      @Override
      public Object drawLabel(String text, mxCellState state, boolean html) {
        Map<String, Object> style = state.getStyle();
        mxITextShape shape = getTextShape(style, html);

        if (g != null && shape != null && drawLabels && text != null
            && text.length() > 0) {
          // Creates a temporary graphics instance for drawing this shape
          float opacity = mxUtils.getFloat(style,
              mxConstants.STYLE_TEXT_OPACITY, 100);
          Graphics2D previousGraphics = g;
          g = createTemporaryGraphics(style, opacity, null);

          Color bg = new Color(200, 200, 200);
          Color border = mxUtils.getColor(style,
              mxConstants.STYLE_LABEL_BORDERCOLOR);

          Rectangle rectangle = state.getLabelBounds().getRectangle();
          // Draws the label background and border
          if (!((mxCell) state.getCell()).isVertex()) {
            g.setPaint(bg);
            g.fill(new RoundRectangle2D.Double(rectangle.x, rectangle.y, rectangle.width, rectangle.height, 8, 8));
          } else {
            if (getGraph().getMarkings().containsValue(state.getCell())) {
              bg = mxUtils.getColor(style,
                  mxConstants.STYLE_LABEL_BACKGROUNDCOLOR);
              paintRectangle(rectangle, bg, border);
            } else {
              rectangle.y += ((mxCell) state.getCell()).getGeometry().getHeight() / 2 + 24;
              g.setPaint(bg);
              g.fill(new RoundRectangle2D.Double(rectangle.x, rectangle.y, rectangle.width, rectangle.height, 8, 8));
            }
          }

          // Paints the label and restores the graphics object
          Font font = new Font(mxConstants.DEFAULT_FONTFAMILY, Font.PLAIN, mxConstants.DEFAULT_FONTSIZE);
          FontMetrics metrics = g.getFontMetrics(font);
          // Determine the X coordinate for the text
          int x = rectangle.x + (rectangle.width - metrics.stringWidth(text)) / 2;
          // Determine the Y coordinate for the text (note we add the ascent, as in java 2d 0 is top of the screen)
          int y = rectangle.y + ((rectangle.height - metrics.getHeight()) / 2) + metrics.getAscent();
          // Set the font
          g.setFont(font);
          g.setPaint(new Color(67, 67, 67));
          // Draw the String
          g.drawString(state.getLabel(), x, y);
          g.dispose();
          g = previousGraphics;
        }

        return shape;
      }
    };
  }

  @Override
  public RGGraph getGraph() {
    return (RGGraph) super.getGraph();
  }

}
