package org.ntic.view.ui;

import javax.swing.*;
import javax.swing.event.DocumentEvent;
import javax.swing.event.DocumentListener;
import javax.swing.text.*;
import java.awt.*;

public class FormulaEditor extends JTextPane {

  public FormulaEditor(String str) {
    this.setPreferredSize(new Dimension(512, 128));
    this.setFont(new Font("JetBrains Mono Medium", Font.PLAIN, 16));
    this.setEditorKit(new MyEditorKit());
    this.setBorder(new JButton().getBorder());

    addCTLKOperatorStyle(this);
    addOperatorStyle(this);
    addDefaultStyle(this);

    try {
      SimpleAttributeSet attrs = new SimpleAttributeSet();
      StyleConstants.setAlignment(attrs, StyleConstants.ALIGN_CENTER);
      StyledDocument doc = (StyledDocument) this.getDocument();
      doc.setParagraphAttributes(0, doc.getLength() - 1, attrs, false);
      doc.addDocumentListener(new DocumentListener() {
        @Override
        public void insertUpdate(DocumentEvent e) {
          updateStyle(doc);
        }

        @Override
        public void removeUpdate(DocumentEvent e) {
          updateStyle(doc);
        }

        @Override
        public void changedUpdate(DocumentEvent e) {
        }
      });
      doc.insertString(0, str, attrs);
    } catch (BadLocationException e) {
      e.printStackTrace();
    }
  }

  private void updateStyle(StyledDocument doc) {
    Runnable updateStyle = () -> {
      try {
        doc.setCharacterAttributes(0, doc.getLength(), getStyle("default"), true);
        for (int i = 0; i < doc.getLength(); i++) {
          String ch = doc.getText(i, 1);
          switch (ch) {
            case "A", "E", "X", "F", "G", "U", "K" -> doc.setCharacterAttributes(i, 1, getStyle("ctlk_operator"), true);
            case "!", "|", "&", "-", ">", "<" -> doc.setCharacterAttributes(i, 1, getStyle("operator"), true);
          }
        }
      } catch (BadLocationException badLocationException) {
        badLocationException.printStackTrace();
      }
    };
    SwingUtilities.invokeLater(updateStyle);
  }

  private void addOperatorStyle(JTextPane pn) {
    Style style = pn.addStyle("operator", null);

    StyleConstants.setForeground(style, new Color(235, 39, 117));
    StyleConstants.setBackground(style, Color.WHITE);
  }

  private void addCTLKOperatorStyle(JTextPane pn) {
    Style style = pn.addStyle("ctlk_operator", null);

    StyleConstants.setForeground(style, new Color(39, 156, 235));
    StyleConstants.setBackground(style, Color.WHITE);
  }

  private void addDefaultStyle(JTextPane pn) {
    Style style = pn.addStyle("default", null);

    StyleConstants.setForeground(style, new Color(50, 50, 50));
    StyleConstants.setBackground(style, Color.WHITE);
  }

}
