package org.ntic.view.ui;

import javax.swing.border.Border;
import java.awt.*;
import java.io.Serializable;

/**
 * Border with a drop shadow.
 */
public class ShadowBorder implements Border, Serializable {

  /**
   *
   */
  private static final long serialVersionUID = 6854989457150641240L;
  public static ShadowBorder sharedInstance = new ShadowBorder();
  private final Insets insets;

  private ShadowBorder() {
    insets = new Insets(0, 0, 2, 0);
  }

  public static ShadowBorder getSharedInstance() {
    return sharedInstance;
  }

  @Override
  public Insets getBorderInsets(Component c) {
    return insets;
  }

  @Override
  public boolean isBorderOpaque() {
    return false;
  }

  @Override
  public void paintBorder(Component c, Graphics g, int x, int y, int w, int h) {
    // choose which colors we want to use
    Color bg = c.getBackground();

    if (c.getParent() != null) {
      bg = c.getParent().getBackground();
    }

    if (bg != null) {
      g.setColor(new Color(200, 200, 200));
      g.drawLine(0, h - 2, w, h - 2);
      g.drawLine(0, h - 1, w, h - 1);

      // draw the drop-shadow
      g.setColor(new Color(220, 220, 220));
      g.drawLine(1, h - 2, w - 2, h - 2);

      g.setColor(new Color(240, 240, 240));
      g.drawLine(2, h - 1, w - 2, h - 1);
    }
  }

}
