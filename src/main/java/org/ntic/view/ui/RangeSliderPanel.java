package org.ntic.view.ui;

import org.ntic.Main;
import org.ntic.view.editor.simulation.AutomaticSimulator;
import org.ntic.view.editor.simulation.Simulator;

import javax.swing.*;
import javax.swing.event.ChangeEvent;
import java.awt.*;

/**
 * Demo application panel to display a range slider.
 *
 * @author Ernest Yu
 */
public class RangeSliderPanel extends JPanel {

  private final RangeSlider rangeSlider;
  private final JLabel rangeSliderMinLabel = new JLabel();
  private final JLabel rangeSliderMaxLabel = new JLabel();
  private RangeSlider slider;

  public RangeSliderPanel() {
    setBorder(null);
    setBackground(null);
    setLayout(new BorderLayout());

    JLabel rangeSliderLabel1 = new JLabel();
    rangeSliderLabel1.setText("Min :");
    JLabel rangeSliderLabel2 = new JLabel();
    rangeSliderLabel2.setText("Max :");
    rangeSliderMinLabel.setHorizontalAlignment(JLabel.LEFT);
    rangeSliderMaxLabel.setHorizontalAlignment(JLabel.LEFT);

    rangeSliderLabel1.setBackground(Color.RED);
    rangeSliderMinLabel.setBackground(Color.BLUE);
    rangeSliderLabel2.setBackground(Color.GREEN);
    rangeSliderMaxLabel.setBackground(Color.YELLOW);

    Dimension dimension = new Dimension(150, 20);

    rangeSlider = new RangeSlider();
    rangeSlider.setPreferredSize(dimension);
    rangeSlider.setMinimumSize(dimension);
    rangeSlider.setMaximumSize(dimension);
    rangeSlider.setBackground(null);

    setPreferredSize(new Dimension(150, 40));
    setMinimumSize(new Dimension(150, 40));
    setMaximumSize(new Dimension(150, 40));

    rangeSlider.setMinimum(1);
    rangeSlider.setMaximum(20);

    // Add listener to update display.
    rangeSlider.addChangeListener(e -> {
      slider = (RangeSlider) e.getSource();
      rangeSliderMinLabel.setText(String.valueOf(slider.getValue()));
      rangeSliderMaxLabel.setText(String.valueOf(slider.getUpperValue()));
      try {
        Simulator simulator = Main.editor.getGraphComponent().getGraph().getSimulator();
        if (simulator instanceof AutomaticSimulator) {
          ((AutomaticSimulator) simulator).setMinDelay(slider.getValue());
          ((AutomaticSimulator) simulator).setMaxDelay(slider.getUpperValue());
        }
      } catch (NullPointerException ignore) {
      }
    });

    Panel labels = new Panel();
    labels.setPreferredSize(dimension);
    labels.setMinimumSize(dimension);
    labels.setMaximumSize(dimension);

    labels.add(rangeSliderLabel1);
    labels.add(rangeSliderMinLabel);
    labels.add(rangeSliderLabel2);
    labels.add(rangeSliderMaxLabel);

    add(labels, BorderLayout.NORTH);
    add(rangeSlider, BorderLayout.SOUTH);

    // Initialize values.
    rangeSlider.setValue(1);
    rangeSlider.setUpperValue(3);

    // Initialize value display.
    rangeSliderMinLabel.setText(String.valueOf(rangeSlider.getValue()));
    rangeSliderMaxLabel.setText(String.valueOf(rangeSlider.getUpperValue()));
  }

  public void initSimulatorValues() {
    rangeSlider.getChangeListeners()[0].stateChanged(new ChangeEvent(rangeSlider));
  }

}
