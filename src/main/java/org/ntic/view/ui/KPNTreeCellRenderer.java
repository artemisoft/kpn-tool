package org.ntic.view.ui;

import org.ntic.model.*;
import org.ntic.util.Utils;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeCellRenderer;
import java.awt.*;

public class KPNTreeCellRenderer extends DefaultTreeCellRenderer {

  ImageIcon statePlaceIcon;
  ImageIcon knowledgePlaceIcon;
  ImageIcon transitionIcon;
  ImageIcon arcIcon;
  ImageIcon agentIcon;
  ImageIcon statePlacesIcon;
  ImageIcon knowledgePlacesIcon;
  ImageIcon transitionsIcon;
  ImageIcon arcsIcon;
  ImageIcon agentsIcon;
  ImageIcon defaultIcon;

  public KPNTreeCellRenderer() {
    statePlaceIcon = new ImageIcon(KPNTreeCellRenderer.class.getResource("/icons/state_place_16.png"));
    knowledgePlaceIcon = new ImageIcon(KPNTreeCellRenderer.class.getResource("/icons/knowledge_place_16.png"));
    transitionIcon = new ImageIcon(KPNTreeCellRenderer.class.getResource("/icons/transition_16.png"));
    arcIcon = new ImageIcon(KPNTreeCellRenderer.class.getResource("/icons/arc_16.png"));
    agentIcon = new ImageIcon(KPNTreeCellRenderer.class.getResource("/icons/agent_16.png"));
    statePlacesIcon = new ImageIcon(KPNTreeCellRenderer.class.getResource("/icons/state_places_16.png"));
    knowledgePlacesIcon = new ImageIcon(KPNTreeCellRenderer.class.getResource("/icons/knowledge_places_16.png"));
    transitionsIcon = new ImageIcon(KPNTreeCellRenderer.class.getResource("/icons/transitions_16.png"));
    arcsIcon = new ImageIcon(KPNTreeCellRenderer.class.getResource("/icons/arcs_16.png"));
    agentsIcon = new ImageIcon(KPNTreeCellRenderer.class.getResource("/icons/agents_16.png"));
    defaultIcon = new ImageIcon(KPNTreeCellRenderer.class.getResource("/icons/kpn_tool_16.png"));
  }

  @Override
  public Component getTreeCellRendererComponent(JTree tree,
                                                Object value, boolean sel, boolean expanded, boolean leaf,
                                                int row, boolean hasFocus) {
    super.getTreeCellRendererComponent(tree, value, sel,
        expanded, leaf, row, hasFocus);
    Object nodeObj = ((DefaultMutableTreeNode) value).getUserObject();
    if (nodeObj != null) {

      if (nodeObj instanceof StatePlace)
        setIcon(statePlaceIcon);
      else if (nodeObj instanceof KnowledgePlace)
        setIcon(knowledgePlaceIcon);
      else if (nodeObj instanceof Transition)
        setIcon(transitionIcon);
      else if (nodeObj instanceof Arc)
        setIcon(arcIcon);
      else if (nodeObj instanceof Agent)
        setIcon(agentIcon);
      else if (nodeObj.equals(Utils.getResource("statePlaces")))
        setIcon(statePlacesIcon);
      else if (nodeObj.equals(Utils.getResource("knowledgePlaces")))
        setIcon(knowledgePlacesIcon);
      else if (nodeObj.equals(Utils.getResource("transitions")))
        setIcon(transitionsIcon);
      else if (nodeObj.equals(Utils.getResource("arcs")))
        setIcon(arcsIcon);
      else if (nodeObj.equals(Utils.getResource("agents")))
        setIcon(agentsIcon);
      else
        setIcon(null);
    }

    return this;
  }

}
