package org.ntic.view.ui;

import javax.swing.text.*;

public class MyEditorKit extends StyledEditorKit {

  @Override
  public ViewFactory getViewFactory() {
    return new StyledViewFactory();
  }

  static class StyledViewFactory implements ViewFactory {

    @Override
    public View create(Element elem) {
      String kind = elem.getName();
      if (kind != null) {
        switch (kind) {
          case AbstractDocument.ContentElementName:
            return new LabelView(elem);
          case AbstractDocument.ParagraphElementName:
            return new ParagraphView(elem);
          case AbstractDocument.SectionElementName:

            return new CenteredBoxView(elem, View.Y_AXIS);
          case StyleConstants.ComponentElementName:
            return new ComponentView(elem);
          case StyleConstants.IconElementName:

            return new IconView(elem);
        }
      }

      return new LabelView(elem);
    }

  }

}
