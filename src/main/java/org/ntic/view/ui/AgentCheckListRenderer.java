package org.ntic.view.ui;

import javax.swing.*;
import java.awt.*;

public class AgentCheckListRenderer extends JCheckBox implements ListCellRenderer<AgentCheckListItem> {

  @Override
  public Component getListCellRendererComponent(JList list, AgentCheckListItem value,
                                                int index, boolean isSelected, boolean hasFocus) {
    setEnabled(list.isEnabled());
    setSelected(value.isSelected());
    setFont(list.getFont());
    setBackground(list.getBackground());
    setForeground(list.getForeground());
    setText(value.toString());
    return this;
  }

}
