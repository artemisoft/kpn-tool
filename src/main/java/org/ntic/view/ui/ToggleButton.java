package org.ntic.view.ui;

import javax.swing.*;

public class ToggleButton extends JToggleButton {

  public ToggleButton(Action action) {
    setAction(action);
    setVerticalTextPosition(SwingConstants.BOTTOM);
    setHorizontalTextPosition(SwingConstants.CENTER);
  }

}
