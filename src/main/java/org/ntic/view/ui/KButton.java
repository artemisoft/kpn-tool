package org.ntic.view.ui;

import javax.swing.*;

public class KButton extends JButton {

  public KButton(Action action) {
    setAction(action);
    setVerticalTextPosition(SwingConstants.BOTTOM);
    setHorizontalTextPosition(SwingConstants.CENTER);
  }

}
