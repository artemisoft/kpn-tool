package org.ntic.view.ui;

import org.ntic.model.Agent;

public class AgentCheckListItem {

  private final Agent agent;
  private boolean isSelected;

  public AgentCheckListItem(Agent agent, boolean isSelected) {
    this.agent = agent;
    this.isSelected = isSelected;
  }

  public boolean isSelected() {
    return isSelected;
  }

  public void setSelected(boolean isSelected) {
    this.isSelected = isSelected;
  }

  public Agent getAgent() {
    return agent;
  }

  @Override
  public String toString() {
    return agent.toString();
  }

}
