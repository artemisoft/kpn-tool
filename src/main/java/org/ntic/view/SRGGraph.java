package org.ntic.view;

import com.mxgraph.model.mxCell;
import com.mxgraph.model.mxGeometry;
import org.ntic.model.Agent;
import org.ntic.model.AgentManager;
import org.ntic.model.reachability.Marking;
import org.ntic.model.reachability.SimilarReachabilityGraph;
import org.ntic.model.reachability.TransitionArc;
import org.ntic.util.EqClassGenerator;
import org.ntic.util.Utils;

import java.util.HashMap;
import java.util.Iterator;
import java.util.Set;

public class SRGGraph extends RGGraph {

  private final HashMap<Agent, mxCell> agentGroups = new HashMap<>();

  public SRGGraph(Set<SimilarReachabilityGraph> sRGraphs, AgentManager agentManager) {
    super();

    if (sRGraphs == null)
      throw new IllegalArgumentException();

    this.drawRGraph(sRGraphs, agentManager);
    getGraphLayout().execute(getDefaultParent());

    // Reposition groups
    Iterator<Agent> agentIterator = agentManager.getAgents().iterator();

    Agent agent;
    mxGeometry geometry = new mxGeometry(0, 0, 0, 0);
    while (agentIterator.hasNext()) {
      agent = agentIterator.next();
      mxCell group = agentGroups.get(agent);
      group.getGeometry().setX(geometry.getX());
      geometry.setX(group.getGeometry().getWidth() + geometry.getX() + 16);
    }
    selectionModel.clear();
    getGraphComponent().setPreferredSize(null);
    refresh();
  }

  public void drawRGraph(Set<SimilarReachabilityGraph> sRGraphs, AgentManager agentManager) {

    Iterator<Agent> agentIterator = agentManager.getAgents().iterator();

    Agent agent;
    while (agentIterator.hasNext()) {
      agent = agentIterator.next();
      this.drawAgentGroup(agent);
    }

    for (SimilarReachabilityGraph rGraph : sRGraphs) {
      Iterator<Marking> vertexIterator = rGraph.vertexSet().iterator();

      Marking marking;
      while (vertexIterator.hasNext()) {
        marking = vertexIterator.next();
        enterGroup(agentGroups.get(rGraph.getAgent()));
        this.drawMarking(marking, EqClassGenerator.getNext(rGraph.getAgent(), marking.getEquivalenceClass()));
        home();
      }

      Iterator<TransitionArc> edgeIterator = rGraph.edgeSet().iterator();

      TransitionArc transitionArc;
      while (edgeIterator.hasNext()) {
        transitionArc = edgeIterator.next();
        this.drawArc(transitionArc);
      }
    }
  }

  public void drawAgentGroup(Agent agent) {
    this.getModel().beginUpdate();
    try {
      mxCell g = (mxCell) this.insertVertex(this.getDefaultParent(), null, Utils.getResource("similarRelations") + " ~" + agent.getName(), 0, 0, 0, 0, "strokeColor=#AAAAAA;strokeWidth=0;fillColor=#FFFFFF;fontColor=#434343");
      agentGroups.put(agent, g);
    } finally {
      this.getModel().endUpdate();
    }
  }

}
