package org.ntic.view;

import javax.swing.*;

public class View extends JFrame {

  public static final long WIDTH = 700;
  public static final long HEIGHT = 600;
  private static final long serialVersionUID = -2707712944901661771L;

  @SuppressWarnings("unused")
  public View() {
    super("KPN Tool");

  }

  public static void main(String[] args) {
    View frame = new View();
    frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
    frame.setSize(400, 320);
    frame.setVisible(true);
  }

}
