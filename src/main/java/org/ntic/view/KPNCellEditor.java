package org.ntic.view;

import com.mxgraph.model.mxCell;
import com.mxgraph.swing.view.mxCellEditor;
import org.ntic.view.graph.KPNGraphComponent;

import java.util.EventObject;

public class KPNCellEditor extends mxCellEditor {

  public KPNCellEditor(KPNGraphComponent graphComponent) {
    super(graphComponent);
  }

  @Override
  public void startEditing(Object cell, EventObject evt) {
    ((KPNGraphComponent) graphComponent).getKeyboardHandler().toggleKeyCommands(false);
    mxCell mxCell = (mxCell) cell;
    String value = mxCell.getValue().toString();
    if (value.indexOf('[') != -1)
      mxCell.setValue(value.substring(0, value.indexOf('[')));
    super.startEditing(cell, evt);
  }

  @Override
  public void stopEditing(boolean cancel) {
    super.stopEditing(cancel);
    ((KPNGraphComponent) graphComponent).getKeyboardHandler().toggleKeyCommands(true);
  }

}
