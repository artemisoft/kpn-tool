package org.ntic.view;

import org.ntic.Main;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionListener;

public class SplashScreen extends JWindow {

  static int count = 1, TIMER_PAUSE = 25, PROGBAR_MAX = 100;

  private final JProgressBar progressBar = new JProgressBar();
  private Timer progressBarTimer;

  ActionListener al = new ActionListener() {
    @Override
    public void actionPerformed(java.awt.event.ActionEvent evt) {
      progressBar.setValue(count);
      if (PROGBAR_MAX == count) {
        progressBarTimer.stop();
        SplashScreen.this.setVisible(false);
        Main.frame.setVisible(true);
      }
      count++;
    }
  };

  private void startProgressBar() {
    progressBarTimer = new Timer(TIMER_PAUSE, al);
    progressBarTimer.start();
  }

  public void splash() {
    Container container = getContentPane();
    container.setLayout(null);

    JPanel panel = new JPanel();
    container.add(panel);
    panel.setBounds(0, 0, 600, 360);
    panel.setLayout(null);

    progressBar.setMaximum(PROGBAR_MAX);
    progressBar.setBounds(320, 300, 200, 2);
    panel.add(progressBar);

    JLabel label = new JLabel();
    label.setIcon(new ImageIcon(SplashScreen.class.getResource("/splash-screen.png")));
    label.setSize(600, 360);
    panel.add(label);

    setSize(600, 360);
    setLocationRelativeTo(null);
    setVisible(true);
    startProgressBar();
  }

}
