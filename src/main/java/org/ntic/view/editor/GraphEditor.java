package org.ntic.view.editor;

import com.mxgraph.layout.hierarchical.mxHierarchicalLayout;
import com.mxgraph.layout.*;
import com.mxgraph.layout.orthogonal.mxOrthogonalLayout;
import com.mxgraph.swing.handler.mxRubberband;
import com.mxgraph.swing.mxGraphOutline;
import com.mxgraph.swing.util.mxMorphing;
import com.mxgraph.util.*;
import com.mxgraph.util.mxEventSource.mxIEventListener;
import com.mxgraph.view.mxGraph;
import org.jgrapht.event.GraphEdgeChangeEvent;
import org.jgrapht.event.GraphListener;
import org.jgrapht.event.GraphVertexChangeEvent;
import org.ntic.Main;
import org.ntic.model.*;
import org.ntic.model.reachability.ReachabilityGraph;
import org.ntic.model.reachability.SimilarReachabilityGraph;
import org.ntic.util.Utils;
import org.ntic.view.RGGraph;
import org.ntic.view.SRGGraph;
import org.ntic.view.editor.simulation.AutomaticSimulator;
import org.ntic.view.editor.simulation.Simulator;
import org.ntic.view.graph.KPNGraphComponent;
import org.ntic.view.graph.KPetriNetGraph;
import org.ntic.view.ui.ShadowBorder;

import javax.swing.*;
import javax.swing.plaf.basic.BasicSplitPaneDivider;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.TreePath;
import java.awt.*;
import java.awt.event.*;
import java.io.*;
import java.util.List;
import java.util.Set;

import static org.ntic.Main.editor;

public class GraphEditor extends JPanel {

  public static final String UNTITLED = "untitled";
  private static final long serialVersionUID = -6561623072112577140L;
  private final JLabel label;
  private final JSplitPane drawingPane;
  protected mxUndoManager undoManager;
  protected JLabel statusBar;
  protected File currentFile;
  protected String appTitle = "KPN Tool";
  protected boolean modified = false;
  protected mxRubberband rubberband;
  protected EditorKeyboardHandler keyboardHandler;
  private KPNGraphComponent graphComponent;
  protected mxIEventListener undoHandler = new mxIEventListener() {
    @Override
    public void invoke(Object source, mxEventObject evt) {
      if (graphComponent.getGraph().getMode() != KPetriNetGraph.Mode.SIMULATION) {
        undoManager.undoableEditHappened((mxUndoableEdit) evt.getProperty("edit"));
      } else {
        graphComponent.repaint();
      }
    }
  };
  private EditorSidebar sidebar;
  private EditorRibbon editorRibbon;
  private JFrame mainFrame;
  private ReachabilityGraph reachabilityGraph;
  private Set<ReachabilityGraph> similarReachabilityGraphs;
  protected mxIEventListener changeTracker = (source, evt) -> {
    if (graphComponent.getGraph().getMode() != KPetriNetGraph.Mode.SIMULATION) {
      setModified(true);
      reachabilityGraph = null;
      similarReachabilityGraphs = null;
    }
  };

  public GraphEditor() {

    initSettings();

    KPetriNetGraph graph = new KPetriNetGraph(new KPetriNet());
    /*try {
      new StaXParser().readNet("DK.kpnml", graph);
      currentFile = new File("DK.kpnml");
    } catch (ParserConfigurationException | IOException | SAXException | XPathExpressionException e) {
      e.printStackTrace();
    }

    Transition transition = new Transition();
    StatePlace place = new StatePlace();

    graph.getPetriNet().addVertex(transition);
    graph.getPetriNet().addVertex(place);
    graph.getPetriNet().addEdge(transition, place);
    graph.getPetriNet().addEdge(place, transition);

     */

    drawingPane = new JSplitPane();
    drawingPane.setBorder(ShadowBorder.getSharedInstance());

    // Stores a reference to the graph and creates the command history
    graphComponent = graph.getGraphComponent();

    undoManager = createUndoManager();

    // Do not change the scale and translation after files have been loaded
    graph.setResetViewOnRootChange(false);

    // Updates the modified flag if the graph model changes
    graph.getModel().addListener(mxEvent.CHANGE, changeTracker);

    // Adds the command history to the model and view
    graph.getModel().addListener(mxEvent.UNDO, undoHandler);
    graph.getView().addListener(mxEvent.UNDO, undoHandler);

    // Keeps the selection in sync with the command history
    mxIEventListener undoHandler = (source, evt) -> {
      List<mxUndoableEdit.mxUndoableChange> changes = ((mxUndoableEdit) evt.getProperty("edit")).getChanges();
      graph.setSelectionCells(graph.getSelectionCellsForChanges(changes));
    };

    undoManager.addListener(mxEvent.UNDO, undoHandler);
    undoManager.addListener(mxEvent.REDO, undoHandler);

    // Creates the status bar
    statusBar = createStatusBar();

    // Display some useful information about repaint events
    installRepaintListener();

    label = new JLabel("Edit");
    label.setBorder(BorderFactory.createEmptyBorder(2, 4, 2, 4));
    label.setBackground(Color.GRAY);
    //graph.getModel().addListener(mxEvent.CHANGE, (o, mxEventObject) -> label.setText(getPetriNet().toString()));

    // Puts everything together
    setLayout(new BorderLayout());
    add(drawingPane, BorderLayout.CENTER);
    drawingPane.setRightComponent(graphComponent);
    drawingPane.setDividerSize(3);
    drawingPane.setDividerLocation(220);

    BasicSplitPaneDivider divider = (BasicSplitPaneDivider) drawingPane.getComponent(1);
    divider.setBackground(new Color(230, 230, 230));
    divider.setBorder(null);

    add(label, BorderLayout.SOUTH);
    installToolBar();
    installSidebar();

    // Installs rubberband selection and handling for some special
    // keystrokes such as F2, Control-C, -V, X, A etc.
    installHandlers();
    installListeners();
    updateTitle();

  }

  private static void initSettings() {
    InputStream inputStream = null;
    try {
      inputStream = new FileInputStream(Main.USER_SETTINGS_PATH);
    } catch (FileNotFoundException e) {
      try {
        Utils.copyDefaultSettings();
        inputStream = new FileInputStream(Main.USER_SETTINGS_PATH);
      } catch (IOException ioException) {
        ioException.printStackTrace();
      }
    }

    try {
      Main.settings.load(inputStream);
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public void installSidebar() {
    sidebar = new EditorSidebar(new DefaultMutableTreeNode(getGraph()) {
      @Override
      public String toString() {
        return super.toString() == null ? Utils.getResource(UNTITLED) : super.toString();
      }
    });
    JScrollPane scrollPane = new JScrollPane(sidebar);
    scrollPane.setBorder(null);
    drawingPane.setLeftComponent(scrollPane);
    sidebar.populate();
    if (getGraph() != null) {
      installGraphLister(getGraph());
    }
    drawingPane.setDividerLocation(220);
  }

  private void installGraphLister(KPetriNetGraph graph) {
    graph.addGraphListener(new GraphListener<>() {
      @Override
      public void edgeAdded(GraphEdgeChangeEvent<Vertex, Arc> e) {
        sidebar.addNode(e.getEdge());
      }

      @Override
      public void edgeRemoved(GraphEdgeChangeEvent<Vertex, Arc> e) {
        sidebar.removeNode(e.getEdge());
      }

      @Override
      public void vertexAdded(GraphVertexChangeEvent<Vertex> e) {
        sidebar.addNode(e.getVertex());
      }

      @Override
      public void vertexRemoved(GraphVertexChangeEvent<Vertex> e) {
        sidebar.removeNode(e.getVertex());
      }
    });

    graph.getAgentManager().addAgentListener(new AgentManager.AgentsEventListener() {
      @Override
      public void agentAdded(Agent agent) {
        sidebar.addNode(agent);
      }

      @Override
      public void agentRemoved(Agent agent) {
        sidebar.removeNode(agent);
      }
    });

  }

  protected mxUndoManager createUndoManager() {
    return new mxUndoManager();
  }

  protected JLabel createStatusBar() {
    JLabel statusBar = new JLabel(Utils.getResource("ready"));
    statusBar.setBorder(BorderFactory.createEmptyBorder(2, 4, 2, 4));

    return statusBar;
  }

  protected void installRepaintListener() {
    graphComponent.getGraph().addListener(mxEvent.REPAINT, (source, evt) -> {
      if (graphComponent.getGraph().getMode() != KPetriNetGraph.Mode.SIMULATION) {
        TreePath selectionPath = sidebar.getSelectionPath();
        editor.getSidebar().collapseRow(0);
        editor.getSidebar().expandRow(0);
        sidebar.setSelectionPath(selectionPath);
      }
    });
  }

  protected void installToolBar() {
    editorRibbon = new EditorRibbon(this, JTabbedPane.TOP);
    add(editorRibbon, BorderLayout.NORTH);
  }

  protected void installHandlers() {
    rubberband = new mxRubberband(graphComponent);
    keyboardHandler = new EditorKeyboardHandler(graphComponent);
  }

  protected void installListeners() {
    // Installs mouse wheel listener for zooming
    MouseWheelListener wheelTracker = e -> {
      if (e.getSource() instanceof mxGraphOutline || e.isControlDown()) {
        GraphEditor.this.mouseWheelMoved(e);
      }
    };

    graphComponent.addMouseWheelListener(wheelTracker);

    // Installs the popup menu in the graph component
    graphComponent.getGraphControl().addMouseListener(new MouseAdapter() {

      @Override
      public void mousePressed(MouseEvent e) {
        // Handles context menu on the Mac where the trigger is on mousePressed
        mouseReleased(e);
      }

      @Override
      public void mouseReleased(MouseEvent e) {
        if (e.isPopupTrigger()) {
          showGraphPopupMenu(e);
        }
      }

    });

    // Installs a mouse motion listener to display the mouse location
    graphComponent.getGraphControl().addMouseMotionListener(new MouseMotionListener() {

      /*
       * (non-Javadoc)
       * @see java.awt.event.MouseMotionListener#mouseDragged(java.awt.event.MouseEvent)
       */
      @Override
      public void mouseDragged(MouseEvent e) {
        mouseLocationChanged(e);
      }

      /*
       * (non-Javadoc)
       * @see java.awt.event.MouseMotionListener#mouseMoved(java.awt.event.MouseEvent)
       */
      @Override
      public void mouseMoved(MouseEvent e) {
        mouseDragged(e);
      }

    });
  }

  public void updateTitle() {
    JFrame frame = (JFrame) SwingUtilities.windowForComponent(this);

    if (frame != null) {
      String title = (currentFile != null) ? currentFile.getAbsolutePath() : Utils.getResource(UNTITLED);
      sidebar.repaint();
      if (modified) {
        title += "*";
      }

      frame.setTitle(title + " - " + appTitle);
    }
  }

  public void status(String msg) {
    statusBar.setText(msg);
  }

  public void label(String msg) {
    label.setText(msg);
  }

  public String label() {
    return label.getText();
  }

  protected void mouseWheelMoved(MouseWheelEvent e) {
    if (e.getWheelRotation() < 0) {
      graphComponent.zoomIn();
    } else {
      graphComponent.zoomOut();
    }

    status(Utils.getResource("scale") + ": " + (int) (100 * graphComponent.getGraph().getView().getScale()) + "%");
  }

  protected void showGraphPopupMenu(MouseEvent e) {
    //Point pt = SwingUtilities.convertPoint(e.getComponent(), e.getPoint(), graphComponent);
    //EditorVertexPopupMenu menu = new EditorVertexPopupMenu(GraphEditor.this);
    //menu.show(graphComponent, pt.x, pt.y);

    e.consume();
  }

  protected void mouseLocationChanged(MouseEvent e) {
    status(e.getX() + ", " + e.getY());
  }

  public File getCurrentFile() {
    return currentFile;
  }

  public void setCurrentFile(File file) {
    File oldValue = currentFile;
    currentFile = file;

    firePropertyChange("currentFile", oldValue, file);

    if (oldValue != file) {
      updateTitle();
    }
  }

  public boolean isModified() {
    return modified;
  }

  public void setModified(boolean modified) {
    boolean oldValue = this.modified;
    this.modified = modified;

    firePropertyChange("modified", oldValue, modified);

    if (oldValue != modified) {
      updateTitle();
    }
  }

  public mxUndoManager getUndoManager() {
    return undoManager;
  }

  public Action bind(String name, final Action action) {
    return bind(name, action, null);
  }

  public Action bind(String name, final Action action, String iconUrl) {
    AbstractAction newAction = new AbstractAction(name, (iconUrl != null) ? new ImageIcon(GraphEditor.class.getResource(iconUrl)) : null) {
      @Override
      public void actionPerformed(ActionEvent e) {
        action.actionPerformed(new ActionEvent(getGraphComponent(), e.getID(), e.getActionCommand()));
      }
    };

    newAction.putValue(Action.SHORT_DESCRIPTION, action.getValue(Action.SHORT_DESCRIPTION));

    return newAction;
  }

  public KPNGraphComponent getGraphComponent() {
    return graphComponent;
  }

  public JFrame createFrame() {
    mainFrame = new JFrame();
    mainFrame.getContentPane().add(this);
    mainFrame.setDefaultCloseOperation(JFrame.DO_NOTHING_ON_CLOSE);
    mainFrame.setSize(1100, 700);
    mainFrame.setIconImage(new ImageIcon(getClass().getResource("/icons/kpn_tool_32.png")).getImage());
    mainFrame.addWindowListener(new WindowAdapter() {
      @Override
      public void windowClosing(WindowEvent e) {
        int saveChanges = -1;
        if (editor.isModified()) {
          saveChanges = JOptionPane.showConfirmDialog(editor, Utils.getResource("saveChanges"));
        }

        if (editor.isModified() && saveChanges == JOptionPane.YES_OPTION)
          new EditorActions.SaveAction(false).actionPerformed(new ActionEvent(this, Utils.getRandomNumberInRange(ActionEvent.ACTION_FIRST, ActionEvent.ACTION_LAST), "close"));

        if (!editor.isModified() || saveChanges != JOptionPane.CANCEL_OPTION)
          System.exit(0);

      }
    });
    // Updates the frame title
    updateTitle();

    mainFrame.setLocationRelativeTo(null);
    return mainFrame;
  }

  public void createRGFrame(RGGraph graph) {

    JFrame rgFrame = new JFrame();

    rgFrame.setSize(800, 600);
    rgFrame.setContentPane(graph.getGraphComponent());
    rgFrame.setIconImage(new ImageIcon(getClass().getResource("/icons/" + (graph instanceof SRGGraph ? "srg_32.png" : "rg_32.png"))).getImage());
    rgFrame.setTitle(Utils.getResource((graph instanceof SRGGraph ? "srg" : "rg")));
    rgFrame.setType(Window.Type.POPUP);
    rgFrame.setLocationRelativeTo(editor.getMainFrame());
    rgFrame.setVisible(true);
  }

  /**
   * Creates an action that executes the specified layout.
   *
   * @param key Key to be used for getting the label from mxResources and also
   *            to create the layout instance for the commercial graph editor example.
   * @return an action that executes the specified layout
   */
  public Action graphLayout(final String key) {
    final mxIGraphLayout layout = createLayout(key);

    if (layout != null) {
      return new AbstractAction(Utils.getResource(key)) {
        @Override
        public void actionPerformed(ActionEvent e) {
          final mxGraph graph = graphComponent.getGraph();
          Object cell = graph.getSelectionCell();

          if (cell == null || graph.getModel().getChildCount(cell) == 0) {
            cell = graph.getDefaultParent();
          }

          graph.getModel().beginUpdate();
          try {
            long t0 = System.currentTimeMillis();
            layout.execute(cell);
            status("Layout: " + (System.currentTimeMillis() - t0) + " ms");
          } finally {
            mxMorphing morph = new mxMorphing(graphComponent, 20, 1.2, 20);

            morph.addListener(mxEvent.DONE, (sender, evt) -> graph.getModel().endUpdate());

            morph.startAnimation();
          }

        }

      };
    } else {
      return new AbstractAction(Utils.getResource(key)) {

        @Override
        public void actionPerformed(ActionEvent e) {
          JOptionPane.showMessageDialog(graphComponent, Utils.getResource("noLayout"));
        }

      };
    }
  }

  /**
   * Creates a layout instance for the given identifier.
   */
  protected mxIGraphLayout createLayout(String ident) {
    mxIGraphLayout layout = null;

    if (ident != null) {
      mxGraph graph = graphComponent.getGraph();

      switch (ident) {
        case "verticalHierarchical" -> layout = new mxHierarchicalLayout(graph);
        case "horizontalHierarchical" -> layout = new mxHierarchicalLayout(graph, JLabel.WEST);
        case "verticalTree" -> layout = new mxCompactTreeLayout(graph, false);
        case "horizontalTree" -> layout = new mxCompactTreeLayout(graph, true);
        case "parallelEdges" -> layout = new mxParallelEdgeLayout(graph);
        case "placeEdgeLabels" -> layout = new mxEdgeLabelLayout(graph);
        case "organicLayout" -> layout = new mxOrganicLayout(graph);
        case "orthogonalLayout" -> layout = new mxOrthogonalLayout(graph);
        case "compactTreeLayout" -> layout = new mxCompactTreeLayout(graph);
      }
      layout = switch (ident) {
        case "verticalPartition" -> new mxPartitionLayout(graph, false) {
          /**
           * Overrides the empty implementation to return the size of the
           * graph control.
           */
          @Override
          public mxRectangle getContainerSize() {
            return graphComponent.getLayoutAreaSize();
          }
        };
        case "horizontalPartition" -> new mxPartitionLayout(graph, true) {
          /**
           * Overrides the empty implementation to return the size of the
           * graph control.
           */
          @Override
          public mxRectangle getContainerSize() {
            return graphComponent.getLayoutAreaSize();
          }
        };
        case "verticalStack" -> new mxStackLayout(graph, false) {
          /**
           * Overrides the empty implementation to return the size of the
           * graph control.
           */
          @Override
          public mxRectangle getContainerSize() {
            return graphComponent.getLayoutAreaSize();
          }
        };
        case "horizontalStack" -> new mxStackLayout(graph, true) {
          /**
           * Overrides the empty implementation to return the size of the
           * graph control.
           */
          @Override
          public mxRectangle getContainerSize() {
            return graphComponent.getLayoutAreaSize();
          }
        };
        case "circleLayout" -> new mxCircleLayout(graph);
        default -> layout;
      };
    }

    return layout;
  }

  public KPetriNet getPetriNet() {
    return graphComponent.getGraph().getPetriNet();
  }

  public KPetriNetGraph getGraph() {
    return graphComponent.getGraph();
  }

  public EditorRibbon getEditorRibbon() {
    return editorRibbon;
  }

  public void initRandomnessSliderValues() {
    Simulator simulator = getGraph().getSimulator();
    if (simulator instanceof AutomaticSimulator) {
      editorRibbon.getRandomnessSlider().initSimulatorValues();
    }
  }

  public EditorSidebar getSidebar() {
    return sidebar;
  }

  public JFrame getMainFrame() {
    return mainFrame;
  }

  public void setMainFrame(JFrame mainFrame) {
    this.mainFrame = mainFrame;
  }

  public ReachabilityGraph getReachabilityGraph() {
    return reachabilityGraph;
  }

  public void setReachabilityGraph(ReachabilityGraph reachabilityGraph) {
    this.reachabilityGraph = reachabilityGraph;
  }

  public SimilarReachabilityGraph getSimilarReachabilityGraph(Agent agent) {
    for (ReachabilityGraph srg : similarReachabilityGraphs)
      if (((SimilarReachabilityGraph) srg).getAgent().equals(agent))
        return (SimilarReachabilityGraph) srg;
    return null;
  }

  public Set<ReachabilityGraph> getSimilarReachabilityGraphs() {
    return similarReachabilityGraphs;
  }

  public void setSimilarReachabilityGraphs(Set<ReachabilityGraph> rGraphs) {
    this.similarReachabilityGraphs = rGraphs;
  }

}
