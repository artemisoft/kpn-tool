package org.ntic.view.editor;

import com.mxgraph.swing.handler.mxKeyboardHandler;
import com.mxgraph.swing.mxGraphComponent;
import org.ntic.view.graph.KPNGraphComponent;

import javax.swing.*;
import java.awt.event.KeyEvent;

public class EditorKeyboardHandler extends mxKeyboardHandler {

  private final KPNGraphComponent graphComponent;
  private boolean commandsActive = true;

  EditorKeyboardHandler(KPNGraphComponent graphComponent) {
    super(graphComponent);
    graphComponent.setKeyboardHandler(this);
    this.graphComponent = graphComponent;
    ActionMap actionMap = createActionMap();
    fillInputMap();
    graphComponent.setActionMap(actionMap);
  }

  @Override
  protected void installKeyboardActions(mxGraphComponent graphComponent) {
    //overriding parent behaviour by doing nothing
  }

  private void fillInputMap() {
    InputMap map;

    map = graphComponent.getInputMap(JComponent.WHEN_FOCUSED);
    InputMap map1 = getInputMap(JComponent.WHEN_FOCUSED);
    for (KeyStroke key : map1.allKeys()) {
      map.put(key, map1.get(key));
    }

    map = graphComponent.getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    map1 = getInputMap(JComponent.WHEN_ANCESTOR_OF_FOCUSED_COMPONENT);
    try {

      for (KeyStroke key : map1.allKeys()) {
        map.put(key, map1.get(key));
      }
    } catch (NullPointerException ignore) {
    }

    map = graphComponent.getInputMap(JComponent.WHEN_IN_FOCUSED_WINDOW);

    map.put(KeyStroke.getKeyStroke("control S"), "save");
    map.put(KeyStroke.getKeyStroke("control shift S"), "saveAs");
    map.put(KeyStroke.getKeyStroke("control N"), "new");
    map.put(KeyStroke.getKeyStroke("control O"), "open");

    map.put(KeyStroke.getKeyStroke("control Z"), "undo");
    map.put(KeyStroke.getKeyStroke("control Y"), "redo");
    map.put(KeyStroke.getKeyStroke("control shift V"), "selectVertices");
    map.put(KeyStroke.getKeyStroke("control shift E"), "selectEdges");
    map.put(KeyStroke.getKeyStroke("S"), "statePlace");
    map.put(KeyStroke.getKeyStroke("K"), "knowledgePlace");
    map.put(KeyStroke.getKeyStroke("T"), "transition");
    map.put(KeyStroke.getKeyStroke("P"), "pointer");
    map.put(KeyStroke.getKeyStroke("UP"), "moveUp");
    map.put(KeyStroke.getKeyStroke("DOWN"), "moveDown");
    map.put(KeyStroke.getKeyStroke("RIGHT"), "MoveRight");
    map.put(KeyStroke.getKeyStroke("LEFT"), "MoveLeft");
    map.put(KeyStroke.getKeyStroke(KeyEvent.VK_ESCAPE, 0), "pointer");

  }

  @Override
  protected ActionMap createActionMap() {
    ActionMap map = super.createActionMap();

    map.put("save", new EditorActions.SaveAction(false));
    map.put("saveAs", new EditorActions.SaveAction(true));
    map.put("new", new EditorActions.NewAction());
    map.put("open", new EditorActions.OpenAction());
    map.put("undo", new EditorActions.HistoryAction(true));
    map.put("redo", new EditorActions.HistoryAction(false));
    map.put("selectVertices", KPetriNetGraphActions.getSelectVerticesAction());
    map.put("selectEdges", KPetriNetGraphActions.getSelectEdgesAction());
    map.put("statePlace", KPetriNetGraphActions.getAddStatePlaceAction());
    map.put("knowledgePlace", KPetriNetGraphActions.getAddKnowledgePlaceAction());
    map.put("transition", KPetriNetGraphActions.getAddTransitionAction());
    map.put("pointer", KPetriNetGraphActions.getClearAddAction());
    map.put("moveLeft", KPetriNetGraphActions.getMoveLeftAction());
    map.put("moveRight", KPetriNetGraphActions.getMoveRightAction());
    map.put("moveUp", KPetriNetGraphActions.getMoveUpAction());
    map.put("moveDown", KPetriNetGraphActions.getMoveDownAction());

    map.put("selectNext", KPetriNetGraphActions.getMoveRightAction());
    map.put("selectPrevious", KPetriNetGraphActions.getMoveLeftAction());
    map.put("selectParent", KPetriNetGraphActions.getMoveUpAction());
    map.put("selectChild", KPetriNetGraphActions.getMoveDownAction());

    return map;
  }

  public void toggleKeyCommands(boolean b) {

    commandsActive = b;
  }

  public boolean isCommandsActive() {
    return commandsActive;
  }

}

