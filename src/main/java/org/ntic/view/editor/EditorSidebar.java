package org.ntic.view.editor;

import org.ntic.model.*;
import org.ntic.util.Utils;
import org.ntic.view.graph.KPetriNetGraph;
import org.ntic.view.ui.KPNTreeCellRenderer;

import javax.swing.*;
import javax.swing.tree.DefaultMutableTreeNode;
import javax.swing.tree.DefaultTreeModel;
import javax.swing.tree.TreeNode;
import java.awt.*;
import java.util.Iterator;

import static org.ntic.Main.editor;

public class EditorSidebar extends JTree {

  private final DefaultMutableTreeNode state_places;
  private final DefaultMutableTreeNode knowledge_places;
  private final DefaultMutableTreeNode transitions;
  private final DefaultMutableTreeNode arcs;
  private final DefaultMutableTreeNode agents;
  private final DefaultMutableTreeNode root;

  public EditorSidebar(TreeNode root) {
    super(root);
    this.root = (DefaultMutableTreeNode) root;
    this.state_places = new DefaultMutableTreeNode(Utils.getResource("statePlaces"));
    this.knowledge_places = new DefaultMutableTreeNode(Utils.getResource("knowledgePlaces"));
    this.transitions = new DefaultMutableTreeNode(Utils.getResource("transitions"));
    this.arcs = new DefaultMutableTreeNode(Utils.getResource("arcs"));
    this.agents = new DefaultMutableTreeNode(Utils.getResource("agents"));
    this.root.add(state_places);
    this.root.add(knowledge_places);
    this.root.add(transitions);
    this.root.add(arcs);
    this.root.add(agents);
    setShowsRootHandles(true);
    setCellRenderer(new KPNTreeCellRenderer());
    expandRow(0);
    setBackground(new Color(240, 240, 240));
  }

  public void addNode(Object node) {
    DefaultTreeModel model = (DefaultTreeModel) getModel();
    DefaultMutableTreeNode newChild = new DefaultMutableTreeNode(node);
    if (node instanceof StatePlace) {
      state_places.add(newChild);
    } else if (node instanceof KnowledgePlace) {
      knowledge_places.add(newChild);
    } else if (node instanceof Transition) {
      transitions.add(newChild);
    } else if (node instanceof Arc) {
      arcs.add(newChild);
    } else if (node instanceof Agent) {
      agents.add(newChild);
    }
    model.reload(newChild.getParent());
  }

  public void removeNode(Object element) {
    DefaultTreeModel model = (DefaultTreeModel) getModel();
    DefaultMutableTreeNode node = null;
    DefaultMutableTreeNode parentNode = null;

    if (element instanceof StatePlace) {
      parentNode = state_places;
    } else if (element instanceof KnowledgePlace) {
      parentNode = knowledge_places;
    } else if (element instanceof Transition) {
      parentNode = transitions;
    } else if (element instanceof Arc) {
      parentNode = arcs;
    } else if (element instanceof Agent) {
      parentNode = agents;
    }

    if (parentNode != null) {
      Iterator<TreeNode> treeNodeIterator = parentNode.children().asIterator();
      while (treeNodeIterator.hasNext()) {
        DefaultMutableTreeNode n = (DefaultMutableTreeNode) treeNodeIterator.next();
        if (n.getUserObject() == element) {
          node = n;
          break;
        }
      }
      if (node != null) {
        parentNode.remove(node);
        model.reload(parentNode);
      }
    }
  }

  public void populate() {
    for (Vertex vertex : ((KPetriNetGraph) root.getUserObject()).getPetriNet().vertexSet())
      addNode(vertex);
    for (Arc arc : ((KPetriNetGraph) root.getUserObject()).getPetriNet().edgeSet())
      addNode(arc);
    for (Agent agent : ((KPetriNetGraph) root.getUserObject()).getAgentManager().getAgents())
      addNode(agent);
  }

  public void changeGraph(KPetriNetGraph graph) {
    root.removeAllChildren();
    root.setUserObject(graph);
    this.root.add(state_places);
    this.root.add(knowledge_places);
    this.root.add(transitions);
    this.root.add(arcs);
    this.root.add(agents);
    populate();
    editor.getSidebar().collapseRow(0);
    editor.getSidebar().expandRow(0);
  }

}
