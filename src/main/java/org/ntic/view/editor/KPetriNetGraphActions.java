package org.ntic.view.editor;

import com.mxgraph.view.mxGraph;
import org.ntic.model.Agent;
import org.ntic.model.AgentManager;
import org.ntic.model.KnowledgePlace;
import org.ntic.util.Utils;
import org.ntic.view.editor.simulation.AutomaticSimulator;
import org.ntic.view.editor.simulation.ManualSimulator;
import org.ntic.view.editor.simulation.Simulator;
import org.ntic.view.graph.KPNGraphComponent;
import org.ntic.view.graph.KPetriNetGraph;
import org.ntic.view.ui.AgentCheckListItem;
import org.ntic.view.ui.AgentCheckListRenderer;

import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.MouseAdapter;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

import static com.mxgraph.swing.util.mxGraphActions.getGraph;
import static org.ntic.Main.editor;

public class KPetriNetGraphActions {

  private static final AbstractAction moveLeftAction = new MoveAction(MoveAction.Direction.LEFT);
  private static final AbstractAction moveRightAction = new MoveAction(MoveAction.Direction.RIGHT);
  private static final AbstractAction moveUpAction = new MoveAction(MoveAction.Direction.UP);
  private static final AbstractAction moveDownAction = new MoveAction(MoveAction.Direction.DOWN);

  public static AbstractAction getMoveLeftAction() {
    return moveLeftAction;
  }

  public static AbstractAction getMoveRightAction() {
    return moveRightAction;
  }

  public static AbstractAction getMoveUpAction() {
    return moveUpAction;
  }

  public static AbstractAction getMoveDownAction() {
    return moveDownAction;
  }

  public static AbstractAction getAddTransitionAction() {
    return new AddTransitionAction("add transition");
  }

  public static AbstractAction getAddKnowledgePlaceAction() {
    return new AddKnowledgePlaceAction("add knowledge place");
  }

  public static AbstractAction getSelectAgentsAction(KnowledgePlace place) {
    return new SelectAgentsAction("Select agents", place);
  }

  public static AbstractAction getAddAgentAction() {
    return new AddAgentAction("Add agent");
  }

  public static AbstractAction getAddStatePlaceAction() {
    return new AddStatePlaceAction("add state place");
  }

  public static AbstractAction getAddArcAction() {
    return new AddArcAction("add arc");
  }

  public static Action getClearAddAction() {
    return new ClearAddAction("clear add action");
  }

  public static Action getDeleteAction() {
    return new DeleteAction("delete");
  }

  public static Action getStartSimulationAction(boolean isAutomatic) {
    return new StartSimulationAction("start simulator", isAutomatic);
  }

  public static Action getPauseSimulationAction() {
    return new PauseSimulationAction("pause simulator");
  }

  public static Action getStopSimulationAction() {
    return new StopSimulationAction("stop simulator");
  }

  public static Action getResetSimulationAction() {
    return new ResetSimulationAction("reset simulator");
  }

  public static Action getSelectVerticesAction() {
    return new SelectAction("vertices");
  }

  public static Action getSelectEdgesAction() {
    return new SelectAction("edges");
  }

  public static class AddTransitionAction extends AbstractAction {

    private static final long serialVersionUID = -8212339796803275529L;

    public AddTransitionAction(String name) {
      super(name);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      if (!editor.getGraphComponent().getKeyboardHandler().isCommandsActive())
        return;

      KPNGraphComponent graphComponent = (KPNGraphComponent) e.getSource();
      if (graphComponent != null) {
        KPetriNetGraph graph = graphComponent.getGraph();

        graph.setMode(KPetriNetGraph.Mode.TRANSITION);

      }

    }

  }

  public static class AddStatePlaceAction extends AbstractAction {

    private static final long serialVersionUID = -8212339796803275529L;

    public AddStatePlaceAction(String name) {
      super(name);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      if (!editor.getGraphComponent().getKeyboardHandler().isCommandsActive())
        return;
      KPNGraphComponent graphComponent = (KPNGraphComponent) e.getSource();
      if (graphComponent != null) {
        KPetriNetGraph graph = graphComponent.getGraph();

        graph.setMode(KPetriNetGraph.Mode.STATE_PLACE);

      }
    }

  }

  public static class AddArcAction extends AbstractAction {

    private static final long serialVersionUID = -8212339796803275529L;

    public AddArcAction(String name) {
      super(name);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      KPNGraphComponent graphComponent = (KPNGraphComponent) e.getSource();
      if (graphComponent != null) {
        KPetriNetGraph graph = graphComponent.getGraph();
        graph.setMode(KPetriNetGraph.Mode.ARC);
      }
    }

  }

  public static class AddKnowledgePlaceAction extends AbstractAction {

    private static final long serialVersionUID = -8212339796803275529L;

    public AddKnowledgePlaceAction(String name) {
      super(name);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      if (!editor.getGraphComponent().getKeyboardHandler().isCommandsActive())
        return;

      KPNGraphComponent graphComponent = (KPNGraphComponent) e.getSource();
      if (graphComponent != null) {
        KPetriNetGraph graph = graphComponent.getGraph();

        graph.setMode(KPetriNetGraph.Mode.KNOWLEDGE_PLACE);

      }

    }

  }

  public static class SelectAgentsAction extends AbstractAction {

    private static final long serialVersionUID = -8212339796803275529L;
    private final KnowledgePlace place;

    public SelectAgentsAction(String name, KnowledgePlace place) {
      super(name);
      this.place = place;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      KPNGraphComponent graphComponent = editor.getGraphComponent();
      graphComponent.getGraph();
      AgentManager agentManager = editor.getGraph().getAgentManager();
      ArrayList<AgentCheckListItem> checkListItems = new ArrayList<>();
      for (Agent agent : agentManager.getAgents()) {
        checkListItems.add(new AgentCheckListItem(agent, agentManager.getAssociatedPlaces(agent).contains(place)));
      }
      JList<AgentCheckListItem> list = new JList<>(checkListItems.toArray(new AgentCheckListItem[]{}));
      list.setCellRenderer(new AgentCheckListRenderer());
      list.setSelectionMode(ListSelectionModel.MULTIPLE_INTERVAL_SELECTION);
      list.setSelectionModel(new DefaultListSelectionModel() {
        @Override
        public void setSelectionInterval(int index0, int index1) {
          if (super.isSelectedIndex(index0)) {
            super.removeSelectionInterval(index0, index1);
          } else {
            super.addSelectionInterval(index0, index1);
          }
        }
      });
      list.addMouseListener(new MouseAdapter() {
        @Override
        public void mouseClicked(MouseEvent event) {
          JList<AgentCheckListItem> list = (JList<AgentCheckListItem>) event.getSource();
          int index = list.locationToIndex(event.getPoint());// Get index of item
          // clicked

          AgentCheckListItem item = list.getModel().getElementAt(index);
          item.setSelected(!item.isSelected()); // Toggle selected state
          if (item.isSelected()) {
            list.getSelectionModel().addSelectionInterval(index, index);
          } else {
            list.getSelectionModel().removeSelectionInterval(index, index);
          }
          list.repaint(list.getCellBounds(index, index));// Repaint cell
        }
      });
      for (int i = 0; i < list.getModel().getSize(); i++) {
        if (list.getModel().getElementAt(i).isSelected()) {
          list.getSelectionModel().addSelectionInterval(i, i);
        }
      }
      int result = JOptionPane.showConfirmDialog(null, new JScrollPane(list), "Select agents", JOptionPane.DEFAULT_OPTION);
      if (result == JOptionPane.OK_OPTION) {
        agentManager.removeAllAssociations(place);

        for (AgentCheckListItem a : list.getSelectedValuesList()) {
          agentManager.associateAgentKnowledgePlace(a.getAgent(), place);
        }
      }

    }

  }

  public static class AddAgentAction extends AbstractAction {

    private static final long serialVersionUID = -8212339796803275529L;

    public AddAgentAction(String name) {
      super(name);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      AgentManager agentManager = editor.getGraph().getAgentManager();
      String name = JOptionPane.showInputDialog(editor.getMainFrame(), Utils.getResource("enterAgent"), Utils.getResource("new") + " " + Utils.getResource("agent"), JOptionPane.QUESTION_MESSAGE);
      if (name != null) {
        Agent agent = agentManager.addAgent(name, false);
        if (agent == null) {
          JOptionPane.showMessageDialog(editor.getMainFrame(), Utils.getResource("agentExists"), Utils.getResource("error"), JOptionPane.WARNING_MESSAGE);
        }
      }

    }

  }

  public static class ClearAddAction extends AbstractAction {

    private static final long serialVersionUID = -8212339796803275529L;

    public ClearAddAction(String name) {
      super(name);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      if (!editor.getGraphComponent().getKeyboardHandler().isCommandsActive())
        return;

      KPNGraphComponent graphComponent = (KPNGraphComponent) e.getSource();
      if (graphComponent != null) {
        KPetriNetGraph graph = graphComponent.getGraph();

        graph.setMode(null);

      }

    }

  }

  public static class DeleteAction extends AbstractAction {

    private static final long serialVersionUID = -8212339796803275529L;

    public DeleteAction(String name) {
      super(name);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      KPNGraphComponent graphComponent = (KPNGraphComponent) e.getSource();
      if (graphComponent != null) {
        KPetriNetGraph graph = graphComponent.getGraph();

        graph.setMode(KPetriNetGraph.Mode.DELETE);

      }

    }

  }

  private static class StartSimulationAction extends AbstractAction {

    private final boolean isAutomatic;

    public StartSimulationAction(String name, boolean isAutomatic) {
      super(name);
      this.isAutomatic = isAutomatic;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      KPNGraphComponent graphComponent = (KPNGraphComponent) e.getSource();
      if (graphComponent != null) {
        KPetriNetGraph graph = graphComponent.getGraph();
        if (graph.getSimulator() == null || graph.getSimulator().getSimulationState() != Simulator.SimulationState.PAUSED) {
          graph.updateSimulator((isAutomatic) ? new AutomaticSimulator(graph.getPetriNet()) : new ManualSimulator(graph.getPetriNet()));
        }
        graph.getSimulator().startSimulation();
      }
      editor.initRandomnessSliderValues();
    }

  }

  private static class PauseSimulationAction extends AbstractAction {

    public PauseSimulationAction(String name) {
      super(name);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      KPNGraphComponent graphComponent = (KPNGraphComponent) e.getSource();
      if (graphComponent != null) {
        graphComponent.getGraph().getSimulator().pauseSimulation();
      }
    }

  }

  private static class StopSimulationAction extends AbstractAction {

    public StopSimulationAction(String name) {
      super(name);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      KPNGraphComponent graphComponent = (KPNGraphComponent) e.getSource();
      if (graphComponent != null) {
        KPetriNetGraph graph = graphComponent.getGraph();
        graph.getSimulator().stopSimulation();
      }
    }

  }

  private static class ResetSimulationAction extends AbstractAction {

    public ResetSimulationAction(String name) {
      super(name);
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      KPNGraphComponent graphComponent = (KPNGraphComponent) e.getSource();
      if (graphComponent != null) {
        KPetriNetGraph graph = graphComponent.getGraph();
        graph.getSimulator().stopSimulation();
        graph.getSimulator().resetSimulation();
      }
    }

  }

  public static class SelectAction extends AbstractAction {

    /**
     *
     */
    private static final long serialVersionUID = 6501585024845668187L;

    public SelectAction(String name) {
      super(name);
    }

    /**
     *
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      mxGraph graph = getGraph(e);

      if (!editor.getGraphComponent().getKeyboardHandler().isCommandsActive())
        return;

      if (graph != null) {
        String name = getValue(Action.NAME).toString();

        if (name.equalsIgnoreCase("selectAll")) {
          graph.selectAll();
        } else if (name.equalsIgnoreCase("selectNone")) {
          graph.clearSelection();
        } else if (name.equalsIgnoreCase("selectNext")) {
          graph.selectNextCell();
        } else if (name.equalsIgnoreCase("selectPrevious")) {
          graph.selectPreviousCell();
        } else if (name.equalsIgnoreCase("selectParent")) {
          graph.selectParentCell();
        } else if (name.equalsIgnoreCase("vertices")) {
          graph.selectVertices();
        } else if (name.equalsIgnoreCase("edges")) {
          graph.selectEdges();
        } else {
          graph.selectChildCell();
        }
      }
    }

  }

  public static class MoveAction extends AbstractAction {

    /**
     *
     */
    private static final long serialVersionUID = 6501585024845668187L;
    private final Direction direction;

    public MoveAction(Direction direction) {
      super("Move Action");
      this.direction = direction;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      mxGraph graph = getGraph(e);

      if (!editor.getGraphComponent().getKeyboardHandler().isCommandsActive())
        return;

      if (graph != null) {
        Object[] cells = graph.getSelectionCells();

        if (cells != null && cells.length > 0) {
          int dx = 0, dy = 0;
          final int steps = 1;
          switch (direction) {
            case LEFT -> dx = -steps;
            case RIGHT -> dx = steps;
            case UP -> dy = -steps;
            case DOWN -> dy = steps;
          }
          graph.moveCells(cells, dx, dy);
        }

      }
    }

    private enum Direction {
      LEFT,
      RIGHT,
      UP,
      DOWN
    }

  }

}
