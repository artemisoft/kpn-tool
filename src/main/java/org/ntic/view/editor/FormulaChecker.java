package org.ntic.view.editor;

import org.ntic.model.Agent;
import org.ntic.model.modelchecking.CTLKFormulaParser;
import org.ntic.model.modelchecking.ModelChecker;
import org.ntic.model.modelchecking.ctlk.CTLKFormula;
import org.ntic.util.Utils;
import org.ntic.view.ui.FormulaEditor;

import javax.swing.*;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static org.ntic.Main.editor;

public class FormulaChecker extends JFrame {

  public FormulaChecker(ModelChecker modelChecker) throws HeadlessException {

    this.setTitle(Utils.getResource("modelChecker"));
    this.setType(Window.Type.POPUP);
    this.setResizable(false);

    JTextPane formulaEditor = new FormulaEditor("");

    JLabel result = new JLabel(Utils.getResource("enterFormula"));
    result.setHorizontalAlignment(SwingConstants.CENTER);
    result.setVerticalAlignment(SwingConstants.CENTER);
    result.setFont(new Font(result.getFont().getName(), Font.BOLD, 12));

    JComboBox<Agent> agentJComboBox = new JComboBox<>(modelChecker.getAgentManager().getAgents().toArray(new Agent[]{}));
    agentJComboBox.setSelectedItem(modelChecker.getAgent());
    agentJComboBox.addActionListener(new AgentSelectionAction(modelChecker));
    agentJComboBox.setPreferredSize(new Dimension(128, 32));

    JButton checkButton = new JButton(Utils.getResource("check"));
    checkButton.setPreferredSize(new Dimension(128, 32));
    checkButton.addActionListener(new WriteAction(formulaEditor, modelChecker, result));

    JPanel wrap = new JPanel(new BorderLayout(8, 8));
    wrap.add(new JLabel(Utils.getResource("formula") + " : "), BorderLayout.NORTH);
    wrap.add(formulaEditor, BorderLayout.CENTER);

    JPanel controls = new JPanel();
    controls.setLayout(new FlowLayout(FlowLayout.CENTER, 8, 8));
    controls.add(new JLabel(Utils.getResource("agent") + " : "));
    controls.add(agentJComboBox);
    controls.add(checkButton);

    JPanel content = new JPanel(new BorderLayout(16, 16));
    content.setBorder(BorderFactory.createEmptyBorder(16, 16, 16, 16));
    content.add(wrap, BorderLayout.NORTH);
    content.add(result, BorderLayout.CENTER);
    content.add(controls, BorderLayout.SOUTH);

    this.setIconImage(new ImageIcon(getClass().getResource("/icons/model_checker_32.png")).getImage());
    this.setContentPane(content);
    this.pack();
    this.setLocationRelativeTo(editor.getMainFrame());
    this.setVisible(true);
  }

  private static class AgentSelectionAction implements ActionListener {

    private final ModelChecker modelChecker;

    public AgentSelectionAction(ModelChecker modelChecker) {
      this.modelChecker = modelChecker;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      modelChecker.setAgent((Agent) ((JComboBox) e.getSource()).getSelectedItem());
    }

  }

  private static class WriteAction implements ActionListener {

    private final JTextPane formulaLabel;
    private final ModelChecker modelChecker;
    private final CTLKFormulaParser formulaParser;
    private final JLabel result;

    public WriteAction(JTextPane formulaLabel, ModelChecker modelChecker, JLabel result) {
      this.formulaLabel = formulaLabel;
      this.modelChecker = modelChecker;
      this.formulaParser = new CTLKFormulaParser();
      this.result = result;
    }

    @Override
    public void actionPerformed(ActionEvent e) {
      try {
        CTLKFormula formula = formulaParser.parse(formulaLabel.getText());
        if (modelChecker.modelChecking(formula.toENFExpression(), editor.getSimilarReachabilityGraph(modelChecker.getAgent()))) {
          result.setText(Utils.getResource("satFormula"));
          result.setForeground(new Color(0, 168, 90, 255));
        } else {
          result.setText(Utils.getResource("notSatFormula"));
          result.setForeground(new Color(189, 127, 0));
        }
      } catch (IllegalArgumentException exception) {
        result.setText(exception.getMessage());
        result.setForeground(new Color(206, 0, 58));
      }
    }

  }

}
