package org.ntic.view.editor;

import com.mxgraph.model.mxCell;
import com.mxgraph.swing.util.mxGraphActions;
import org.ntic.model.KnowledgePlace;
import org.ntic.util.Utils;
import org.ntic.view.editor.EditorActions.HistoryAction;

import javax.swing.*;

public class EditorVertexPopupMenu extends JPopupMenu {

  /**
   *
   */
  private static final long serialVersionUID = -3132749140550242191L;

  public EditorVertexPopupMenu(GraphEditor editor) {
    boolean selectionEmpty = editor.getGraph().isSelectionEmpty();
    Object[] selection = editor.getGraph().getSelectionCells();

    if (selectionEmpty) {
      add(editor.bind(Utils.getResource("undo"), new HistoryAction(true), "/icons/undo_16.png"));
      add(editor.bind(Utils.getResource("redo"), new HistoryAction(false), "/icons/redo_16.png"));
      addSeparator();
      add(editor.bind(Utils.getResource("selectEdges"), mxGraphActions.getSelectEdgesAction()));
      add(editor.bind(Utils.getResource("selectAll"), mxGraphActions.getSelectAllAction()));
    } else {
      if (selection.length == 1 && selection[0] instanceof mxCell && editor.getGraph().getGraphPainter().getVertex((mxCell) selection[0]) instanceof KnowledgePlace) {
        add(editor.bind(Utils.getResource("selectAgents"), KPetriNetGraphActions.getSelectAgentsAction((KnowledgePlace) editor.getGraph().getGraphPainter().getVertex((mxCell) selection[0])), "/icons/agents_16.png"));
        addSeparator();
      }
      add(editor.bind(Utils.getResource("cut"), TransferHandler.getCutAction(), "/icons/cut_16.png"));
      add(editor.bind(Utils.getResource("copy"), TransferHandler.getCopyAction(), "/icons/copy_16.png"));
      add(editor.bind(Utils.getResource("paste"), TransferHandler.getPasteAction(), "/icons/paste_16.png"));
      addSeparator();
      add(editor.bind(Utils.getResource("delete"), mxGraphActions.getDeleteAction(), "/icons/erase_16.png"));
      if (selection.length == 1) {
        addSeparator();
        add(editor.bind(Utils.getResource("edit"), mxGraphActions.getEditAction()));
      }
    }
  }

}
