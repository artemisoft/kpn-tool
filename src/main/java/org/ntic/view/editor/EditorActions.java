package org.ntic.view.editor;

import com.mxgraph.io.mxCodec;
import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.util.mxCellRenderer;
import com.mxgraph.util.mxXmlUtils;
import com.mxgraph.util.png.mxPngEncodeParam;
import com.mxgraph.util.png.mxPngImageEncoder;
import com.mxgraph.view.mxGraph;
import org.ntic.model.Agent;
import org.ntic.model.AgentManager;
import org.ntic.model.KPetriNet;
import org.ntic.model.modelchecking.ModelChecker;
import org.ntic.model.reachability.ReachabilityGraph;
import org.ntic.model.reachability.SimilarReachabilityGraph;
import org.ntic.util.Utils;
import org.ntic.view.RGGraph;
import org.ntic.view.SRGGraph;
import org.ntic.view.editor.stax.StaXParser;
import org.ntic.view.editor.stax.StaxValidator;
import org.ntic.view.editor.stax.StaxWriter;
import org.ntic.view.graph.KPNGraphComponent;
import org.ntic.view.graph.KPetriNetGraph;
import org.xml.sax.SAXException;

import javax.imageio.ImageIO;
import javax.swing.*;
import javax.swing.filechooser.FileFilter;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;
import java.awt.image.BufferedImage;
import java.awt.print.PageFormat;
import java.awt.print.Paper;
import java.awt.print.PrinterException;
import java.awt.print.PrinterJob;
import java.beans.PropertyChangeListener;
import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.lang.reflect.Method;
import java.net.URLEncoder;
import java.nio.charset.StandardCharsets;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;

import static org.ntic.Main.editor;

public class EditorActions {

  public static GraphEditor getEditor(ActionEvent e) {
    if (e.getSource() instanceof Component) {
      Component component = (Component) e.getSource();

      while (component != null && !(component instanceof GraphEditor)) {
        component = component.getParent();
      }

      return (GraphEditor) component;
    }

    return null;
  }

  /**
   *
   */
  @SuppressWarnings("serial")
  public static class PrintAction extends AbstractAction {

    /**
     *
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      if (e.getSource() instanceof mxGraphComponent) {
        mxGraphComponent graphComponent = (mxGraphComponent) e.getSource();
        PrinterJob pj = PrinterJob.getPrinterJob();

        if (pj.printDialog()) {
          PageFormat pf = graphComponent.getPageFormat();
          Paper paper = new Paper();
          double margin = 36;
          paper.setImageableArea(margin, margin, paper.getWidth() - margin * 2, paper.getHeight() - margin * 2);
          pf.setPaper(paper);
          pj.setPrintable(graphComponent, pf);

          try {
            pj.print();
          } catch (PrinterException e2) {
            e2.printStackTrace();
          }
        }
      }
    }

  }

  /**
   *
   */
  @SuppressWarnings("serial")
  public static class SaveAction extends AbstractAction {

    /**
     *
     */
    protected boolean showDialog;

    /**
     *
     */
    protected String lastDir = null;

    /**
     *
     */
    public SaveAction(boolean showDialog) {
      this.showDialog = showDialog;
    }

    /**
     *
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      GraphEditor editor = getEditor(e);

      if (editor != null) {
        KPNGraphComponent graphComponent = editor.getGraphComponent();

        if (!graphComponent.getKeyboardHandler().isCommandsActive())
          return;

        KPetriNetGraph graph = graphComponent.getGraph();
        FileFilter selectedFilter = null;
        DefaultFileFilter xmlPngFilter = new DefaultFileFilter(".png", "PNG" + Utils.getResource("file") + " (.png)");

        String filename;
        boolean dialogShown = false;

        if (showDialog || editor.getCurrentFile() == null) {
          String wd;

          if (lastDir != null) {
            wd = lastDir;
          } else if (editor.getCurrentFile() != null) {
            wd = editor.getCurrentFile().getParent();
          } else {
            wd = System.getProperty("user.dir");
          }

          JFileChooser fc = new JFileChooser(wd);

          fc.addChoosableFileFilter(xmlPngFilter);
          DefaultFileFilter defaultFileFilter = new DefaultFileFilter(".kpnml", "KPNML file (.kpnml)");
          fc.addChoosableFileFilter(defaultFileFilter);
          fc.setFileFilter(defaultFileFilter);

          int rc = fc.showDialog(null, Utils.getResource("save"));
          dialogShown = true;

          if (rc != JFileChooser.APPROVE_OPTION) {
            return;
          } else {
            lastDir = fc.getSelectedFile().getParent();
          }

          filename = fc.getSelectedFile().getAbsolutePath();
          selectedFilter = fc.getFileFilter();

          if (selectedFilter instanceof DefaultFileFilter) {
            String ext = ((DefaultFileFilter) selectedFilter).getExtension();

            if (!filename.toLowerCase().endsWith(ext)) {
              filename += ext;
            }
          }

          if (new File(filename).exists() && JOptionPane.showConfirmDialog(graphComponent, Utils.getResource("overwriteExistingFile")) != JOptionPane.YES_OPTION) {
            return;
          }
        } else {
          filename = editor.getCurrentFile().getAbsolutePath();
        }

        try {
          String ext = filename.substring(filename.lastIndexOf('.') + 1);

          if (ext.equalsIgnoreCase("kpnml")) {
            graph.getPetriNet().setName(filename.substring(filename.lastIndexOf('\\') + 1, filename.lastIndexOf('.')));
            StaxWriter writer = new StaxWriter(graph, filename);
            writer.saveNet();
            editor.setModified(false);
            editor.setCurrentFile(new File(filename));
          } else {
            Color bg = null;

            if ((!ext.equalsIgnoreCase("gif") && !ext.equalsIgnoreCase("png")) || JOptionPane.showConfirmDialog(graphComponent, Utils.getResource("transparentBackground")) != JOptionPane.YES_OPTION) {
              bg = graphComponent.getBackground();
            }

            if (selectedFilter == xmlPngFilter || (editor.getCurrentFile() != null && ext.equalsIgnoreCase("png") && !dialogShown)) {
              saveXmlPng(editor, filename, bg);
            } else {
              BufferedImage image = mxCellRenderer.createBufferedImage(graph, null, 1, bg, graphComponent.isAntiAlias(), null, graphComponent.getCanvas());

              if (image != null) {
                ImageIO.write(image, ext, new File(filename));
              } else {
                JOptionPane.showMessageDialog(graphComponent, Utils.getResource("noImageData"));
              }
            }
          }
        } catch (Throwable ex) {
          ex.printStackTrace();
          JOptionPane.showMessageDialog(graphComponent, ex.toString(), Utils.getResource("error"), JOptionPane.ERROR_MESSAGE);
        }
      }
    }

    /**
     * Saves XML+PNG format.
     */
    protected void saveXmlPng(GraphEditor editor, String filename, Color bg) throws IOException {
      mxGraphComponent graphComponent = editor.getGraphComponent();
      mxGraph graph = graphComponent.getGraph();

      // Creates the image for the PNG file
      BufferedImage image = mxCellRenderer.createBufferedImage(graph, null, 1, bg, graphComponent.isAntiAlias(), null, graphComponent.getCanvas());

      // Creates the URL-encoded XML data
      mxCodec codec = new mxCodec();
      String xml = URLEncoder.encode(mxXmlUtils.getXml(codec.encode(graph.getModel())), StandardCharsets.UTF_8);
      mxPngEncodeParam param = mxPngEncodeParam.getDefaultEncodeParam(image);
      param.setCompressedText(new String[]{"mxGraphModel", xml});

      // Saves as a PNG file
      try (FileOutputStream outputStream = new FileOutputStream(new File(filename))) {
        mxPngImageEncoder encoder = new mxPngImageEncoder(outputStream, param);

        encoder.encode(image);

        editor.setModified(false);
        editor.setCurrentFile(new File(filename));
      }
    }

  }

  public static class ToggleProperty extends JCheckBox {

    public ToggleProperty(Object target, String name, String fieldName, boolean refresh) {
      this(target, name, fieldName, refresh, null);
    }

    public ToggleProperty(final Object target, String name, final String fieldName, final boolean refresh, ActionListener listener) {
      super(name);

      // Since action listeners are processed last to first we add the given
      // listener here which means it will be processed after the one below
      if (listener != null) {
        addActionListener(listener);
      }

      addActionListener(e -> execute(target, fieldName, refresh));

      /*
       * (non-Javadoc)
       * @see java.beans.PropertyChangeListener#propertyChange(java.beans.PropertyChangeEvent)
       */
      PropertyChangeListener propertyChangeListener = evt -> {
        if (evt.getPropertyName().equalsIgnoreCase(fieldName)) {
          update(target, fieldName);
        }
      };

      if (target instanceof mxGraphComponent) {
        ((mxGraphComponent) target).addPropertyChangeListener(propertyChangeListener);
      } else if (target instanceof mxGraph) {
        ((mxGraph) target).addPropertyChangeListener(propertyChangeListener);
      }

      update(target, fieldName);
    }

    public void execute(Object target, String fieldName, boolean refresh) {
      if (target != null && fieldName != null) {
        try {
          Method getter = target.getClass().getMethod("is" + fieldName);
          Method setter = target.getClass().getMethod("set" + fieldName, boolean.class);

          Object current = getter.invoke(target);

          if (current instanceof Boolean) {
            boolean value = !(Boolean) current;
            setter.invoke(target, value);
            setSelected(value);
          }

          if (refresh) {
            mxGraph graph = null;

            if (target instanceof mxGraph) {
              graph = (mxGraph) target;
            } else if (target instanceof mxGraphComponent) {
              graph = ((mxGraphComponent) target).getGraph();
            }

            if (graph != null) {
              graph.refresh();
            }
          }
        } catch (Exception e) {
          // ignore
        }
      }
    }

    public void update(Object target, String fieldName) {
      if (target != null && fieldName != null) {
        try {
          Method getter = target.getClass().getMethod("is" + fieldName);

          Object current = getter.invoke(target);

          if (current instanceof Boolean) {
            setSelected((Boolean) current);
          }
        } catch (Exception e) {
          // ignore
        }
      }
    }

  }

  /**
   *
   */
  @SuppressWarnings("serial")
  public static class HistoryAction extends AbstractAction {

    /**
     *
     */
    protected boolean undo;

    /**
     *
     */
    public HistoryAction(boolean undo) {
      this.undo = undo;
    }

    /**
     *
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      GraphEditor editor = getEditor(e);

      if (editor != null) {

        if (!editor.getGraphComponent().getKeyboardHandler().isCommandsActive())
          return;

        if (undo) {
          editor.getUndoManager().undo();
        } else {
          editor.getUndoManager().redo();
          //editor.getGraph().redo();
        }
      }
    }

  }

  /**
   *
   */
  @SuppressWarnings("serial")
  public static class NewAction extends AbstractAction {

    /**
     *
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      GraphEditor editor = getEditor(e);

      if (editor != null) {

        if (!editor.getGraphComponent().getKeyboardHandler().isCommandsActive())
          return;

        int saveChanges = -1;
        if (editor.isModified()) {
          saveChanges = JOptionPane.showConfirmDialog(editor, Utils.getResource("saveChanges"));
        }
        if (editor.isModified() && saveChanges == JOptionPane.YES_OPTION)
          new SaveAction(false).actionPerformed(e);

        if (!editor.isModified() || saveChanges != JOptionPane.CANCEL_OPTION) {
          KPetriNetGraph graph = editor.getGraph();
          graph.updateState(new KPetriNet(Utils.getResource("untitled")), null, null);

          editor.setModified(false);
          editor.setCurrentFile(null);
          editor.getGraphComponent().zoomAndCenter();
          editor.getSidebar().changeGraph(graph);
          editor.setSimilarReachabilityGraphs(null);
          editor.setReachabilityGraph(null);
        }

      }

    }

  }

  /**
   *
   */
  @SuppressWarnings("serial")
  public static class OpenAction extends AbstractAction {

    /**
     *
     */
    protected String lastDir;

    /**
     *
     */
    @Override
    public void actionPerformed(ActionEvent e) {
      GraphEditor editor = getEditor(e);

      if (editor != null) {
        if (!editor.getGraphComponent().getKeyboardHandler().isCommandsActive())
          return;

        int saveChanges = -1;
        if (editor.isModified()) {
          saveChanges = JOptionPane.showConfirmDialog(editor, Utils.getResource("saveChanges"));
        }

        if (editor.isModified() && saveChanges == JOptionPane.YES_OPTION)
          new SaveAction(false).actionPerformed(e);

        if (!editor.isModified() || saveChanges != JOptionPane.CANCEL_OPTION) {
          KPetriNetGraph graph = editor.getGraph();
          if (graph != null) {
            String wd = (lastDir != null) ? lastDir : System.getProperty("user.dir") + "/target/classes/kpnml";

            JFileChooser fc = new JFileChooser(wd);

            DefaultFileFilter defaultFileFilter = new DefaultFileFilter(".kpnml", "KPNML file (.kpnml)");
            fc.addChoosableFileFilter(defaultFileFilter);
            fc.setFileFilter(defaultFileFilter);

            int rc = fc.showDialog(null, "Open file");

            if (rc == JFileChooser.APPROVE_OPTION) {
              lastDir = fc.getSelectedFile().getParent();

              try {
                StaxValidator validator = new StaxValidator();
                if (!validator.validate(fc.getSelectedFile().getAbsolutePath())) {
                  JOptionPane.showMessageDialog(editor.getGraphComponent(), "This File is Not valid", "Error", JOptionPane.ERROR_MESSAGE);
                } else {
                  try {
                    StaXParser parser = new StaXParser();
                    parser.readNet(fc.getSelectedFile().getAbsolutePath(), editor.getGraph());
                    editor.setCurrentFile(fc.getSelectedFile());
                    resetEditor(editor);
                    editor.getSidebar().changeGraph(graph);
                    editor.setSimilarReachabilityGraphs(null);
                    editor.setReachabilityGraph(null);
                  } catch (IOException | ParserConfigurationException | XPathExpressionException | SAXException ex) {
                    ex.printStackTrace();
                    JOptionPane.showMessageDialog(editor.getGraphComponent(), ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
                  }
                }
              } catch (SAXException | IOException ex) {
                JOptionPane.showMessageDialog(editor.getGraphComponent(), "Could not validate this file", "Error", JOptionPane.ERROR_MESSAGE);
                ex.printStackTrace();
              }
            }
          }
        }

      }

    }

    protected void resetEditor(GraphEditor editor) {
      editor.setModified(false);
      editor.getUndoManager().clear();
      editor.getGraphComponent().zoomAndCenter();
      editor.getGraphComponent().repaint();
    }

  }

  public static class RGAction extends AbstractAction {

    @Override
    public void actionPerformed(ActionEvent e) {
      ReachabilityGraph rGraph = editor.getReachabilityGraph() == null ? new ReachabilityGraph(editor.getGraph().getPetriNet()) : editor.getReachabilityGraph();

      rGraph.build();
      editor.setReachabilityGraph(rGraph);

      RGGraph graph = new RGGraph(rGraph);

      editor.createRGFrame(graph);

    }

  }

  public static class SRGAction extends AbstractAction {

    public static Set<ReachabilityGraph> createSRGs() {

      AgentManager agentManager = editor.getGraph().getAgentManager();
      Set<ReachabilityGraph> rGraphs = new HashSet<>();

      for (Agent agent : agentManager.getAgents()) {
        SimilarReachabilityGraph similarReachabilityGraph = new SimilarReachabilityGraph(editor.getPetriNet(), agentManager, agent);
        similarReachabilityGraph.build();
        rGraphs.add(similarReachabilityGraph);
      }

      editor.setSimilarReachabilityGraphs(rGraphs);
      return rGraphs;
    }

    @Override
    public void actionPerformed(ActionEvent e) {

      Set<ReachabilityGraph> rGraphs = createSRGs();
      editor.setSimilarReachabilityGraphs(rGraphs);

      RGGraph graph = new SRGGraph(rGraphs.stream().map(g -> (SimilarReachabilityGraph) g).collect(Collectors.toSet()), editor.getGraphComponent().getGraph().getAgentManager());

      editor.createRGFrame(graph);

    }

  }

  public static class ModelCheckerAction extends AbstractAction {

    @Override
    public void actionPerformed(ActionEvent e) {
      if (editor.getSimilarReachabilityGraphs() == null) {
        SRGAction.createSRGs();
      }
      AgentManager agentManager = editor.getGraph().getAgentManager();
      Agent a3 = agentManager.getAgents().toArray(new Agent[]{})[0];
      ModelChecker modelChecker = new ModelChecker(agentManager, a3);
      new FormulaChecker(modelChecker);
    }

  }

}
