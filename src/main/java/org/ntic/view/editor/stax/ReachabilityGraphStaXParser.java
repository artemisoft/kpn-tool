package org.ntic.view.editor.stax;

import org.ntic.model.Agent;
import org.ntic.model.AgentManager;
import org.ntic.model.KPetriNet;
import org.ntic.model.Transition;
import org.ntic.model.reachability.Marking;
import org.ntic.model.reachability.ReachabilityGraph;
import org.ntic.model.reachability.SimilarReachabilityGraph;
import org.ntic.model.reachability.TransitionArc;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Set;

public class ReachabilityGraphStaXParser {

  public void readGraph(String file, Set<ReachabilityGraph> graph, KPetriNet petriNet, AgentManager agentManager) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

    DocumentBuilder builder = factory.newDocumentBuilder();
    File fileXML = new File(file);
    Document xml = builder.parse(fileXML);

    Element root = xml.getDocumentElement();
    XPathFactory xpf = XPathFactory.newInstance();
    XPath path = xpf.newXPath();

    HashMap<String, Marking> markings = new HashMap<>();
    HashMap<String, Agent> agents = new HashMap<>();

    String expression = "//graph/group";
    NodeList list = (NodeList) path.evaluate(expression, root, XPathConstants.NODESET);
    int nodesLength = list.getLength();

    for (int i = 0; i < nodesLength; i++) {
      expression = "marking";
      Node group = list.item(i);
      String agentName = group.getAttributes().getNamedItem("agent").getTextContent();

      NodeList markingList = (NodeList) path.evaluate(expression, group, XPathConstants.NODESET);
      ReachabilityGraph reachabilityGraph = new SimilarReachabilityGraph(petriNet, agentManager, agentManager.getAgents().stream().filter(a -> a.getName().equals(agentName)).findFirst().get());
      graph.add(reachabilityGraph);

      readGroup(reachabilityGraph, path, markings, agents, markingList, group);
    }

  }

  public void readGraph(String file, ReachabilityGraph reachabilityGraph) throws ParserConfigurationException, SAXException, XPathExpressionException, IOException {

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

    DocumentBuilder builder = factory.newDocumentBuilder();
    File fileXML = new File(file);
    Document xml = builder.parse(fileXML);

    Element root = xml.getDocumentElement();
    XPathFactory xpf = XPathFactory.newInstance();
    XPath path = xpf.newXPath();

    HashMap<String, Marking> markings = new HashMap<>();
    HashMap<String, Agent> agents = new HashMap<>();

    String expression = "//graph/group";
    NodeList list = (NodeList) path.evaluate(expression, root, XPathConstants.NODESET);
    int nodesLength = list.getLength();

    for (int i = 0; i < nodesLength; i++) {
      expression = "marking";
      Node group = list.item(i);
      NodeList markingList = (NodeList) path.evaluate(expression, group, XPathConstants.NODESET);

      readGroup(reachabilityGraph, path, markings, agents, markingList, group);
    }

  }

  private void readGroup(ReachabilityGraph reachabilityGraph, XPath path, HashMap<String, Marking> markings, HashMap<String, Agent> agents, NodeList markingList, Node node) throws XPathExpressionException {
    String expression;
    for (int j = 0; j < markingList.getLength(); j++) {
      Node n = markingList.item(j);
      String id = n.getAttributes().getNamedItem("id").getTextContent();
      String equivalenceClass = n.getAttributes().getNamedItem("equivalenceClass").getTextContent();
      String name = n.getAttributes().getNamedItem("name").getTextContent();
      String agentName = n.getAttributes().getNamedItem("agent") != null ? n.getAttributes().getNamedItem("agent").getTextContent() : "";

      Marking marking = new Marking();
      marking.setName(name);
      marking.setEquivalenceClass(Integer.parseInt(equivalenceClass));
      if (!agentName.isBlank()) {
        if (agents.get(agentName) == null) {
          agents.put(agentName, new Agent(agentName));
        }
        marking.setAgent(agents.get(agentName));
      }

      expression = "item";
      path.compile(expression);

      NodeList itemsList = (NodeList) path.evaluate(expression, n, XPathConstants.NODESET);

      for (int k = 0; k < itemsList.getLength(); k++) {
        Node item = itemsList.item(k);
        String key = item.getAttributes().getNamedItem("key").getTextContent();
        boolean value = Boolean.parseBoolean(item.getAttributes().getNamedItem("value").getTextContent());
        marking.put(key, value);
      }

      markings.put(id, marking);
      reachabilityGraph.addVertex(marking);
    }

    expression = "arc";
    NodeList arcList = (NodeList) path.evaluate(expression, node, XPathConstants.NODESET);

    for (int j = 0; j < arcList.getLength(); j++) {
      Node n = arcList.item(j);

      String transitionName = n.getAttributes().getNamedItem("transition").getTextContent();
      String source = n.getAttributes().getNamedItem("source").getTextContent();
      String target = n.getAttributes().getNamedItem("target").getTextContent();

      reachabilityGraph.addEdge(markings.get(source), markings.get(target), new TransitionArc(new Transition(transitionName)));

    }
  }

}

