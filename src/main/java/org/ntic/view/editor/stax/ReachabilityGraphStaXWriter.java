package org.ntic.view.editor.stax;

import com.sun.xml.txw2.output.IndentingXMLStreamWriter;
import org.ntic.model.reachability.Marking;
import org.ntic.model.reachability.ReachabilityGraph;
import org.ntic.model.reachability.SimilarReachabilityGraph;
import org.ntic.model.reachability.TransitionArc;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Set;

public class ReachabilityGraphStaXWriter {

  public static final String NAMESPACE_URI = "http://www.pnml.org/version-2009/grammar/pnml";

  private final String file;
  private final Set<ReachabilityGraph> reachabilityGraph;
  private XMLStreamWriter xMLStreamWriter;

  private int markingId = 1;

  public ReachabilityGraphStaXWriter(Set<ReachabilityGraph> reachabilityGraph, String file) {
    this.reachabilityGraph = reachabilityGraph;
    this.file = file;
  }

  public void saveGraph() {
    try {

      XMLOutputFactory xMLOutputFactory = XMLOutputFactory.newInstance();
      FileWriter fileWriter = new FileWriter(this.file);
      xMLStreamWriter = new IndentingXMLStreamWriter(xMLOutputFactory.createXMLStreamWriter(fileWriter));

      xMLStreamWriter.writeStartDocument("");
      xMLStreamWriter.writeStartElement("", "graph", NAMESPACE_URI);
      xMLStreamWriter.writeNamespace("", NAMESPACE_URI);

      HashMap<Marking, String> ids = new HashMap<>();

      for (Object o : reachabilityGraph.toArray()) {
        if (o instanceof SimilarReachabilityGraph) {
          o("group", "agent", ((SimilarReachabilityGraph) o).getAgent().getName());
        } else {
          o("group");
        }

        ReachabilityGraph graph = (ReachabilityGraph) o;
        for (Marking marking : new ArrayList<>(graph.vertexSet())) {

          ids.put(marking, String.valueOf(markingId));
          if (marking.getAgent() == null) {
            o("marking", "id", String.valueOf(markingId++), "equivalenceClass", String.valueOf(marking.getEquivalenceClass()), "name", marking.getName());

          } else {
            o("marking", "id", String.valueOf(markingId++), "equivalenceClass", String.valueOf(marking.getEquivalenceClass()), "name", marking.getName(), "agent", marking.getAgent().getName());

          }
          for (String key : marking.keySet()) {
            e("item", "key", key, "value", String.valueOf(marking.get(key)));
          }

          c("marking");
        }

        for (TransitionArc arc : new ArrayList<>(graph.edgeSet())) {
          e("arc", "transition", arc.getTransition().getName(), "source", ids.get(arc.getSource()), "target", ids.get(arc.getTarget()));
        }
        c("group");
      }

      xMLStreamWriter.writeEndDocument();

      xMLStreamWriter.flush();
      xMLStreamWriter.close();
      fileWriter.close();

    } catch (XMLStreamException | IOException e) {
      e.printStackTrace();
    }

  }

  @SuppressWarnings("unused")
  private void c(String tag) throws XMLStreamException {
    xMLStreamWriter.writeEndElement();
  }

  private void o(String tag, String... attributes) throws XMLStreamException {

    if (attributes.length % 2 != 0)
      throw new IllegalArgumentException("every attribute should have a value");

    xMLStreamWriter.writeStartElement(tag);
    for (int i = 0; i < attributes.length; i += 2) {
      xMLStreamWriter.writeAttribute(attributes[i], attributes[i + 1]);
    }
    xMLStreamWriter.writeCharacters("");

  }

  private void e(String tag, String... attributes) throws XMLStreamException {

    if (attributes.length % 2 != 0)
      throw new IllegalArgumentException("every attribute should have a value");
    xMLStreamWriter.writeEmptyElement(tag);
    for (int i = 0; i < attributes.length; i += 2) {
      xMLStreamWriter.writeAttribute(attributes[i], attributes[i + 1]);
    }
  }

}
