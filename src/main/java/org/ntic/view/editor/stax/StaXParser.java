package org.ntic.view.editor.stax;

import org.javatuples.Pair;
import org.ntic.model.*;
import org.ntic.view.graph.KPetriNetGraph;
import org.w3c.dom.Document;
import org.w3c.dom.Element;
import org.w3c.dom.Node;
import org.w3c.dom.NodeList;
import org.xml.sax.SAXException;

import javax.xml.parsers.DocumentBuilder;
import javax.xml.parsers.DocumentBuilderFactory;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPath;
import javax.xml.xpath.XPathConstants;
import javax.xml.xpath.XPathExpressionException;
import javax.xml.xpath.XPathFactory;
import java.awt.*;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

public class StaXParser {

  public void readNet(String file, KPetriNetGraph graph) throws ParserConfigurationException, IOException, SAXException, XPathExpressionException {

    DocumentBuilderFactory factory = DocumentBuilderFactory.newInstance();

    DocumentBuilder builder = factory.newDocumentBuilder();
    File fileXML = new File(file);
    Document xml = builder.parse(fileXML);

    Element root = xml.getDocumentElement();
    XPathFactory xpf = XPathFactory.newInstance();
    XPath path = xpf.newXPath();

    String expression = "//net/name/text";
    Node net = (Node) path.evaluate(expression, root, XPathConstants.NODE);
    String netName = net.getTextContent();

    expression = "//net/page/arc";
    NodeList list = (NodeList) path.evaluate(expression, root, XPathConstants.NODESET);
    int nodesLength = list.getLength();

    ArrayList<Vertex> vertices = new ArrayList<>();
    HashMap<Vertex, Point> positions = new HashMap<>();
    HashMap<Pair<String, String>, Point[]> arcPositionsWithTerminalsIds = new HashMap<>();
    HashMap<Pair<Vertex, Vertex>, Point[]> arcPositionsWithTerminals = new HashMap<>();
    HashMap<String, List<String>> sourceToTargets = new HashMap<>();
    HashMap<String, List<String>> targetToSources = new HashMap<>();
    HashMap<String, Vertex> sources = new HashMap<>();
    HashMap<String, Vertex> targets = new HashMap<>();
    List<Pair<Vertex, Vertex>> arcTerminals = new ArrayList<>();

    for (int i = 0; i < nodesLength; i++) {
      Node n = list.item(i);

      String source = n.getAttributes().getNamedItem("source").getTextContent();
      String target = n.getAttributes().getNamedItem("target").getTextContent();
      List<String> targetIds = sourceToTargets.get(source);
      List<String> sourceIds = targetToSources.get(target);
      if (targetIds == null)
        targetIds = new ArrayList<>();
      if (sourceIds == null)
        sourceIds = new ArrayList<>();
      targetIds.add(target);
      sourceIds.add(source);
      sourceToTargets.put(source, targetIds);
      targetToSources.put(target, sourceIds);

      expression = "graphics/position";
      path.compile(expression);

      NodeList positionsList = (NodeList) path.evaluate(expression, n, XPathConstants.NODESET);

      Point[] points = new Point[positionsList.getLength()];
      for (int j = 0; j < positionsList.getLength(); j++) {
        Node item = positionsList.item(j);
        int x = (int) Double.parseDouble(item.getAttributes().getNamedItem("x").getTextContent());
        int y = (int) Double.parseDouble(item.getAttributes().getNamedItem("y").getTextContent());
        points[j] = new Point(x, y);
      }

      arcPositionsWithTerminalsIds.put(new Pair<>(source, target), points);
    }

    expression = "//net/page/place";
    list = (NodeList) path.evaluate(expression, root, XPathConstants.NODESET);
    nodesLength = list.getLength();

    for (int i = 0; i < nodesLength; i++) {
      Node n = list.item(i);

      String id = n.getAttributes().getNamedItem("id").getTextContent();

      expression = "name/text";
      path.compile(expression);
      String placeName = ((Node) path.evaluate(expression, n, XPathConstants.NODE)).getTextContent();

      expression = "initialMarking/text";
      path.compile(expression);
      boolean initialMarking = ((Node) path.evaluate(expression, n, XPathConstants.NODE)).getTextContent().equals("1");

      expression = "type";
      path.compile(expression);
      String placeType = ((Node) path.evaluate(expression, n, XPathConstants.NODE)).getTextContent();
      Place place;

      expression = "graphics/position";
      path.compile(expression);
      int x = Integer.parseInt(((Node) path.evaluate(expression, n, XPathConstants.NODE)).getAttributes().getNamedItem("x").getTextContent());
      int y = Integer.parseInt(((Node) path.evaluate(expression, n, XPathConstants.NODE)).getAttributes().getNamedItem("y").getTextContent());

      if (placeType.equals("state")) {
        place = new StatePlace(placeName, initialMarking);

      } else {
        place = new KnowledgePlace(placeName, initialMarking);
      }
      place.setId(id);
      vertices.add(place);
      positions.put(place, new Point(x, y));

      if (sourceToTargets.get(id) != null) {
        sources.put(id, place);
      }
      if (targetToSources.get(id) != null) {
        targets.put(id, place);
      }
    }

    expression = "//net/page/transition";
    list = (NodeList) path.evaluate(expression, root, XPathConstants.NODESET);
    nodesLength = list.getLength();

    for (int i = 0; i < nodesLength; i++) {
      Node n = list.item(i);
      String id = n.getAttributes().getNamedItem("id").getTextContent();

      expression = "name/text";
      path.compile(expression);
      Transition transition = new Transition(((Node) path.evaluate(expression, n, XPathConstants.NODE)).getTextContent());

      expression = "graphics/position";
      path.compile(expression);
      int x = Integer.parseInt(((Node) path.evaluate(expression, n, XPathConstants.NODE)).getAttributes().getNamedItem("x").getTextContent());
      int y = Integer.parseInt(((Node) path.evaluate(expression, n, XPathConstants.NODE)).getAttributes().getNamedItem("y").getTextContent());

      vertices.add(transition);
      positions.put(transition, new Point(x, y));

      if (sourceToTargets.get(id) != null) {
        sources.put(id, transition);
      }
      if (targetToSources.get(id) != null) {
        targets.put(id, transition);
      }
    }
    for (String key : sourceToTargets.keySet()) {
      for (String id : sourceToTargets.get(key)) {
        arcPositionsWithTerminals.put(new Pair<>(sources.get(key), targets.get(id)), arcPositionsWithTerminalsIds.get(new Pair<>(key, id)));
        arcTerminals.add(new Pair<>(sources.get(key), targets.get(id)));
      }
    }

    KPetriNet petriNet = new KPetriNet(netName, vertices, arcTerminals);
    graph.updateState(petriNet, positions, arcPositionsWithTerminals);

    expression = "//agent";
    list = (NodeList) path.evaluate(expression, root, XPathConstants.NODESET);
    nodesLength = list.getLength();

    for (int i = 0; i < nodesLength; i++) {
      Node n = list.item(i);
      String name = n.getAttributes().getNamedItem("name").getTextContent();
      Agent agent = graph.getAgentManager().addAgent(name, true);

      expression = "place-ref";
      path.compile(expression);

      NodeList placeRefList = (NodeList) path.evaluate(expression, n, XPathConstants.NODESET);

      for (int j = 0; j < placeRefList.getLength(); j++) {
        String id = placeRefList.item(j).getAttributes().getNamedItem("id").getTextContent();
        KnowledgePlace place = (KnowledgePlace) vertices.stream().filter(v -> v.getId().equals(id)).findFirst().get();
        graph.getAgentManager().associateAgentKnowledgePlace(agent, place);
      }
    }

  }

}

