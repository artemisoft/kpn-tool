package org.ntic.view.editor.stax;

import com.mxgraph.util.mxPoint;
import com.sun.xml.txw2.output.IndentingXMLStreamWriter;
import org.ntic.model.*;
import org.ntic.view.graph.KPetriNetGraph;

import javax.xml.stream.XMLOutputFactory;
import javax.xml.stream.XMLStreamException;
import javax.xml.stream.XMLStreamWriter;
import java.io.FileWriter;
import java.io.IOException;
import java.util.HashMap;
import java.util.stream.Collectors;

public class StaxWriter {

  public static final String NAMESPACE_URI = "http://www.pnml.org/version-2009/grammar/pnml";
  public static final String NET_TYPE_URL = "http://www.pnml.org/version-2009/grammar/ptnet";
  private static final String PNML = "pnml";
  private static int netId = 1;
  private static int pageId = 1;
  private static int placeId = 1;
  private final String file;
  private final KPetriNet petriNet;
  private final KPetriNetGraph graph;
  private int agentId = 1;
  private XMLStreamWriter xMLStreamWriter;

  public StaxWriter(KPetriNetGraph graph, String file) {
    this.graph = graph;
    this.petriNet = graph.getPetriNet();
    this.file = file;
  }

  public void saveNet() {
    try {
      //todo before saving the file make sure the net is valid

      XMLOutputFactory xMLOutputFactory = XMLOutputFactory.newInstance();
      FileWriter fileWriter = new FileWriter(this.file);
      xMLStreamWriter = new IndentingXMLStreamWriter(xMLOutputFactory.createXMLStreamWriter(fileWriter));

      xMLStreamWriter.writeStartDocument("");
      xMLStreamWriter.writeStartElement("", PNML, NAMESPACE_URI);
      xMLStreamWriter.writeNamespace("", NAMESPACE_URI);

      HashMap<Vertex, String> ids = new HashMap<>();

      o("net", "", "id", "i" + netId++, "type", NET_TYPE_URL);
      o("name", "");
      o("text", petriNet.getName());
      c("text");
      c("name");
      o("page", "", "id", "i" + pageId++);

      for (Vertex vertex : petriNet.vertexSet().stream().filter(v -> v instanceof Place).collect(Collectors.toList())) {

        String id = "cId" + placeId++ + "i" + netId;
        vertex.setId(id);

        o("place", "", "id", id);
        ooccoec(vertex);
        o("type", vertex.getType() == Vertex.VertexType.KNOWLEDGE_PLACE ? "knowledge" : "state");
        c("type");
        o("initialMarking", "");
        o("text", ((Place) vertex).isInitiallyMarked() ? "1" : "0");
        c("text");
        c("initialMarking");
        c("place");
        ids.put(vertex, id);

      }
      for (Vertex vertex : petriNet.vertexSet().stream().filter(v -> v instanceof Transition).collect(Collectors.toList())) {
        String id = "cId" + placeId++ + "i" + netId;
        o("transition", "", "id", id);
        ooccoec(vertex);
        c("transition");
        ids.put(vertex, id);
      }
      for (Arc arc : petriNet.edgeSet()) {
        o("arc", "", "id", ("cId" + placeId++ + "i" + netId), "source", ids.get(arc.getSource()), "target", ids.get(arc.getTarget()));
        o("graphics", "");

        for (mxPoint point : (graph.getView().getState(graph.getGraphPainter().getCellFromArc(arc))).getAbsolutePoints()) {
          e("x", String.valueOf(point.getX()), "y", String.valueOf(point.getY()));
        }

        c("graphics");
        c("arc");
      }

      c("page");
      c("net");

      for (Agent agent : graph.getAgentManager().getAgents()) {
        o("agent", "", "name", agent.getName(), "id", "ai" + agentId++);
        for (KnowledgePlace place : graph.getAgentManager().getAssociatedPlaces(agent)) {
          o("place-ref", "", "id", place.getId());
          c("place-ref");
        }
        c("agent");
      }

      netId++;
      c("pnml");

      xMLStreamWriter.writeEndDocument();

      xMLStreamWriter.flush();
      xMLStreamWriter.close();
      fileWriter.close();

    } catch (XMLStreamException | IOException e) {
      e.printStackTrace();
    }

  }

  private void ooccoec(Vertex vertex) throws XMLStreamException {
    o("name", "");
    o("text", vertex.getName());
    c("text");
    c("name");
    o("graphics", "");
    e("x", String.valueOf(graph.getGraphPainter().getVertexPosition(vertex).x), "y", String.valueOf(graph.getGraphPainter().getVertexPosition(vertex).y));
    c("graphics");
  }

  @SuppressWarnings("unused")
  private void c(String tag) throws XMLStreamException {
    xMLStreamWriter.writeEndElement();
  }

  private void o(String tag, String content, String... attributes) throws XMLStreamException {

    if (attributes.length % 2 != 0)
      throw new IllegalArgumentException("every attribute should have a value");

    xMLStreamWriter.writeStartElement(tag);
    for (int i = 0; i < attributes.length; i += 2) {
      xMLStreamWriter.writeAttribute(attributes[i], attributes[i + 1]);
    }
    xMLStreamWriter.writeCharacters(content);

  }

  private void e(String... attributes) throws XMLStreamException {

    if (attributes.length % 2 != 0)
      throw new IllegalArgumentException("every attribute should have a value");
    xMLStreamWriter.writeEmptyElement("position");
    for (int i = 0; i < attributes.length; i += 2) {
      xMLStreamWriter.writeAttribute(attributes[i], attributes[i + 1]);
    }
  }

}
