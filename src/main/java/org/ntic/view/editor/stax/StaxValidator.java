package org.ntic.view.editor.stax;

import com.thaiopensource.relaxng.jaxp.XMLSyntaxSchemaFactory;
import org.apache.xerces.dom.DOMInputImpl;
import org.ntic.util.Utils;
import org.w3c.dom.ls.LSInput;
import org.xml.sax.InputSource;
import org.xml.sax.SAXException;
import org.xml.sax.SAXParseException;

import javax.xml.XMLConstants;
import javax.xml.transform.sax.SAXSource;
import javax.xml.transform.stream.StreamSource;
import javax.xml.validation.Schema;
import javax.xml.validation.SchemaFactory;
import javax.xml.validation.Validator;
import java.io.File;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URI;
import java.net.URL;

public class StaxValidator {

  public boolean validate(String file) throws IOException, SAXException {
    if (file == null)
      throw new IllegalArgumentException("File shouldn't be null");

    System.setProperty(SchemaFactory.class.getName() + ":" + XMLConstants.RELAXNG_NS_URI, XMLSyntaxSchemaFactory.class.getCanonicalName()); // necessary precaution
    SchemaFactory schemaFactory = XMLSyntaxSchemaFactory.newInstance(XMLConstants.RELAXNG_NS_URI);
    schemaFactory.setResourceResolver((type, namespaceURI, publicId, systemId, baseURI) -> {
      LSInput input = new DOMInputImpl();
      input.setPublicId(publicId);
      input.setSystemId(systemId);
      input.setBaseURI(baseURI);
      input.setCharacterStream(new InputStreamReader(getSchemaAsStream(input.getSystemId(), input.getBaseURI())));
      return input;
    });

    SAXSource source = new SAXSource(new InputSource(Utils.class.getClassLoader().getResourceAsStream("kpnml.rng")));
    final Schema schema = schemaFactory.newSchema(source);
    final Validator validator = schema.newValidator();
    try {
      validator.validate(new StreamSource(new File(file)));
    } catch (SAXParseException e) {
      return false;
    }
    return true;
  }

  /*
   * You can leave out the web stuff if you are sure that everything is
   * available on your machine
   */
  private InputStream getSchemaFromWeb(String baseUri, String systemId) {
    try {
      URI uri = new URI(systemId);
      if (uri.isAbsolute()) {
        //System.out.println("Get stuff from web: " + systemId);
        return urlToInputStream(uri.toURL(), "text/xml");
      }
      // System.out.println("Get stuff from web: Host: " + baseUri + " Path: " + systemId);
      return getSchemaRelativeToBaseUri(baseUri, systemId);
    } catch (Exception e) {
      // maybe the systemId is not a valid URI or
      // the web has nothing to offer under this address
    }
    return null;
  }

  private InputStream urlToInputStream(URL url, String accept) {
    HttpURLConnection con;
    InputStream inputStream;
    try {
      con = (HttpURLConnection) url.openConnection();
      con.setConnectTimeout(15000);
      con.setRequestProperty("User-Agent", "Name of my application.");
      con.setReadTimeout(15000);
      con.setRequestProperty("Accept", accept);
      con.connect();
      int responseCode = con.getResponseCode();
      if (responseCode == HttpURLConnection.HTTP_MOVED_PERM || responseCode == HttpURLConnection.HTTP_MOVED_TEMP || responseCode == 307 || responseCode == 303) {
        String redirectUrl = con.getHeaderField("Location");
        try {
          URL newUrl = new URL(redirectUrl);
          return urlToInputStream(newUrl, accept);
        } catch (MalformedURLException e) {
          URL newUrl = new URL(url.getProtocol() + "://" + url.getHost() + redirectUrl);
          return urlToInputStream(newUrl, accept);
        }
      }
      inputStream = con.getInputStream();
      return inputStream;
    } catch (IOException e) {
      throw new RuntimeException(e);
    }

  }

  private InputStream getSchemaRelativeToBaseUri(String baseUri, String systemId) {
    try {
      URL url = new URL(baseUri + systemId);
      return urlToInputStream(url, "text/xml");
    } catch (Exception e) {
      e.printStackTrace();
      throw new RuntimeException(e);
    }
  }

  private InputStream getSchemaAsStream(String systemId, String baseUri) {
    InputStream in = getSchemaFromClasspath(systemId);
    // You could just return in; , if you are sure that everything is on
    // your machine. Here I call getSchemaFromWeb as last resort.
    return in == null ? getSchemaFromWeb(baseUri, systemId) : in;
  }

  private InputStream getSchemaFromClasspath(String systemId) {
    //System.out.println("Try to get stuff from localdir: " + localPath + systemId.substring(systemId.lastIndexOf('/')));
    return Thread.currentThread().getContextClassLoader().getResourceAsStream("schema" + systemId.substring(systemId.lastIndexOf('/')));
  }

}

