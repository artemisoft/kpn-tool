package org.ntic.view.editor;

import com.mxgraph.swing.mxGraphComponent;
import com.mxgraph.swing.util.mxGraphActions;
import com.mxgraph.util.mxEvent;
import com.mxgraph.util.mxEventSource.mxIEventListener;
import com.mxgraph.view.mxGraphView;
import org.ntic.Main;
import org.ntic.util.Language;
import org.ntic.util.Utils;
import org.ntic.view.editor.EditorActions.*;
import org.ntic.view.editor.simulation.Simulator;
import org.ntic.view.graph.KPetriNetGraph;
import org.ntic.view.ui.KButton;
import org.ntic.view.ui.RangeSliderPanel;
import org.ntic.view.ui.ShadowBorder;
import org.ntic.view.ui.ToggleButton;

import javax.swing.*;
import javax.swing.border.EmptyBorder;
import java.awt.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

import static org.ntic.view.graph.KPetriNetGraph.MODE;
import static org.ntic.view.graph.KPetriNetGraph.SIMULATION;

public class EditorRibbon extends JTabbedPane {

  private static final long serialVersionUID = -8015443128436394471L;
  private final RangeSliderPanel randomnessSlider = new RangeSliderPanel();

  private boolean ignoreZoomChange = false;

  public EditorRibbon(final GraphEditor editor, int orientation) {
    super(orientation);
    setBorder(ShadowBorder.getSharedInstance());

    setMaximumSize(new Dimension((int) getMaximumSize().getWidth(), 64));
    JToolBar fileTab = new JToolBar(JToolBar.HORIZONTAL);
    JToolBar editingTab = new JToolBar(JToolBar.HORIZONTAL);
    JToolBar simulationTab = new JToolBar(JToolBar.HORIZONTAL);
    JToolBar viewTab = new JToolBar(JToolBar.HORIZONTAL);
    JToolBar analysisTab = new JToolBar(JToolBar.HORIZONTAL);

    fileTab.setFloatable(false);
    editingTab.setFloatable(false);
    simulationTab.setFloatable(false);
    viewTab.setFloatable(false);
    analysisTab.setFloatable(false);

    fileTab.setBackground(Color.WHITE);
    editingTab.setBackground(Color.WHITE);
    simulationTab.setBackground(Color.WHITE);
    viewTab.setBackground(Color.WHITE);
    analysisTab.setBackground(Color.WHITE);

    fileTab.setMaximumSize(new Dimension((int) getMaximumSize().getWidth(), 64));
    editingTab.setMaximumSize(new Dimension((int) getMaximumSize().getWidth(), 64));
    simulationTab.setMaximumSize(new Dimension((int) getMaximumSize().getWidth(), 64));
    viewTab.setMaximumSize(new Dimension((int) getMaximumSize().getWidth(), 64));
    analysisTab.setMaximumSize(new Dimension((int) getMaximumSize().getWidth(), 64));

    add(Utils.getResource("file"), fileTab);
    add(Utils.getResource("editing"), editingTab);
    add(Utils.getResource("simulation"), simulationTab);
    add(Utils.getResource("view"), viewTab);
    add(Utils.getResource("analysis"), analysisTab);

    fileTab.add(editor.bind(Utils.getResource("new"), new NewAction(), "/icons/new_file_32.png")).setText(Utils.getResource("new"));
    fileTab.add(editor.bind(Utils.getResource("open"), new OpenAction(), "/icons/open_32.png")).setText(Utils.getResource("open"));
    fileTab.add(editor.bind(Utils.getResource("save"), new SaveAction(false), "/icons/save_32.png")).setText(Utils.getResource("save"));
    fileTab.add(editor.bind(Utils.getResource("saveAs"), new SaveAction(true), "/icons/save_as_32.png")).setText(Utils.getResource("saveAs"));
    fileTab.addSeparator();
    fileTab.add(editor.bind(Utils.getResource("print"), new PrintAction(), "/icons/print_32.png")).setText(Utils.getResource("print"));

    editingTab.add(editor.bind(Utils.getResource("cut"), TransferHandler.getCutAction(), "/icons/cut_32.png")).setText(Utils.getResource("cut"));
    editingTab.add(editor.bind(Utils.getResource("copy"), TransferHandler.getCopyAction(), "/icons/copy_32.png")).setText(Utils.getResource("copy"));
    editingTab.add(editor.bind(Utils.getResource("paste"), TransferHandler.getPasteAction(), "/icons/paste_32.png")).setText(Utils.getResource("paste"));
    editingTab.addSeparator();
    editingTab.add(editor.bind(Utils.getResource("undo"), new HistoryAction(true), "/icons/undo_32.png")).setText(Utils.getResource("undo"));
    editingTab.add(editor.bind(Utils.getResource("redo"), new HistoryAction(false), "/icons/redo_32.png")).setText(Utils.getResource("redo"));
    editingTab.addSeparator();

    ButtonGroup group = new ButtonGroup();

    JToggleButton pointerButton = new ToggleButton(editor.bind(Utils.getResource("selection"), KPetriNetGraphActions.getClearAddAction(), "/icons/cursor_32.png"));
    editingTab.add(pointerButton);
    group.add(pointerButton);
    JToggleButton arcButton = new ToggleButton(editor.bind(Utils.getResource("arc"), KPetriNetGraphActions.getAddArcAction(), "/icons/arc_32.png"));
    editingTab.add(arcButton);
    group.add(arcButton);
    JToggleButton statePlaceButton = new ToggleButton(editor.bind(Utils.getResource("statePlace"), KPetriNetGraphActions.getAddStatePlaceAction(), "/icons/state_place_32.png"));
    editingTab.add(statePlaceButton);
    group.add(statePlaceButton);
    JToggleButton knowledgePlaceButton = new ToggleButton(editor.bind(Utils.getResource("knowledgePlace"), KPetriNetGraphActions.getAddKnowledgePlaceAction(), "/icons/knowledge_place_32.png"));
    editingTab.add(knowledgePlaceButton);
    group.add(knowledgePlaceButton);
    JToggleButton transitionButton = new ToggleButton(editor.bind(Utils.getResource("transition"), KPetriNetGraphActions.getAddTransitionAction(), "/icons/transition_32.png"));
    editingTab.add(transitionButton);
    group.add(transitionButton);
    KButton agentButton = new KButton(editor.bind(Utils.getResource("agent"), KPetriNetGraphActions.getAddAgentAction(), "/icons/agent_32.png"));
    editingTab.add(agentButton);
    JToggleButton deleteButton = new ToggleButton(editor.bind(Utils.getResource("erase"), KPetriNetGraphActions.getDeleteAction(), "/icons/erase_32.png"));
    editingTab.add(deleteButton);
    group.add(deleteButton);

    agentButton.addActionListener(e -> group.clearSelection());

    KButton startSimButton = new KButton(editor.bind(Utils.getResource("startSim"), KPetriNetGraphActions.getStartSimulationAction(true), "/icons/play_32.png"));
    //group1.add(startSimButton);
    simulationTab.add(startSimButton);
    KButton pauseSimButton = new KButton(editor.bind(Utils.getResource("pauseSim"), KPetriNetGraphActions.getPauseSimulationAction(), "/icons/pause_32.png"));
    //group1.add(pauseSimButton);
    simulationTab.add(pauseSimButton);
    KButton stopSimButton = new KButton(editor.bind(Utils.getResource("stopSim"), KPetriNetGraphActions.getStopSimulationAction(), "/icons/stop_32.png"));
    //group1.add(stopSimButton);
    simulationTab.add(stopSimButton);
    KButton resetSimButton = new KButton(editor.bind(Utils.getResource("resetSim"), KPetriNetGraphActions.getResetSimulationAction(), "/icons/skip_to_start_32.png"));
    simulationTab.add(resetSimButton);
    //group1.add(resetSimButton);

    pauseSimButton.setEnabled(false);
    stopSimButton.setEnabled(false);
    resetSimButton.setEnabled(false);

    simulationTab.addSeparator();

    JComboBox<Simulator.SimulationMode> simulationMode = new JComboBox<>(Simulator.SimulationMode.values());
    simulationMode.addActionListener(e -> startSimButton.setAction(editor.bind(Utils.getResource("startSim"), KPetriNetGraphActions.getStartSimulationAction(simulationMode.getSelectedItem() == Simulator.SimulationMode.AUTOMATIC), "/icons/play_32.png")));
    simulationMode.setMinimumSize(new Dimension(160, 28));
    simulationMode.setPreferredSize(new Dimension(160, 28));
    simulationMode.setMaximumSize(new Dimension(160, 28));
    simulationTab.add(simulationMode);
    simulationTab.addSeparator();
    simulationTab.add(randomnessSlider);

    pointerButton.setSelected(true);
    editor.getGraph().addChangeListener(evt -> {
      if (evt.getPropertyName().equals(MODE)) {
        arcButton.setSelected(false);
        pointerButton.setSelected(false);
        transitionButton.setSelected(false);
        statePlaceButton.setSelected(false);
        knowledgePlaceButton.setSelected(false);
        deleteButton.setSelected(false);

        arcButton.setEnabled(true);
        pointerButton.setEnabled(true);
        transitionButton.setEnabled(true);
        statePlaceButton.setEnabled(true);
        knowledgePlaceButton.setEnabled(true);
        deleteButton.setEnabled(true);
        agentButton.setEnabled(true);

        String status = "Edit :";
        if (evt.getNewValue() == null) {
          pointerButton.setSelected(true);
          status = "Edit";
        } else {
          switch ((KPetriNetGraph.Mode) evt.getNewValue()) {
            case TRANSITION -> {
              transitionButton.setSelected(true);
              status += " adding transition";
            }
            case STATE_PLACE -> {
              statePlaceButton.setSelected(true);
              status += " adding state place";
            }
            case KNOWLEDGE_PLACE -> {
              knowledgePlaceButton.setSelected(true);
              status += " adding knowledge place";
            }
            case ARC -> {
              arcButton.setSelected(true);
              status += " adding arc";
            }
            case DELETE -> {
              deleteButton.setSelected(true);
              status += " deleting";
            }
            case SIMULATION -> {
              pointerButton.setEnabled(false);
              arcButton.setEnabled(false);
              transitionButton.setEnabled(false);
              statePlaceButton.setEnabled(false);
              knowledgePlaceButton.setEnabled(false);
              deleteButton.setEnabled(false);
              agentButton.setEnabled(false);
              status = Main.editor.getGraph().getSimulator().getSimulationMode().toString() + " : ";
            }
          }
        }
        Main.editor.label(status);

      } else if (evt.getPropertyName().equals(SIMULATION)) {
        //I installed the listener in graph because the simulator instance is ditched every time

        startSimButton.setEnabled(false);
        pauseSimButton.setEnabled(false);
        stopSimButton.setEnabled(false);
        resetSimButton.setEnabled(false);

        switch ((Simulator.SimulationState) evt.getNewValue()) {
          case NEW -> {
            startSimButton.setEnabled(true);

            simulationMode.setEnabled(true);
            arcButton.setEnabled(true);
            pointerButton.setEnabled(true);
            transitionButton.setEnabled(true);
            statePlaceButton.setEnabled(true);
            knowledgePlaceButton.setEnabled(true);
            deleteButton.setEnabled(true);
            agentButton.setEnabled(true);
            pointerButton.setSelected(true);
          }
          case RUNNING -> {
            simulationMode.setEnabled(false);
            if (editor.getGraph().getSimulator().getSimulationMode() == Simulator.SimulationMode.AUTOMATIC) {
              pauseSimButton.setEnabled(true);
              stopSimButton.setEnabled(true);
            }
            resetSimButton.setEnabled(true);
          }
          case PAUSED -> {
            startSimButton.setEnabled(true);
            stopSimButton.setEnabled(true);
            resetSimButton.setEnabled(true);
          }
          case ENDED, STOPPED -> resetSimButton.setEnabled(true);
        }
      }

    });

    viewTab.add(new ToggleProperty(editor.getGraphComponent(), Utils.getResource("antialias"), "AntiAlias", true));

    viewTab.addSeparator();

    viewTab.add(new KButton(editor.bind(Utils.getResource("zoomIn"), mxGraphActions.getZoomInAction(), "/icons/zoom_in_32.png")));
    viewTab.add(new KButton(editor.bind(Utils.getResource("zoomOut"), mxGraphActions.getZoomOutAction(), "/icons/zoom_out_32.png")));

    final mxGraphView view = editor.getGraphComponent().getGraph().getView();
    final JComboBox<Object> zoomCombo = new JComboBox<>(new Object[]{"400%", "200%", "150%", "100%", "75%", "50%", Utils.getResource("page"), Utils.getResource("width"), Utils.getResource("actualSize")});
    zoomCombo.setEditable(true);
    zoomCombo.setMinimumSize(new Dimension(72, 24));
    zoomCombo.setPreferredSize(new Dimension(72, 24));
    zoomCombo.setMaximumSize(new Dimension(72, 24));
    zoomCombo.setMaximumRowCount(9);
    viewTab.add(zoomCombo);

    viewTab.addSeparator();

    JLabel layoutLabel = new JLabel(Utils.getResource("layout"));
    layoutLabel.setBorder(new EmptyBorder(0, 0, 0, 4));
    viewTab.add(layoutLabel);

    final JComboBox<LayoutLabel> layoutCombo = new JComboBox<>(new LayoutLabel[]{
        new LayoutLabel("verticalHierarchical"),
        new LayoutLabel("horizontalHierarchical"),
        new LayoutLabel("verticalPartition"),
        new LayoutLabel("horizontalPartition"),
        new LayoutLabel("verticalStack"),
        new LayoutLabel("horizontalStack"),
        new LayoutLabel("verticalTree"),
        new LayoutLabel("horizontalTree"),
        new LayoutLabel("compactTreeLayout"),
        new LayoutLabel("orthogonalLayout"),
        new LayoutLabel("organicLayout"),
        new LayoutLabel("circleLayout")
    }
    );
    layoutCombo.setMinimumSize(new Dimension(128, 24));
    layoutCombo.setPreferredSize(new Dimension(128, 24));
    layoutCombo.setMaximumSize(new Dimension(128, 24));
    layoutCombo.setMaximumRowCount(9);
    viewTab.add(layoutCombo);

    viewTab.addSeparator();

    JLabel languageLabel = new JLabel(Utils.getResource("language"));
    languageLabel.setBorder(new EmptyBorder(0, 0, 0, 4));
    viewTab.add(languageLabel);

    final JComboBox<Language> languageCombo = new JComboBox<>(Language.values());
    languageCombo.setMinimumSize(new Dimension(96, 24));
    languageCombo.setPreferredSize(new Dimension(96, 24));
    languageCombo.setMaximumSize(new Dimension(96, 24));
    languageCombo.setMaximumRowCount(9);
    languageCombo.setSelectedItem(Language.valueOf(Utils.getSetting("language").toUpperCase()));
    viewTab.add(languageCombo);
    // Sets the zoom in the zoom combo the current value
    mxIEventListener scaleTracker = (sender, evt) -> {
      ignoreZoomChange = true;

      try {
        zoomCombo.setSelectedItem((int) Math.round(100 * view.getScale()) + "%");
      } finally {
        ignoreZoomChange = false;
      }
    };

    // Installs the scale tracker to update the value in the combo box
    // if the zoom is changed from outside the combo box
    view.getGraph().getView().addListener(mxEvent.SCALE, scaleTracker);
    view.getGraph().getView().addListener(mxEvent.SCALE_AND_TRANSLATE, scaleTracker);

    // Invokes once to sync with the actual zoom value
    scaleTracker.invoke(null, null);

    zoomCombo.addActionListener(new ActionListener() {
      /**
       *
       */
      @Override
      public void actionPerformed(ActionEvent e) {
        mxGraphComponent graphComponent = editor.getGraphComponent();

        // Zoom combo is changed when the scale is changed in the diagram
        // but the change is ignored here
        if (!ignoreZoomChange && zoomCombo.getSelectedItem() != null) {
          String zoom = zoomCombo.getSelectedItem().toString();

          if (zoom.equals(Utils.getResource("page"))) {
            graphComponent.setPageVisible(true);
            graphComponent.setZoomPolicy(mxGraphComponent.ZOOM_POLICY_PAGE);
          } else if (zoom.equals(Utils.getResource("width"))) {
            graphComponent.setPageVisible(true);
            graphComponent.setZoomPolicy(mxGraphComponent.ZOOM_POLICY_WIDTH);
          } else if (zoom.equals(Utils.getResource("actualSize"))) {
            graphComponent.zoomActual();
          } else {
            try {
              zoom = zoom.replace("%", "");
              double scale = Math.min(16, Math.max(0.01, Double.parseDouble(zoom) / 100));
              graphComponent.zoomTo(scale, graphComponent.isCenterZoom());
            } catch (Exception ex) {
              JOptionPane.showMessageDialog(editor, ex.getMessage());
            }
          }
        }
      }
    });

    languageCombo.addActionListener(e -> {
      if (languageCombo.getSelectedItem() != null) {
        Utils.setSetting("language", languageCombo.getSelectedItem().toString().toLowerCase().substring(0, 2));
        JOptionPane.showMessageDialog(Main.editor.getMainFrame(), Utils.getResource("languageChange"));
      }
    });
    if (layoutCombo.getSelectedItem() != null)
      layoutCombo.addActionListener(e -> editor.graphLayout(((LayoutLabel) layoutCombo.getSelectedItem()).getKey()).actionPerformed(new ActionEvent(languageCombo, ActionEvent.ACTION_PERFORMED, null)));

    analysisTab.add(new JButton(editor.bind(Utils.getResource("rg") + "  ", new RGAction(), "/icons/rg_48.png")));
    analysisTab.add(new JButton(editor.bind(Utils.getResource("srg") + "  ", new SRGAction(), "/icons/srg_48.png")));
    analysisTab.add(new JButton(editor.bind(Utils.getResource("modelChecker") + "  ", new ModelCheckerAction(), "/icons/model_checker_48.png")));
    setSelectedIndex(1);
  }

  public RangeSliderPanel getRandomnessSlider() {
    return randomnessSlider;
  }

  static class LayoutLabel {

    private final String key;

    public LayoutLabel(String key) {
      this.key = key;
    }

    @Override
    public String toString() {
      return Utils.getResource(key);
    }

    public String getKey() {
      return key;
    }

  }

}
