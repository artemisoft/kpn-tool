package org.ntic.view.editor.simulation;

import org.ntic.model.KPetriNet;
import org.ntic.model.Transition;
import org.ntic.util.Utils;

import static org.ntic.view.editor.simulation.Simulator.SimulationMode.AUTOMATIC;
import static org.ntic.view.editor.simulation.Simulator.SimulationState.*;

public class AutomaticSimulator extends Simulator {

  private final SimulationQueue firingQueue = new SimulationQueue();

  private int speed = 1, maxDelay = 3, minDelay = 1;

  public AutomaticSimulator(KPetriNet g) {
    super(g);
    this.setName("AutomaticSimulator");
  }

  @Override
  public void run() {
    checkInitialFiring();
    while (getSimulationState() != STOPPED && getSimulationState() != ENDED) {
      if (getSimulationState() == RUNNING) {
        Transition transition = firingQueue.peek();
        if (transition != null && transition.getNextFiringTime() != 0 && transition.getNextFiringTime() < System.currentTimeMillis()) {
          transition.fire(g);
          checkFirings(firingQueue.remove());
        }
        if (transition == null) {
          setSimulationState(ENDED);
        }
      }
    }
  }

  // Checks the firing of a transition
  @Override
  public void checkFiring(Transition transition) {
    super.checkFiring(transition);
    scheduleForFiring(transition);
  }

  @Override
  public SimulationMode getSimulationMode() {
    return AUTOMATIC;
  }

  @Override
  protected void scheduleForFiring(Transition transition) {
    if (transition.isFireable()) {
      int delay = Utils.getRandomNumberInRange(minDelay * 1000, maxDelay * 1000) / speed;
      transition.setNextFiringTime(System.currentTimeMillis() + delay);
      firingQueue.add(transition);
    } else {
      transition.setNextFiringTime(0); // To cancel firing if expired
      firingQueue.remove(transition);
    }
  }

  public int getSpeed() {
    return speed;
  }

  public void setSpeed(int speed) {
    this.speed = speed;
  }

  public int getMaxDelay() {
    return maxDelay;
  }

  public void setMaxDelay(int maxDelay) {
    this.maxDelay = maxDelay;
  }

  public int getMinDelay() {
    return minDelay;
  }

  public void setMinDelay(int minDelay) {
    this.minDelay = minDelay;
  }

}
