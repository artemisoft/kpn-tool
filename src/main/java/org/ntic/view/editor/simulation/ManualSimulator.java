package org.ntic.view.editor.simulation;

import org.ntic.model.KPetriNet;
import org.ntic.model.Transition;

import static org.ntic.view.editor.simulation.Simulator.SimulationMode.MANUAL;
import static org.ntic.view.editor.simulation.Simulator.SimulationState.RUNNING;
import static org.ntic.view.editor.simulation.Simulator.SimulationState.STOPPED;

public class ManualSimulator extends Simulator {

  private Transition transitionToFire = null;

  public ManualSimulator(KPetriNet g) {
    super(g);
    this.setName("ManualSimulator");
  }

  @Override
  public void run() {
    checkInitialFiring();
    while (getSimulationState() != STOPPED) {
      if (getSimulationState() == RUNNING && transitionToFire != null) {
        transitionToFire.fire(g);
        checkFirings(transitionToFire);
        transitionToFire = null;

      }
    }
  }

  @Override
  public void scheduleForFiring(Transition transition) {
    if (getSimulationState() == RUNNING && transition != null) {
      transition.fire(g);
      checkFirings(transition);
    }
  }

  @Override
  public SimulationMode getSimulationMode() {
    return MANUAL;
  }

}
