package org.ntic.view.editor.simulation;

import org.ntic.model.Transition;

import java.util.PriorityQueue;

public class SimulationQueue extends PriorityQueue<Transition> {

  @Override
  public boolean add(Transition transition) {
    if (!contains(transition))
      return super.add(transition);
    else return false;
  }

}
