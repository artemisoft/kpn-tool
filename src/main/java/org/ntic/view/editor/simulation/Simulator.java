package org.ntic.view.editor.simulation;

import org.ntic.Main;
import org.ntic.model.KPetriNet;
import org.ntic.model.Place;
import org.ntic.model.Transition;
import org.ntic.model.Vertex;

import java.beans.PropertyChangeEvent;
import java.beans.PropertyChangeListener;
import java.util.ArrayList;
import java.util.List;

import static org.ntic.view.editor.simulation.Simulator.SimulationState.*;

public abstract class Simulator extends Thread {

  private final List<PropertyChangeListener> listeners = new ArrayList<>();
  protected KPetriNet g;
  private SimulationState simulationState = NEW;

  public Simulator(KPetriNet g) {
    this.g = g;
  }

  public void startSimulation() {
    if (simulationState == NEW || simulationState == PAUSED) {
      setSimulationState(RUNNING);
      if (getState() == State.NEW) {
        start();
      }
      Main.editor.label(Main.editor.label().concat(" running."));
    }
  }

  public void pauseSimulation() {
    if (simulationState == RUNNING) {
      setSimulationState(PAUSED);
      Main.editor.label(Main.editor.label().concat(" paused."));
    }
  }

  public void stopSimulation() {
    if (simulationState == RUNNING || simulationState == PAUSED) {
      setSimulationState(STOPPED);
      Main.editor.label(Main.editor.label().concat(" stopped."));
    }
  }

  // Checks the firing of a transition
  public void checkFiring(Transition transition) {
    ArrayList<Vertex> tPredecessors = g.getPredecessors(transition);
    boolean fireable = true;
    for (Vertex p : tPredecessors) {
      if (!((Place) p).isMarked()) {
        fireable = false;
        break;
      }
    }
    transition.setFireable(fireable);
  }

  // Checks all firings at the initial marking
  protected void checkInitialFiring() {
    for (Vertex t : g.vertexSet()) {
      if (t instanceof Transition) {
        checkFiring((Transition) t);
      }
    }
  }

  // Checks firing for all impacted transition by the firing of the parameter transition
  public void checkFirings(Transition transition) {
    ArrayList<Vertex> places = new ArrayList<>();
    places.addAll(g.getPredecessors(transition));
    places.addAll(g.getSuccessors(transition));
    for (Vertex place : places) {
      ArrayList<Vertex> transitions = g.getSuccessors(place);
      for (Vertex t : transitions) {
        checkFiring((Transition) t);
      }
    }
  }

  public void resetSimulation() {
    setSimulationState(NEW);
    for (Place place : g.getPlaces()) {
      place.setMarked(place.isInitiallyMarked());
    }
    Main.editor.status(Main.editor.label().concat(" new."));
  }

  public abstract SimulationMode getSimulationMode();

  public boolean isSimulationMode() {
    return simulationState != NEW;
  }

  public SimulationState getSimulationState() {
    return simulationState;
  }

  public void setSimulationState(SimulationState simulationState) {
    notifyListeners(this.simulationState, this.simulationState = simulationState);
  }

  private void notifyListeners(Object oldValue, Object newValue) {
    for (PropertyChangeListener name : listeners) {
      name.propertyChange(new PropertyChangeEvent(this, "State", oldValue, newValue));
    }
  }

  public void addChangeListener(PropertyChangeListener newListener) {
    listeners.add(newListener);
  }

  protected abstract void scheduleForFiring(Transition transition);

  public void setG(KPetriNet g) {
    this.g = g;
  }

  public enum SimulationState {
    NEW,
    RUNNING,
    PAUSED,
    STOPPED,
    ENDED
  }

  public enum SimulationMode {
    AUTOMATIC("Automatic Simulation"),
    MANUAL("Manual Simulation");

    private final String label;

    SimulationMode(String label) {
      this.label = label;
    }

    @Override
    public String toString() {
      return label;
    }
  }

}
