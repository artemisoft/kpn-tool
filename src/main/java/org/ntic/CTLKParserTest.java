package org.ntic;

import org.ntic.model.modelchecking.CTLKFormulaParser;
import org.ntic.model.modelchecking.ctlk.CTLKFormula;

public class CTLKParserTest {

  public static void main(String[] args) {
    String expression = "(!E(true U ( p3,3 & !(K(!( !(p1,3) & !(p2,3) ))))))";
    CTLKFormulaParser expressionParser = new CTLKFormulaParser();
    CTLKFormula ctlkFormula = expressionParser.parse(expression);
    System.out.println(ctlkFormula);
  }

}
