package org.ntic.util;

import org.ntic.Main;

import java.io.*;
import java.util.Properties;
import java.util.Random;

import static org.ntic.Main.USER_SETTINGS_PATH;

public class Utils {

  public static String getResource(String key) {
    return getProperty(key, "editor_" + getSetting("language") + ".properties");
  }

  public static String getSetting(String key) {
    return Main.settings.getProperty(key);
  }

  private static String getProperty(String key, String propFileName) {
    Properties prop = new Properties();
    try (InputStream inputStream = Utils.class.getClassLoader().getResourceAsStream(propFileName)) {
      if (inputStream != null) {
        prop.load(inputStream);
      } else {
        throw new FileNotFoundException("property file '" + propFileName + "' not found in the classpath");
      }
      return prop.getProperty(key);
    } catch (Exception e) {
      e.printStackTrace();
    }
    return "";
  }

  public static void setSetting(String key, String value) {
    Properties prop = Main.settings;
    prop.setProperty(key, value);
    try {
      prop.store(new FileOutputStream(USER_SETTINGS_PATH), "Language changed");
    } catch (IOException e) {
      e.printStackTrace();
    }
  }

  public static int getRandomNumberInRange(int min, int max) {

    if (min > max) {
      throw new IllegalArgumentException("Max must be greater than min");
    }

    Random r = new Random();
    return r.nextInt((max - min) + 1) + min;
  }

  public static void copyDefaultSettings() throws IOException {
    boolean created = new File(System.getenv("LOCALAPPDATA") + "\\KPN Tool").mkdirs();
    boolean newFile = new File(USER_SETTINGS_PATH).createNewFile();
    if (created && newFile)
      try (InputStream is = Main.class.getClassLoader().getResourceAsStream("settings.properties"); OutputStream os = new FileOutputStream(USER_SETTINGS_PATH)) {
        byte[] buffer = new byte[1024];
        int length;
        if (is != null) {
          while ((length = is.read(buffer)) > 0) {
            os.write(buffer, 0, length);
          }
        }
      }
  }

}
