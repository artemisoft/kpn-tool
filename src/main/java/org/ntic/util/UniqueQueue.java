package org.ntic.util;

import java.util.LinkedList;

public class UniqueQueue<T> extends LinkedList<T> {

  public UniqueQueue(T t) {
    add(t);
  }

  public UniqueQueue() {

  }

  @Override
  public boolean add(T t) {
    return !contains(t) && super.add(t);
  }

}
