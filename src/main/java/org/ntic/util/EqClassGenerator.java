package org.ntic.util;

import org.apache.commons.lang3.tuple.MutablePair;
import org.apache.commons.lang3.tuple.Pair;
import org.ntic.model.Agent;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

public class EqClassGenerator {

  private static final ArrayList<Pair<Agent, Integer>> previousPairs = new ArrayList<>();
  private static final ArrayList<String> previousColors = new ArrayList<>();
  private static final Map<Pair<Agent, Integer>, String> previousReservations = new HashMap<>();

  public static String getNext(Agent agent, Integer eqClass) {
    if (!previousPairs.contains(new MutablePair<>(agent, eqClass))) {
      String generatedColor = generate();
      previousPairs.add(new MutablePair<>(agent, eqClass));
      previousReservations.put(new MutablePair<>(agent, eqClass), generatedColor);
      return generatedColor;
    }
    return previousReservations.get(new MutablePair<>(agent, eqClass));
  }

  public static String generate() {
    String colorCode;
    int tries = 0;
    do {
      tries++;
      int r = Utils.getRandomNumberInRange(127, 255);
      int g = Utils.getRandomNumberInRange(127, 255);
      int b = Utils.getRandomNumberInRange(127, 255);

      colorCode = String.format("#%02x%02x%02x", r, g, b);
    } while ((previousColors.contains(colorCode)) || tries > 100);

    return colorCode;
  }

}
