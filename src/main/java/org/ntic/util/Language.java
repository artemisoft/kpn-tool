package org.ntic.util;

public enum Language {
  EN(Utils.getResource("english")), FR(Utils.getResource("french"));

  String label;

  Language(String label) {
    this.label = label;
  }

  @Override
  public String toString() {
    return label;
  }
}
