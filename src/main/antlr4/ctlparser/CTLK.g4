grammar CTLK;

formula :
'(' formula ')' |

TRUE |
PLACE |

UNARY_OPERATOR formula |
(UNARY_OPERATOR | CTL_OPERATOR) '(' formula ')' |

formula BINARY_OPERATOR formula |
'(' formula BINARY_OPERATOR formula ')' |

UNTIL_OPERATOR '(' formula UNTIL_OPERATOR_SEPARATOR formula ')';

TRUE : 'true';
PLACE : [a-z][a-z0-9_,]+;

UNARY_OPERATOR : '!';

BINARY_OPERATOR : '|' | '&' | '->' | '<->';

UNTIL_OPERATOR : 'A' | 'E';
UNTIL_OPERATOR_SEPARATOR : 'U';

CTL_OPERATOR : 'AX' | 'AF'| 'AG' | 'EX'| 'EF' | 'EG' | 'K';

WHITESPACE : ' ' -> skip ;
