package org.ntic;

import org.ntic.model.*;
import org.ntic.util.Utils;
import org.ntic.view.editor.GraphEditor;

import java.util.ArrayList;

public class UndoRedoTest extends SwingTestCase {

  private static boolean didSetup = false;

  @Override
  protected void setUp() {
    if (!didSetup) {
      didSetup = true;
      Main.editor = new GraphEditor();
      graphEditor = Main.editor;
      ribbon = graphEditor.getEditorRibbon();

      KPetriNet petriNet = graphEditor.getGraph().getPetriNet();

      ArrayList arrayList = new ArrayList();
      arrayList.addAll(petriNet.vertexSet());
      petriNet.removeAllVertices(arrayList);
    }
  }

  public void testUndoMarking() {
    Place place = new StatePlace();
    graphEditor.getPetriNet().addVertex(place);

    place.setInitiallyMarked(true);
    assertTrue(place.isInitiallyMarked());

    TestUtils.getChildButtonByText(ribbon, Utils.getResource("undo")).doClick();

    assertFalse(place.isInitiallyMarked());

  }

  public void testUndoAddingStatePlace() {
    Place place = new StatePlace();
    graphEditor.getPetriNet().addVertex(place);

    assertTrue(graphEditor.getPetriNet().containsVertex(place));

    TestUtils.getChildButtonByText(ribbon, Utils.getResource("undo")).doClick();

    assertFalse(graphEditor.getPetriNet().containsVertex(place));

  }

  public void testUndoAddingKnowledgePlace() {
    Place place = new KnowledgePlace();
    graphEditor.getPetriNet().addVertex(place);

    assertTrue(graphEditor.getPetriNet().containsVertex(place));

    TestUtils.getChildButtonByText(ribbon, Utils.getResource("undo")).doClick();

    assertFalse(graphEditor.getPetriNet().containsVertex(place));

  }

  public void testUndoAddingTransition() {
    Transition transition = new Transition();
    graphEditor.getPetriNet().addVertex(transition);

    assertTrue(graphEditor.getPetriNet().containsVertex(transition));

    TestUtils.getChildButtonByText(ribbon, Utils.getResource("undo")).doClick();

    assertFalse(graphEditor.getPetriNet().containsVertex(transition));

  }

  public void testUndoAddingArc() {
    Transition transition = new Transition();
    Place place = new StatePlace();
    graphEditor.getPetriNet().addVertex(transition);
    graphEditor.getPetriNet().addVertex(place);
    Arc arc = graphEditor.getPetriNet().addEdge(transition, place);

    assertTrue(graphEditor.getPetriNet().containsVertex(transition));
    assertTrue(graphEditor.getPetriNet().containsVertex(place));
    assertTrue(graphEditor.getPetriNet().containsEdge(arc));

    TestUtils.getChildButtonByText(ribbon, Utils.getResource("undo")).doClick();

    assertTrue(graphEditor.getPetriNet().containsVertex(transition));
    assertTrue(graphEditor.getPetriNet().containsVertex(place));
    assertFalse(graphEditor.getPetriNet().containsEdge(arc));

  }

  public void testRedoMarking() {
    Place place = new StatePlace();
    graphEditor.getPetriNet().addVertex(place);

    place.setInitiallyMarked(true);
    assertTrue(place.isInitiallyMarked());

    TestUtils.getChildButtonByText(ribbon, Utils.getResource("undo")).doClick();

    assertFalse(place.isInitiallyMarked());

    TestUtils.getChildButtonByText(ribbon, Utils.getResource("redo")).doClick();
    assertTrue(place.isInitiallyMarked());

  }

  public void testRedoAddingStatePlace() {
    Place place = new StatePlace();
    graphEditor.getPetriNet().addVertex(place);

    assertTrue(graphEditor.getPetriNet().containsVertex(place));

    TestUtils.getChildButtonByText(ribbon, Utils.getResource("undo")).doClick();

    assertFalse(graphEditor.getPetriNet().containsVertex(place));

    TestUtils.getChildButtonByText(ribbon, Utils.getResource("redo")).doClick();

    assertTrue(graphEditor.getPetriNet().containsVertex(place));

  }

  public void testRedoAddingKnowledgePlace() {
    Place place = new KnowledgePlace();
    graphEditor.getPetriNet().addVertex(place);

    assertTrue(graphEditor.getPetriNet().containsVertex(place));

    TestUtils.getChildButtonByText(ribbon, Utils.getResource("undo")).doClick();

    assertFalse(graphEditor.getPetriNet().containsVertex(place));

    TestUtils.getChildButtonByText(ribbon, Utils.getResource("redo")).doClick();
    assertTrue(graphEditor.getPetriNet().containsVertex(place));

  }

  public void testRedoAddingTransition() {
    Transition transition = new Transition();
    graphEditor.getPetriNet().addVertex(transition);

    assertTrue(graphEditor.getPetriNet().containsVertex(transition));

    TestUtils.getChildButtonByText(ribbon, Utils.getResource("undo")).doClick();

    assertFalse(graphEditor.getPetriNet().containsVertex(transition));

    TestUtils.getChildButtonByText(ribbon, Utils.getResource("redo")).doClick();
    assertTrue(graphEditor.getPetriNet().containsVertex(transition));

  }

  public void testRedoAddingArc() {
    Transition transition = new Transition();
    Place place = new StatePlace();
    graphEditor.getPetriNet().addVertex(transition);
    graphEditor.getPetriNet().addVertex(place);
    graphEditor.getPetriNet().addEdge(transition, place);

    assertTrue(graphEditor.getPetriNet().containsVertex(transition));
    assertTrue(graphEditor.getPetriNet().containsVertex(place));
    assertTrue(graphEditor.getPetriNet().containsEdge(transition, place));

    TestUtils.getChildButtonByText(ribbon, Utils.getResource("undo")).doClick();

    assertTrue(graphEditor.getPetriNet().containsVertex(transition));
    assertTrue(graphEditor.getPetriNet().containsVertex(place));
    assertFalse(graphEditor.getPetriNet().containsEdge(transition, place));

    TestUtils.getChildButtonByText(ribbon, Utils.getResource("redo")).doClick();
    assertTrue(graphEditor.getPetriNet().containsVertex(transition));
    assertTrue(graphEditor.getPetriNet().containsVertex(place));
    assertTrue(graphEditor.getPetriNet().containsEdge(transition, place));

  }

}
