package org.ntic;

import junit.framework.TestCase;
import org.ntic.view.editor.EditorRibbon;
import org.ntic.view.editor.GraphEditor;
import org.ntic.view.editor.stax.StaXParser;
import org.ntic.view.editor.stax.StaxValidator;
import org.ntic.view.graph.KPetriNetGraph;
import org.ntic.view.graph.listeners.AddModeListener;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.awt.event.MouseListener;
import java.io.File;
import java.io.IOException;

public class SwingTestCase extends TestCase {

  protected static GraphEditor graphEditor;
  protected static EditorRibbon ribbon;
  private JFrame testFrame;

  @Override
  protected void tearDown() throws Exception {
    if (this.testFrame != null) {
      this.testFrame.dispose();
      this.testFrame = null;
    }
  }

  public JFrame getTestFrame() {
    if (this.testFrame == null) {
      this.testFrame = new JFrame("Test");
    }
    return this.testFrame;
  }

  public void test() {
  }

  protected AddModeListener getMouseListener() {
    MouseListener[] mouseMotionListeners = graphEditor.getGraph().getGraphComponent().getGraphControl().getMouseListeners();
    for (MouseListener mouseMotionListener : mouseMotionListeners) {
      if (mouseMotionListener instanceof AddModeListener) {
        return (AddModeListener) mouseMotionListener;
      }
    }
    return null;
  }

  protected void openFile() {

    GraphEditor editor = graphEditor;

    if (editor != null) {
      if (!editor.getGraphComponent().getKeyboardHandler().isCommandsActive())
        return;

      KPetriNetGraph graph = editor.getGraphComponent().getGraph();
      if (graph != null) {
        String wd = System.getProperty("user.dir");

        try {
          StaxValidator validator = new StaxValidator();
          if (!validator.validate("src/main/resources/kpnml/bit_transmission_protocol.kpnml")) {
            JOptionPane.showMessageDialog(editor.getGraphComponent(), "This File is Not valid", "Error", JOptionPane.ERROR_MESSAGE);
          } else {
            try {
              StaXParser parser = new StaXParser();
              parser.readNet("src/main/resources/kpnml/bit_transmission_protocol.kpnml", editor.getGraph());
              editor.setCurrentFile(new File("src/main/resources/kpnml/bit_transmission_protocol.kpnml"));

              editor.setModified(false);
              editor.getUndoManager().clear();
              editor.getGraphComponent().zoomAndCenter();
              editor.getGraphComponent().repaint();

              editor.getSidebar().changeGraph(graph);
            } catch (IOException | ParserConfigurationException | XPathExpressionException | SAXException ex) {
              ex.printStackTrace();
              JOptionPane.showMessageDialog(editor.getGraphComponent(), ex.toString(), "Error", JOptionPane.ERROR_MESSAGE);
            }
          }
        } catch (SAXException | IOException ex) {
          JOptionPane.showMessageDialog(editor.getGraphComponent(), "Could not validate this file", "Error", JOptionPane.ERROR_MESSAGE);
          ex.printStackTrace();
        }
      }

    }

  }

}

