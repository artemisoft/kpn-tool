package org.ntic;

import org.ntic.view.editor.simulation.Simulator;

import javax.swing.*;
import java.awt.*;

public class TestUtils {

  public static AbstractButton getChildButtonByText(Component parent, String name) {

    if (parent instanceof Container) {
      Component[] children = ((Container) parent).getComponents();
      for (Component component : children) {
        if (component instanceof AbstractButton && name.equals(((AbstractButton) component).getText())) {
          return (AbstractButton) component;
        }
        AbstractButton child = getChildButtonByText(component, name);
        if (child != null) {
          return child;
        }

      }
    }
    return null;

  }

  public static JLabel getChildLabelByText(Component parent, String name) {

    if (parent instanceof Container) {
      Component[] children = ((Container) parent).getComponents();
      for (Component component : children) {
        if (component instanceof JLabel && name.equals(((JLabel) component).getText())) {
          return (JLabel) component;
        }
        JLabel child = getChildLabelByText(component, name);
        if (child != null) {
          return child;
        }

      }
    }
    return null;
  }

  public static JComboBox<Simulator.SimulationMode> getComboBox(Component parent) {
    if (parent instanceof Container) {
      Component[] children = ((Container) parent).getComponents();
      for (Component component : children) {
        if (component instanceof JComboBox && ((JComboBox<Simulator.SimulationMode>) component).getModel().getSelectedItem() instanceof Simulator.SimulationMode) {

          return (JComboBox<Simulator.SimulationMode>) component;
        }
        JComboBox<Simulator.SimulationMode> child = getComboBox(component);
        if (child != null) {
          return child;
        }

      }
    }
    return null;
  }

  public static JTextPane getFirstJTExtPane(Component parent) {

    if (parent instanceof Container) {
      Component[] children = ((Container) parent).getComponents();
      for (Component component : children) {
        if (component instanceof JTextPane) {
          return (JTextPane) component;
        }
        JTextPane child = getFirstJTExtPane(component);
        if (child != null) {
          return child;
        }

      }
    }
    return null;
  }

}
