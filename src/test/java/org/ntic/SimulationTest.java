package org.ntic;

import org.ntic.model.*;
import org.ntic.util.Utils;
import org.ntic.view.editor.GraphEditor;
import org.ntic.view.editor.simulation.AutomaticSimulator;
import org.ntic.view.editor.simulation.Simulator;
import org.ntic.view.graph.KPetriNetGraph;
import org.ntic.view.graph.listeners.AddModeListener;

import javax.swing.*;
import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;
import java.util.concurrent.atomic.AtomicBoolean;

import static org.ntic.view.graph.KPetriNetGraph.SIMULATION;

public class SimulationTest extends SwingTestCase {

  private static Arc lastArc;
  private static boolean didSetup = false;

  @Override
  protected void setUp() {
    if (!didSetup) {
      didSetup = true;
      Main.editor = new GraphEditor();
      graphEditor = Main.editor;
      ribbon = graphEditor.getEditorRibbon();

      KPetriNet petriNet = graphEditor.getGraph().getPetriNet();

      ArrayList arrayList = new ArrayList();
      arrayList.addAll(petriNet.vertexSet());
      petriNet.removeAllVertices(arrayList);

      StatePlace p1 = new StatePlace("p1", true);
      StatePlace p2 = new StatePlace("p2", false);
      StatePlace p3 = new StatePlace("p3", false);
      StatePlace p4 = new StatePlace("p4", false);
      StatePlace p5 = new StatePlace("p5", false);
      petriNet.addVertex(p1);
      petriNet.addVertex(p2);
      petriNet.addVertex(p3);
      petriNet.addVertex(p4);
      petriNet.addVertex(p5);
      Transition t1 = new Transition();
      Transition t2 = new Transition();
      Transition t3 = new Transition();
      Transition t4 = new Transition();
      Transition t5 = new Transition();
      petriNet.addVertex(t1);
      petriNet.addVertex(t2);
      petriNet.addVertex(t3);
      petriNet.addVertex(t4);
      petriNet.addVertex(t5);

      petriNet.addEdge(p1, t1);
      petriNet.addEdge(t1, p2);
      petriNet.addEdge(p2, t2);
      petriNet.addEdge(t2, p3);
      petriNet.addEdge(p3, t4);
      petriNet.addEdge(t4, p4);
      petriNet.addEdge(p4, t5);
      lastArc = petriNet.addEdge(t5, p1);

      Simulator simulator = graphEditor.getGraph().getSimulator();
      if (simulator instanceof AutomaticSimulator) {
        ((AutomaticSimulator) simulator).setMinDelay(5);
        ((AutomaticSimulator) simulator).setMaxDelay(7);
      }
    }
  }

  public void testSimulationStart() {

    startSimulation();

    assertFalse(TestUtils.getChildButtonByText(ribbon, Utils.getResource("startSim")).isEnabled());
    assertTrue(TestUtils.getChildButtonByText(ribbon, Utils.getResource("pauseSim")).isEnabled());
    assertTrue(TestUtils.getChildButtonByText(ribbon, Utils.getResource("stopSim")).isEnabled());
    assertTrue(TestUtils.getChildButtonByText(ribbon, Utils.getResource("resetSim")).isEnabled());

    assertFalse(editingButtonsAreEnabled());

    assertFalse(editorInterractable());
    assertFalse(arcsCanBeCreated());

    assertFalse(markingCanBeSet());
    assertFalse(TestUtils.getComboBox(ribbon).isEnabled());

  }

  public void testSimulationPause() {
    startSimulation();

    JButton button1 = (JButton) TestUtils.getChildButtonByText(ribbon, Utils.getResource("pauseSim"));
    assertNotNull(button1);

    button1.doClick();
    assertEquals(Simulator.SimulationState.PAUSED, graphEditor.getGraph().getSimulator().getSimulationState());
    assertEquals(graphEditor.getGraph().getMode(), KPetriNetGraph.Mode.SIMULATION);

    assertFalse(editingButtonsAreEnabled());
    assertFalse(markingCanBeSet());
    assertFalse(editorInterractable());
    assertFalse(arcsCanBeCreated());

    assertTrue(TestUtils.getChildButtonByText(ribbon, Utils.getResource("startSim")).isEnabled());
    assertFalse(TestUtils.getChildButtonByText(ribbon, Utils.getResource("pauseSim")).isEnabled());
    assertTrue(TestUtils.getChildButtonByText(ribbon, Utils.getResource("stopSim")).isEnabled());
    assertTrue(TestUtils.getChildButtonByText(ribbon, Utils.getResource("resetSim")).isEnabled());
    assertFalse(TestUtils.getComboBox(ribbon).isEnabled());

    assertTrue(reusesSimulator());

  }

  public void testSimulationReset() {

    startSimulation();

    JButton button1 = (JButton) TestUtils.getChildButtonByText(ribbon, Utils.getResource("resetSim"));
    assertNotNull(button1);

    button1.doClick();
    assertEquals(graphEditor.getGraph().getSimulator().getSimulationState(), Simulator.SimulationState.NEW);
    assertNull(graphEditor.getGraph().getMode());

    assertTrue(editingButtonsAreEnabled());
    assertTrue(markingCanBeSet());
    assertTrue(editorInterractable());

    assertTrue(TestUtils.getChildButtonByText(ribbon, Utils.getResource("selection")).isSelected());

    TestUtils.getChildButtonByText(ribbon, Utils.getResource("arc")).doClick();
    assertTrue(arcsCanBeCreated());

    assertTrue(TestUtils.getChildButtonByText(ribbon, Utils.getResource("startSim")).isEnabled());
    assertFalse(TestUtils.getChildButtonByText(ribbon, Utils.getResource("pauseSim")).isEnabled());
    assertFalse(TestUtils.getChildButtonByText(ribbon, Utils.getResource("stopSim")).isEnabled());
    assertFalse(TestUtils.getChildButtonByText(ribbon, Utils.getResource("resetSim")).isEnabled());
    JComboBox<Simulator.SimulationMode> comboBox = TestUtils.getComboBox(ribbon);
    assertTrue(comboBox.isEnabled());

    assertFalse(reusesSimulator());
  }

  public void testSimulationStop() {

    startSimulation();
    JButton button1 = (JButton) TestUtils.getChildButtonByText(ribbon, Utils.getResource("stopSim"));
    assertNotNull(button1);

    button1.doClick();
    assertEquals(graphEditor.getGraph().getSimulator().getSimulationState(), Simulator.SimulationState.STOPPED);
    assertEquals(graphEditor.getGraph().getMode(), KPetriNetGraph.Mode.SIMULATION);

    assertFalse(editingButtonsAreEnabled());
    assertFalse(markingCanBeSet());
    assertFalse(editorInterractable());
    assertFalse(arcsCanBeCreated());

    assertFalse(TestUtils.getChildButtonByText(ribbon, Utils.getResource("startSim")).isEnabled());
    assertFalse(TestUtils.getChildButtonByText(ribbon, Utils.getResource("pauseSim")).isEnabled());
    assertFalse(TestUtils.getChildButtonByText(ribbon, Utils.getResource("stopSim")).isEnabled());
    assertTrue(TestUtils.getChildButtonByText(ribbon, Utils.getResource("resetSim")).isEnabled());
    assertFalse(TestUtils.getComboBox(ribbon).isEnabled());

    JButton button2 = (JButton) TestUtils.getChildButtonByText(ribbon, Utils.getResource("resetSim"));
    assertNotNull(button2);
    button2.doClick();
    assertEquals(graphEditor.getGraph().getSimulator().getSimulationState(), Simulator.SimulationState.NEW);
    assertFalse(reusesSimulator());

  }

  public void testSimulationEnd() {
    AtomicBoolean ended = new AtomicBoolean(false);
    graphEditor.getGraph().addChangeListener(evt -> {
      if (evt.getPropertyName().equals(SIMULATION)) {
        if (evt.getNewValue() == Simulator.SimulationState.ENDED) {

          assertEquals(graphEditor.getGraph().getSimulator().getSimulationState(), Simulator.SimulationState.ENDED);
          assertEquals(graphEditor.getGraph().getMode(), KPetriNetGraph.Mode.SIMULATION);

          assertFalse(editingButtonsAreEnabled());
          assertFalse(markingCanBeSet());
          assertFalse(editorInterractable());
          assertFalse(arcsCanBeCreated());

          assertFalse(TestUtils.getChildButtonByText(ribbon, Utils.getResource("startSim")).isEnabled());
          assertFalse(TestUtils.getChildButtonByText(ribbon, Utils.getResource("pauseSim")).isEnabled());
          assertFalse(TestUtils.getChildButtonByText(ribbon, Utils.getResource("stopSim")).isEnabled());
          assertTrue(TestUtils.getChildButtonByText(ribbon, Utils.getResource("resetSim")).isEnabled());
          assertFalse(TestUtils.getComboBox(ribbon).isEnabled());

          JButton button2 = (JButton) TestUtils.getChildButtonByText(ribbon, Utils.getResource("resetSim"));
          assertNotNull(button2);
          button2.doClick();
          assertEquals(graphEditor.getGraph().getSimulator().getSimulationState(), Simulator.SimulationState.NEW);
          assertFalse(reusesSimulator());
          ended.set(true);
        }
      }
    });

    graphEditor.getGraph().getPetriNet().removeEdge(lastArc);

    Simulator simulator = graphEditor.getGraph().getSimulator();
    if (simulator instanceof AutomaticSimulator) {
      ((AutomaticSimulator) simulator).setMinDelay(1);
      ((AutomaticSimulator) simulator).setMaxDelay(2);
    }
    startSimulation();

    while (!ended.get()) {
      try {
        Thread.sleep(500);
      } catch (InterruptedException e) {
        e.printStackTrace();
      }
    }

  }

  private boolean reusesSimulator() {

    Simulator simulator = graphEditor.getGraph().getSimulator();
    JButton button2 = (JButton) TestUtils.getChildButtonByText(ribbon, Utils.getResource("startSim"));
    assertNotNull(button2);
    button2.doClick();
    assertEquals(graphEditor.getGraph().getSimulator().getSimulationState(), Simulator.SimulationState.RUNNING);

    Simulator simulator1 = graphEditor.getGraph().getSimulator();
    assertTrue(simulator1.isAlive());
    return (simulator1 == simulator);
  }

  private boolean editorInterractable() {
    return graphEditor.getGraph().isCellSelectable(graphEditor.getGraph().getGraphPainter().getVertices().values().toArray()[0]) && graphEditor.getGraph().isCellMovable(graphEditor.getGraph().getGraphPainter().getVertices().values().toArray()[0]);
  }

  private boolean arcsCanBeCreated() {
    return graphEditor.getGraph().isValidSource(graphEditor.getGraph().getGraphPainter().getVertices().values().toArray()[0]);
  }

  private boolean markingCanBeSet() {
    Place place = graphEditor.getGraph().getPetriNet().getPlaces().get(0);
    boolean initialMarking = place.isMarked();

    AddModeListener listener = getMouseListener();
    listener.mouseReleased(new MouseEvent(graphEditor.getGraph().getGraphComponent(), 1, System.currentTimeMillis(), InputEvent.BUTTON1_DOWN_MASK, (int) graphEditor.getGraph().getGraphPainter().getVertices().get(place).getGeometry().getCenterX(), (int) graphEditor.getGraph().getGraphPainter().getVertices().get(place).getGeometry().getCenterY(), 1, 1, 2, false, 1));
    return (initialMarking != place.isMarked());
  }

  private boolean editingButtonsAreEnabled() {
    return TestUtils.getChildButtonByText(ribbon, Utils.getResource("selection")).isEnabled() && TestUtils.getChildButtonByText(ribbon, Utils.getResource("arc")).isEnabled() && TestUtils.getChildButtonByText(ribbon, Utils.getResource("statePlace")).isEnabled() && TestUtils.getChildButtonByText(ribbon, Utils.getResource("knowledgePlace")).isEnabled() && TestUtils.getChildButtonByText(ribbon, Utils.getResource("transition")).isEnabled() && TestUtils.getChildButtonByText(ribbon, Utils.getResource("agent")).isEnabled() && TestUtils.getChildButtonByText(ribbon, Utils.getResource("erase")).isEnabled();
  }

  private void startSimulation() {
    JButton button1 = (JButton) TestUtils.getChildButtonByText(ribbon, Utils.getResource("startSim"));
    assertNotNull(button1);

    button1.doClick();
    assertEquals(Simulator.SimulationState.RUNNING, graphEditor.getGraph().getSimulator().getSimulationState());
    assertEquals(graphEditor.getGraph().getMode(), KPetriNetGraph.Mode.SIMULATION);
  }

}
