package org.ntic;

import org.ntic.util.Utils;

import javax.swing.text.BadLocationException;

public class BitTransmissionProtocolCTLKTest extends CTLKTest {

  public static final String BIT_TRANSMISSION_PROTOCOL_KPNML = "src/test/resources/bit_transmission_protocol.kpnml";
  private String formula1 = "!E(true U ( p3,3 & !(K(!( !p1,3 & !p2,3 )))))";
  private String formula2 = "AG(p3,3 -> K(p1,3))";
  private String formula3 = "AG(p3,3->K(p2,3))";

  public void testFormula1ForAgent1() throws BadLocationException {

    LoadKPN(BIT_TRANSMISSION_PROTOCOL_KPNML);

    String result = checkFormula(formula1, "a1");

    assertEquals(Utils.getResource("notSatFormula"), result);
  }

  public void testFormula1ForAgent3() throws BadLocationException {
    LoadKPN(BIT_TRANSMISSION_PROTOCOL_KPNML);

    String result = checkFormula(formula1, "a3");

    assertEquals(Utils.getResource("satFormula"), result);
  }

  public void testFormula2ForAgent3() throws BadLocationException {
    LoadKPN(BIT_TRANSMISSION_PROTOCOL_KPNML);

    String result = checkFormula(formula2, "a3");

    assertEquals(Utils.getResource("notSatFormula"), result);
  }

  public void testFormula2ForAgent1() throws BadLocationException {
    LoadKPN(BIT_TRANSMISSION_PROTOCOL_KPNML);

    String result = checkFormula(formula2, "a1");

    assertEquals(Utils.getResource("notSatFormula"), result);
  }

  public void testFormula3ForAgent3() throws BadLocationException {
    LoadKPN(BIT_TRANSMISSION_PROTOCOL_KPNML);

    String result = checkFormula(formula3, "a3");

    assertEquals(Utils.getResource("notSatFormula"), result);
  }

  public void testFormula3ForAgent1() throws BadLocationException {
    LoadKPN(BIT_TRANSMISSION_PROTOCOL_KPNML);

    String result = checkFormula(formula3, "a1");

    assertEquals(Utils.getResource("notSatFormula"), result);
  }

}
