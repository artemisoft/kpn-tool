package org.ntic;

import org.ntic.model.KPetriNet;
import org.ntic.model.KnowledgePlace;
import org.ntic.util.Utils;
import org.ntic.view.editor.GraphEditor;
import org.ntic.view.graph.listeners.AddModeListener;
import org.ntic.view.ui.ToggleButton;

import java.awt.event.InputEvent;
import java.awt.event.MouseEvent;
import java.util.ArrayList;

public class GraphListenerTest extends SwingTestCase {

  private static boolean didSetup = false;

  @Override
  protected void setUp() {
    if (!didSetup) {
      didSetup = true;
      Main.editor = new GraphEditor();
      graphEditor = Main.editor;
      ribbon = graphEditor.getEditorRibbon();

      KPetriNet petriNet = graphEditor.getGraph().getPetriNet();

      ArrayList arrayList = new ArrayList();
      arrayList.addAll(petriNet.vertexSet());
      petriNet.removeAllVertices(arrayList);

      KnowledgePlace p1 = new KnowledgePlace("p1", true);

    }
  }

  public void testAddOnePlace() {

    openFile();

    int count = graphEditor.getGraph().getChildCells(graphEditor.getGraph().getDefaultParent()).length;

    ToggleButton button1 = (ToggleButton) TestUtils.getChildButtonByText(ribbon, Utils.getResource("knowledgePlace"));
    assertNotNull(button1);

    button1.doClick();
    AddModeListener listener = getMouseListener();
    listener.mouseReleased(new MouseEvent(graphEditor.getGraph().getGraphComponent(), 1, System.currentTimeMillis(), InputEvent.BUTTON1_DOWN_MASK, 100, 100, 1, 1, 1, false, 1));

    assertEquals(count + 1, graphEditor.getGraph().getChildCells(graphEditor.getGraph().getDefaultParent()).length);

  }

}
