package org.ntic;

import org.ntic.model.Agent;
import org.ntic.model.KPetriNet;
import org.ntic.view.editor.GraphEditor;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.Set;

public class EditorTest extends SwingTestCase {

  private static boolean didSetup = false;

  @Override
  protected void setUp() {
    if (!didSetup) {
      didSetup = true;
      Main.editor = new GraphEditor();
      graphEditor = Main.editor;
      ribbon = graphEditor.getEditorRibbon();

      KPetriNet petriNet = graphEditor.getGraph().getPetriNet();

      ArrayList arrayList = new ArrayList();
      arrayList.addAll(petriNet.vertexSet());
      petriNet.removeAllVertices(arrayList);

      graphEditor.getGraph().getAgentManager().addAgent("A1", true);
    }
  }

  public void testOpenFile() {
    Set<Agent> agents = new HashSet<>(graphEditor.getGraph().getAgentManager().getAgents());
    KPetriNet petriNet = graphEditor.getPetriNet();
    openFile();

    assertNotSame(graphEditor.getPetriNet(), petriNet);

    Set<Agent> agents2 = graphEditor.getGraph().getAgentManager().getAgents();

    for (Agent agent : agents) {
      assertFalse(agents2.contains(agent));
    }

  }

}
