package org.ntic;

import org.ntic.model.Agent;
import org.ntic.model.AgentManager;
import org.ntic.model.modelchecking.ModelChecker;
import org.ntic.util.Utils;
import org.ntic.view.editor.FormulaChecker;
import org.ntic.view.editor.GraphEditor;
import org.ntic.view.editor.stax.StaXParser;
import org.ntic.view.editor.stax.StaxValidator;
import org.xml.sax.SAXException;

import javax.swing.*;
import javax.swing.text.BadLocationException;
import javax.swing.text.SimpleAttributeSet;
import javax.swing.text.StyledDocument;
import javax.xml.parsers.ParserConfigurationException;
import javax.xml.xpath.XPathExpressionException;
import java.io.File;
import java.io.IOException;

import static org.ntic.Main.editor;

public class CTLKTest extends SwingTestCase {

  @Override
  protected void setUp() throws Exception {
    Main.editor = new GraphEditor();
  }

  protected void LoadKPN(String filename) {
    try {
      StaxValidator validator = new StaxValidator();
      if (validator.validate(filename)) {
        try {
          StaXParser parser = new StaXParser();
          parser.readNet(filename, editor.getGraph());
          editor.setCurrentFile(new File(filename));
          //resetEditor(editor);

        } catch (IOException | ParserConfigurationException | XPathExpressionException | SAXException ex) {
          ex.printStackTrace();
        }
      }
    } catch (SAXException | IOException ex) {
      ex.printStackTrace();
    }
  }

  protected String checkFormula(String formula, String agentName) throws BadLocationException {
    AgentManager agentManager = editor.getGraph().getAgentManager();
    Agent agent = agentManager.getAgents().stream().filter(a -> a.getName().equals(agentName)).findFirst().get();
    ModelChecker modelChecker = new ModelChecker(agentManager, agent);
    final FormulaChecker formulaChecker = new FormulaChecker(modelChecker);

    JLabel result = TestUtils.getChildLabelByText(formulaChecker, Utils.getResource("enterFormula"));

    JTextPane formulaEditor = TestUtils.getFirstJTExtPane(formulaChecker);

    StyledDocument doc = (StyledDocument) formulaEditor.getDocument();
    doc.insertString(0, formula, new SimpleAttributeSet());

    JButton checkButton = (JButton) TestUtils.getChildButtonByText(formulaChecker, Utils.getResource("check"));
    checkButton.doClick();
    return result.getText();
  }

}
