package org.ntic;

import com.mxgraph.model.mxCell;
import org.ntic.model.*;
import org.ntic.util.Utils;
import org.ntic.view.editor.GraphEditor;

import java.util.ArrayList;
import java.util.HashSet;

public class KPetriNetGraphTest extends SwingTestCase {

  private static boolean didSetup = false;

  @Override
  protected void setUp() {
    if (!didSetup) {
      didSetup = true;
      Main.editor = new GraphEditor();
      graphEditor = Main.editor;
      ribbon = graphEditor.getEditorRibbon();

      KPetriNet petriNet = graphEditor.getGraph().getPetriNet();

      ArrayList arrayList = new ArrayList();
      arrayList.addAll(petriNet.vertexSet());
      petriNet.removeAllVertices(arrayList);

      StatePlace p1 = new StatePlace("p1", true);
      petriNet.addVertex(p1);
      Transition transition = new Transition("t");
      petriNet.addVertex(transition);

    }
  }

  public void testRename() {
    Place place = graphEditor.getGraph().getPetriNet().getPlaces().get(0);

    mxCell placeCell = graphEditor.getGraph().getGraphPainter().getVertices().get(place);
    mxCell label = (mxCell) placeCell.getChildAt(0);

    graphEditor.getGraph().cellLabelChanged(label, "P1", true);

    assertEquals(place.getName(), label.getValue());
    assertTrue(graphEditor.getGraph().getPetriNet().containsVertex(place));

    Transition transition = graphEditor.getGraph().getPetriNet().getTransitions().get(0);

    mxCell transitionCell = graphEditor.getGraph().getGraphPainter().getVertices().get(transition);
    mxCell label2 = (mxCell) transitionCell.getChildAt(0);

    graphEditor.getGraph().cellLabelChanged(label2, "pd1", true);

    assertEquals(transition.getName(), label2.getValue());
    assertTrue(graphEditor.getGraph().getPetriNet().containsVertex(transition));

  }

  public void testCopyPaste() {

    KPetriNet petriNet = graphEditor.getGraph().getPetriNet();

    ArrayList arrayList = new ArrayList();
    arrayList.addAll(petriNet.vertexSet());
    petriNet.removeAllVertices(arrayList);

    Transition transition1 = new Transition();
    Transition transition2 = new Transition();
    StatePlace place = new StatePlace();
    petriNet.addVertex(transition1);
    petriNet.addVertex(transition2);
    petriNet.addVertex(place);

    Arc arc1 = petriNet.addEdge(transition1, place);
    Arc arc2 = petriNet.addEdge(place, transition2);

    Object[] cells = new Object[]{
        graphEditor.getGraph().getGraphPainter().getVertices().get(transition1),
        graphEditor.getGraph().getGraphPainter().getVertices().get(place),
        graphEditor.getGraph().getGraphPainter().getCellFromArc(arc1),
        graphEditor.getGraph().getGraphPainter().getCellFromArc(arc2),
    };
    graphEditor.getGraph().setSelectionCells(cells);

    TestUtils.getChildButtonByText(ribbon, Utils.getResource("copy")).doClick();
    TestUtils.getChildButtonByText(ribbon, Utils.getResource("paste")).doClick();

    assertEquals(5, petriNet.vertexSet().size());
    assertEquals(3, petriNet.edgeSet().size());

    HashSet<String> uniqueNames = new HashSet<>();

    for (Vertex vertex : petriNet.vertexSet()) {
      assertFalse(uniqueNames.contains(vertex.getName()));
      uniqueNames.add(vertex.getName());
    }

  }

}


